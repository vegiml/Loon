﻿using Loon;
using Loon.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Experiments
{
    class Program
    {
        static void Main(string[] args)
        {
            string excelfileToWrite = @"C:\Users\vegim\Desktop\Experimenti_Elitist_instance2_FinalExperiment.xlsx";
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add("First");
                excel.Workbook.Worksheets.Add("Second");
                excel.Workbook.Worksheets.Add("Third");
                excel.Workbook.Worksheets.Add("Fourth");
                excel.Workbook.Worksheets.Add("Fifth");
                FileInfo excelFile = new FileInfo(excelfileToWrite);
                excel.SaveAs(excelFile);
            }
            //Declarations of variables
            string follderPath = @"C:\Users\vegim\OneDrive\Diploma\Documents-Vegim\Visual Studio 2017\Projects\Loon\Loon\Instances\";
            string file = "data.in";
            string fullPath = follderPath + file;

            string[] instanceLines = File.ReadAllLines(fullPath);
            Random randomNumberGenerator = new Random();
            int[] possibilities = new int[] { 0, -1, 1 };
            List<double> meanSolutionFItness = new List<double>();
            //Declare parameters that we should consider
            //selection aggresivity is based on the tournamenSize variable value
            int randomindividualindex;
            int randomLoonIndex;
            HashSet<int[,]> solutionsEvolution = new HashSet<int[,]>(new MatrixComparer());
            List<int> bestSolutionsScore = new List<int>();
            List<Solution> tournametList;
            Solution firstParent;
            Solution secondParent;
            Solution firstWorstSolutionTournamnet;
            Solution secondWorstSolutionTournament;
            bool executeMutate;
            bool executeMutateTurn;
            bool executeSwapTurn;
            bool executeMutate_Loon_Route;
            int randomNumberForMutation;
            int randomNumberForCrossover;
            int randomNumberForMutationExecution;
            int firsTurn;
            int SecondTurn;
            List<Solution> childs = new List<Solution>();
            int bestscore = 0;
            int maxScoreOnThisPopulation = 0;
            //parameters of Genetic Algorithm
            Console.WriteLine("Select generation size: ");
            int generationSize = int.Parse(Console.ReadLine());
            bool exploitiveMethod = true;
            int nrIterationsNoImprovement = 200;
            int popSize = 10;
            Console.WriteLine("Select population size ");
            popSize = int.Parse(Console.ReadLine());
            Console.WriteLine("Select tournamnet size ");
            //int tournamentSize = Convert.ToInt32(popSize * 0.7);
            int tournamentSize = int.Parse(Console.ReadLine());

            //exploration rate for genetic operators mutate and mutate_rate
            double explorationRate = 0.6;
            //if we should have mutation at all
            //parametrat e operatoreve te mutacionit 


            //parametrat
            double mute_rate = 0.5;
            double crossoverRate = 0.4;
            //types of mutation operators rates that will be used
            double mutate_Rate = 0.3;
            double swap_turns_Rate = 0;
            double mutate_turn_Rate = 0;
            double mutate_loon_route_rate = 0.7;
            //percentage of turns and baloons to be changed by methods mutate (percentageOfBaloonsToBeChanged)
            //and mutate_rate (percentageOfRowsToBeChanged,percentageOfBaloonsToBeChanged)
            double percentageOfRowsToBeChanged = 0.01;
            double percentageOfBaloonsToBeChanged = 0.09;


            List<string> bestSolutionsToWriteExcel;
            //Input file processing
            List<Solution> solutionsList = new List<Solution>();
            int rows = Convert.ToInt32(instanceLines[0].Split(' ')[0]);
            int columns = Convert.ToInt32(instanceLines[0].Split(' ')[1]);
            int altitudes = Convert.ToInt32(instanceLines[0].Split(' ')[2]);
            InputInstance instance = new InputInstance(altitudes, columns, rows);
            instance.NrTargetCells = Convert.ToInt32(instanceLines[1].Split(' ')[0]);
            instance.Radius = Convert.ToInt32(instanceLines[1].Split(' ')[1]);
            instance.NrBaloons = Convert.ToInt32(instanceLines[1].Split(' ')[2]);
            instance.NrTurns = Convert.ToInt32(instanceLines[1].Split(' ')[3]);
            instance.StartCell = new Coordinate(Convert.ToInt32(instanceLines[2].Split(' ')[0]), Convert.ToInt32(instanceLines[2].Split(' ')[1]));
            int[] ballonAltitudes = new int[instance.NrBaloons];
            ballonAltitudes.Populate<int>(0);
            HashSet<int[,]> solutions = new HashSet<int[,]>(new MatrixComparer());
            Coordinate windData;
            List<Solution> bestSolutions = new List<Solution>();
            for (int i = 0; i < instance.NrTargetCells; i++)
            {
                instance.TargetCells.Add(new Coordinate(Convert.ToInt32(instanceLines[i + 3].Split(' ')[0]), Convert.ToInt32(instanceLines[i + 3].Split(' ')[1])));
            }
            for (int i = 0; i < instance.Altitudes; i++)
            {
                for (int j = 0; j < instance.NrRows; j++)
                {
                    for (int k = 0; k < instance.NrColumns; k++)
                    {
                        int x = Convert.ToInt32(instanceLines[j + (i * instance.NrRows) + (3 + instance.NrTargetCells)].Split(' ')[2 * k]);
                        int y = Convert.ToInt32(instanceLines[(j + (i * instance.NrRows) + (3 + instance.NrTargetCells))].Split(' ')[2 * k + 1]);
                        instance.WindData[i, j, k] = new Coordinate(x, y);
                    }
                }
            }
            Solution firstChild = new Solution(instance.NrRows, instance.NrColumns);
            Solution secondChild = new Solution(instance.NrRows, instance.NrColumns);
            LoonFunctions loonfunction = new LoonFunctions(instance);
            //pjesa e GA - evolutionary algorithm
            //Steady state or generational EA. Selection method to be choosen
            #region
            Stopwatch stopwatch = new Stopwatch();


            bestSolutionsToWriteExcel = new List<string>();
            solutionsList = loonfunction.generateInitialSolutionImproved(popSize);
            int bestSolutionOnInitialPopulation = solutionsList.Max(a => a.solutionScore);
            int worstSolutionOnInitialPopulationst = solutionsList.Min(a => a.solutionScore);
            stopwatch.Start();
            bestscore = bestSolutionOnInitialPopulation;
            //timer configuration
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromSeconds(10);
            var timer = new System.Threading.Timer((e) =>
            {
                WriteSolutionToList(bestscore, bestSolutionsToWriteExcel);
            }, null, startTimeSpan, periodTimeSpan);
            //timer configuration end

            //initial population end
            #endregion
            #region
            //for (int i = 0; i < solutionsList.Count; i++)
            //{
            //    //solutionScore = loonfunction.CalculateScoreWithTurnNew(solutionsList[i]);
            //    Console.WriteLine("Solution for {0} population is {1} ", i, solutionsList[i].solutionScore);
            //    //solutionsList[i].solutionScore = solutionScore;
            //}
            //stopwatch configuration

            //fittnes function end
            #endregion
            List<Solution> eliteSolutions = new List<Solution>();
            for (int generation = 1; generation <= generationSize; generation++)
            {
                if (stopwatch.Elapsed > TimeSpan.FromMinutes(300))
                {
                      WriteToExcel(bestSolutionsToWriteExcel, 0, excelfileToWrite);
                    stopwatch.Stop();
                    stopwatch.Reset();
                    bestscore = 0;
                    bestSolutionsToWriteExcel = new List<string>();
                    break;
                }
                eliteSolutions = new List<Solution>();
                solutionsList.OrderByDescending(a => a.solutionScore);
                int n = solutionsList.Count / 3;
                for (int i = 0; i < n; i++)
                {
                    eliteSolutions.Add(ExtensionMethods.DeepCopy(solutionsList[i]));

                }
                for (int i = 0; i < (popSize - n) / 2; i++)
                {
                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    //selection phase (tournament based)  
                    tournametList = loonfunction.tournamentSelection(tournamentSize, solutionsList);
                    tournametList = tournametList.OrderByDescending(b => b.solutionScore).ToList();
                    firstParent = tournametList.ElementAt(0);
                    secondParent = tournametList.ElementAt(1);
                    int s = 1;
                    while (firstParent.solutionScore == secondParent.solutionScore)
                    {
                        s++;
                        if (s == tournamentSize)
                        {
                            break;
                        }
                        secondParent = tournametList.ElementAt(s);
                    }
                    firstWorstSolutionTournamnet = tournametList.ElementAt(tournametList.Count - 1);
                    secondWorstSolutionTournament = tournametList.ElementAt(tournametList.Count - 2);
                    // end selection

                    randomNumberForMutation = randomNumberGenerator.Next(1, 101);
                    randomNumberForCrossover = randomNumberGenerator.Next(1, 101);
                    executeMutate = (randomNumberForMutation >= 0) && (randomNumberForMutation <= (mutate_Rate * 100));
                    executeMutateTurn = (randomNumberForMutation >= (mutate_Rate * 100)) && (randomNumberForMutation <= ((mutate_Rate * 100) + (mutate_turn_Rate * 100)));
                    executeSwapTurn = ((randomNumberForMutation >= (mutate_Rate * 100) + (mutate_turn_Rate * 100))) && (randomNumberForMutation <= ((mutate_turn_Rate * 100) + (mutate_Rate * 100) + (swap_turns_Rate * 100)));
                    executeMutate_Loon_Route = ((randomNumberForMutation >= (mutate_Rate * 100) + (mutate_turn_Rate * 100) + (swap_turns_Rate * 100))) && (randomNumberForMutation <= ((mutate_turn_Rate * 100) + (mutate_Rate * 100) + (swap_turns_Rate * 100) + (mutate_loon_route_rate * 100)));
                    if (randomNumberForCrossover <= (crossoverRate * 100))
                    {
                        childs = loonfunction.crossoverOperator(firstParent, secondParent);
                        firstChild = childs[0];
                        secondChild = childs[1];
                    }
                    else
                    {
                        firstChild = firstParent;
                        secondChild = secondParent;
                    }
                    randomNumberForMutationExecution = randomNumberGenerator.Next(1, 101);
                    if (randomNumberForMutationExecution <= (mute_rate * 100))
                    {
                        if (executeMutate)
                        {
                            int randomNumberOfLoonsToChangerRoute = randomNumberGenerator.Next(1, 4);
                            int loonIndex = 0;
                            while (loonIndex < randomNumberOfLoonsToChangerRoute)
                            {
                                loonfunction.mutate_loon_route_random(firstChild, instance, loonIndex);
                                loonfunction.mutate_loon_route_random(secondChild, instance, loonIndex);
                                loonIndex++;
                            }
                            firstChild.solutionScore = loonfunction.CalculateScoreWithTurnNew(firstChild);
                            secondChild.solutionScore = loonfunction.CalculateScoreWithTurnNew(secondChild);
                        }
                        else if (executeMutate_Loon_Route)
                        {
                            int randomNumberOfLoonsToChangerRoute = randomNumberGenerator.Next(1, instance.NrBaloons / 15);
                            int loonIndex = 0;
                            while (loonIndex < randomNumberOfLoonsToChangerRoute)
                            {
                                firstChild = loonfunction.mutate_loon_route_combine(firstChild, instance, loonIndex, exploitiveMethod);
                                secondChild = loonfunction.mutate_loon_route_combine(secondChild, instance, loonIndex, exploitiveMethod);
                                loonIndex++;
                            }
                        }
                    }
                    eliteSolutions.Add(ExtensionMethods.DeepCopy(firstChild));
                    eliteSolutions.Add(ExtensionMethods.DeepCopy(secondChild));
                }
                solutionsList = new List<Solution>();
                solutionsList = eliteSolutions;

                //end mutation
                //testing convergence
                //do not remove every time the worst population
                //remove randomly
                //if (firstChild.solutionScore >= firstWorstSolutionTournamnet.solutionScore)
                //{
                //    if (exploitiveMethod)
                //    {
                //        solutionsList.Add(firstChild);
                //        solutionsList.Remove(firstWorstSolutionTournamnet);
                //    }
                //    else
                //    {
                //        randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);
                //        while (firstChild.Equals(solutionsList[randomindividualindex]))
                //        {
                //            randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);
                //        }
                //        solutionsList.RemoveAt(randomindividualindex);
                //    }
                //}
                //if (secondChild.solutionScore >= secondWorstSolutionTournament.solutionScore)
                //{
                //    if (exploitiveMethod)
                //    {
                //        solutionsList.Add(secondChild);
                //        solutionsList.Remove(secondWorstSolutionTournament);
                //    }
                //    else
                //    {
                //        randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);
                //        while (secondChild.Equals(solutionsList[randomindividualindex]))
                //        {
                //            randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);

                //        }
                //        solutionsList.RemoveAt(randomindividualindex);
                //    }
                //}
                maxScoreOnThisPopulation = solutionsList.Max(a => a.solutionScore);
                if (maxScoreOnThisPopulation > bestscore)
                {
                    bestscore = maxScoreOnThisPopulation;
                    bestSolutionsScore.Add(bestscore);
                    //bestSolutionsToWriteExcel.Add(bestscore.ToString());
                    //Solution solution = solutionsList.Where(a => a.solutionScore == bestscore).FirstOrDefault();
                    //bestSolutions.Add(ExtensionMethods.DeepCopy(solution));
                }
                else
                {
                    bestSolutionsScore.Add(maxScoreOnThisPopulation);
                    //bestSolutionsToWriteExcel.Add(maxScoreOnThisPopulation.ToString());
                }
                if (((generation) % nrIterationsNoImprovement) == 0)
                {
                    //nese ne nrIterationsNoImprovement nuk ka improvement shko ne meneyre explorative
                    // perndryshe shko ne meneyre exploitive
                    if (!(loonfunction.checkIfNoImprovement(nrIterationsNoImprovement, bestSolutionsScore)))
                    {
                        //explorative
                        exploitiveMethod = false;
                        //tournamentSize = Convert.ToInt32(popSize * 0.3);
                        //mutate_Rate = 0.4;
                        // mutate_loon_route_rate = 0.6;
                    }
                    else
                    {
                        //exploative
                        exploitiveMethod = true;
                        //tournamentSize = Convert.ToInt32(popSize * 0.7);
                        // mutate_Rate = 0.3;
                        //mutate_loon_route_rate = 0.7;
                    }
                }
                //meanSolutionFItness.Add(solutionsList.Average(a => a.solutionScore));
                // watch.Stop();
                // var elapsedMs = watch.ElapsedMilliseconds;
                if (generation == generationSize)
                {

                    Solution bestSolution = bestSolutions.Where(a => a.solutionScore == bestscore).FirstOrDefault();
                    string filename4 = "solution" + DateTime.Now.Ticks;
                    using (TextWriter tw = new StreamWriter(@"C:\Users\vegim\Desktop\" + filename4 + ".txt"))

                    {
                        for (int j = 0; j < solutionsList[0].solutionMatrix.GetLength(0); j++)
                        {
                            for (int i = 0; i < solutionsList[0].solutionMatrix.GetLength(1); i++)
                            {
                                tw.Write(solutionsList[0].solutionMatrix[j, i] + " ");
                            }
                            tw.WriteLine();
                        }
                    }
                    int scoreBest = loonfunction.CalculateScoreWithTurnNew(bestSolution);
                    Console.WriteLine(bestscore);
                }
                //until the best solution is found
            }

        }
        private static void WriteToExcel(List<string> bestSolutionsListWrite, int executionTime, string excelFileToWrite)
        {
            FileInfo excelFile = new FileInfo(excelFileToWrite);
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {

                var headerRow = new List<string[]>()
                {
                new string[] { "p1", "p2", "p3" }
                };
                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets["First"];
                if (executionTime == 0)
                {
                    worksheet = excel.Workbook.Worksheets["First"];
                }
                else if (executionTime == 1)
                {
                    worksheet = excel.Workbook.Worksheets["Second"];
                }
                else if (executionTime == 2)
                {
                    worksheet = excel.Workbook.Worksheets["Third"];
                }
                else if (executionTime == 3)
                {
                    worksheet = excel.Workbook.Worksheets["Fourth"];
                }
                else if (executionTime == 4)
                {
                    worksheet = excel.Workbook.Worksheets["Fifth"];
                }
                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays( headerRow);
                worksheet.Cells["A2"].LoadFromCollection(bestSolutionsListWrite);
                excel.Save();
            }
        }
        private static void WriteSolutionToList(int bestScore, List<string> bestSolutionScoreToWrite)
        {
            bestSolutionScoreToWrite.Add(bestScore.ToString());
        }
    }
}
