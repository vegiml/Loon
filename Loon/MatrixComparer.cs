﻿using Loon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loon
{
    public class MatrixComparer : IEqualityComparer<int [,]>
    {
        public bool Equals(int[,] x, int[,] y)
        {
            var equal =
            x.Rank == y.Rank &&
            Enumerable.Range(0, x.Rank).All(dimension => x.GetLength(dimension) == y.GetLength(dimension)) &&
            x.Cast<int>().SequenceEqual(y.Cast<int>());
            return equal;
        }
        public int GetHashCode(int [,] obj)
        {
            return base.GetHashCode();
        }
    }
}
