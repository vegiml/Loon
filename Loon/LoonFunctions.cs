using Loon.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loon
{
    public class LoonFunctions
    {
        public InputInstance instance;
        private Random randomNumberGenerator;
        private int[] possibilites = { 1, 0, -1 };
        private Dictionary<Point, List<Coordinate>> scoreDictionary;
        private int[,] scoreMatrix;
        Dictionary<Point, List<int>> coverage;
        public LoonFunctions(InputInstance instance)
        {
            this.instance = instance;
            coverage = new Dictionary<Point, List<int>>();
            randomNumberGenerator = new Random();
            scoreMatrix = new int[instance.NrRows, instance.NrColumns];
            scoreDictionary = new Dictionary<Point, List<Coordinate>>();
        }
        public int ColumnDist(int c1, int c2)
        {
            return Math.Min(Math.Abs(c1 - c2), instance.NrColumns - Math.Abs(c1 - c2));
        }
        /// <summary>
        /// This function calculates coordinates of target cells that are covered from the position of the baloon with baloonPosition coordinate.
        /// </summary>
        /// <param name="targetCells"></param>
        /// <param name="radius"></param>
        /// <param name="nrRows"></param>
        /// <param name="nrColumns"></param>
        /// <param name="baloonPosition"></param>


        public List<Coordinate> CalculateCoveredTargetCells(Coordinate baloonPosition)
        {
            //for a baloon positioned at r,c  u,v is covered if (r − u)2 + (columndist(c,v))2 ≤ V 2
            List<Coordinate> coveredCoordinates = new List<Coordinate>();
            foreach (var targetCell in this.instance.TargetCells)
            {
                if (((Math.Pow(baloonPosition.XCoordinate - targetCell.XCoordinate, 2)) + (Math.Pow(ColumnDist(baloonPosition.YCoordinate, targetCell.YCoordinate), 2))) <= Math.Pow(this.instance.Radius, 2))
                {
                    coveredCoordinates.Add(targetCell);
                }
            }
            return coveredCoordinates;
        }
        /// <summary>
        /// This function calculates coordinates of target cells that are covered from the position of the baloon with baloonPosition coordinate.
        /// </summary>
        /// <param name="targetCells"></param>
        /// <param name="radius"></param>
        /// <param name="nrRows"></param>
        /// <param name="nrColumns"></param>
        /// <param name="baloonPosition"></param>
        public List<Coordinate> CalculateUnCoveredTargetCells(Coordinate baloonPosition)
        {
            //for a baloon positioned at r,c  u,v is covered if (r − u)2 + (columndist(c,v))2 ≤ V 2
            List<Coordinate> uncoveredCoordinates = new List<Coordinate>();
            foreach (var targetCell in this.instance.TargetCells)
            {
                if (!((Math.Pow(baloonPosition.XCoordinate - targetCell.XCoordinate, 2) + Math.Pow(Math.Min(Math.Abs(baloonPosition.YCoordinate - targetCell.YCoordinate), this.instance.NrColumns - Math.Abs(baloonPosition.YCoordinate - targetCell.YCoordinate)), 2)) <= Math.Pow(this.instance.Radius, 2)))
                {
                    uncoveredCoordinates.Add(targetCell);
                }
            }
            return uncoveredCoordinates;
        }
        /// <summary>
        /// Calculates new Position of the baloon based on the wind data on that baloon coordinate and altitude
        /// </summary>
        /// <param name="nrColumns"></param>
        /// <param name="baloonLocation"></param>
        /// <param name="windDirection"></param>
        public Coordinate ChangeBaloonCoordinate(Coordinate windDirection, Coordinate baloonLocation)
        {
            Coordinate newCoordinate = new Coordinate(0, 0);
            newCoordinate.YCoordinate = (baloonLocation.YCoordinate + windDirection.YCoordinate + this.instance.NrColumns) % this.instance.NrColumns;
            //while (newCoordinate.YCoordinate < 0)
            //{
            //    newCoordinate.YCoordinate = instance.NrColumns + newCoordinate.YCoordinate;
            //}

            newCoordinate.XCoordinate = (baloonLocation.XCoordinate + windDirection.XCoordinate);
            return newCoordinate;
        }
        /// <summary>
        /// This method compares the provided two coordinates coordinate1 and coordinate2 and returns if they are equal or not.
        /// </summary>
        /// <param name="coordinate1"></param>
        /// <param name="coordinate2"></param>
        public bool CompareCoordinates(Coordinate coordinate1, Coordinate coordinate2)
        {
            if ((coordinate1.XCoordinate == coordinate2.XCoordinate) && (coordinate1.YCoordinate == coordinate2.YCoordinate))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Calculates the number of distinct target cells that are covered per turn
        /// </summary>
        /// <param name="coveredTargetCells"></param>
        public int CalculateCoveredDistinctTargetCells(List<Coordinate> coveredTargetCells)
        {
            var distinctCoveredTargetCells = coveredTargetCells.Select(x => new { x.XCoordinate, x.YCoordinate })
                                                                .Distinct()
                                                                .ToList();
            return distinctCoveredTargetCells.Count;
        }
        public List<Coordinate> CalculateCoveredDistinctTargetCells2(List<Coordinate> coveredTargetCells)
        {
            var distinctCoveredTargetCells = coveredTargetCells.Select(x => new Coordinate() { XCoordinate = x.XCoordinate, YCoordinate = x.YCoordinate })
                                                                .Distinct().ToList();
            return distinctCoveredTargetCells;
        }
        public List<Coordinate> TargetCellsCoveredForRow(int[] balloonsAltitudes, Coordinate[] balloonsCoordinates, int NrBaloons, int baloonIndex)
        {
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            for (int i = 0; i < baloonIndex; i++)
            {
                if (i != baloonIndex && balloonsAltitudes[i] != 0 && balloonsCoordinates[i].XCoordinate > -1 && balloonsCoordinates[i].XCoordinate < instance.NrRows)
                {
                    Coordinate windData = this.instance.WindData[balloonsAltitudes[i] - 1, balloonsCoordinates[i].XCoordinate, balloonsCoordinates[i].YCoordinate];
                    Coordinate newBalloonCoordinate = ChangeBaloonCoordinate(windData, balloonsCoordinates[i]);

                    targetCellsCovered.AddRange(CalculateCoveredTargetCells(newBalloonCoordinate));
                }
            }
            return targetCellsCovered;
        }
        public int[] getDimension(int[,] array, int index, int dimension)
        {
            int[] row = new int[array.GetLength(dimension)];
            if (dimension < 2 && dimension >= 0)
            {
                for (int i = 0; i < row.Length; i++)
                {
                    if (dimension == 0)
                    {
                        row[i] = array[i, index];


                    }
                    else
                    {
                        row[i] = array[index, i];

                    }
                }
            }
            return row;
        }
        public Coordinate[] GetCoordinateRow(Coordinate[,] balloonsCoordinate, int index)
        {
            Coordinate[] coordinate = new Coordinate[balloonsCoordinate.GetLength(1)];
            int rowLength = balloonsCoordinate.GetLength(1);
            for (int i = 0; i < rowLength; i++)
            {
                coordinate[i] = balloonsCoordinate[index, i];
            }
            return coordinate;
        }
        public Coordinate[] GetPreviousTurnCoordinate(Coordinate[,] balloonsCoordinate, int index)
        {
            Coordinate[] coordinate = new Coordinate[balloonsCoordinate.GetLength(1)];
            int rowLength = balloonsCoordinate.GetLength(1);

            for (int i = 0; i < rowLength; i++)
            {
                if (index == 0)
                {
                    coordinate[i] = new Coordinate();

                }
                else
                {
                    coordinate[i] = balloonsCoordinate[index - 1, i];

                }

            }
            return coordinate;
        }
        public Coordinate[] GetPreviousTurnCoordinate(Coordinate[,] balloonsCoordinate, int index, Solution solution)
        {
            Coordinate[] coordinate = new Coordinate[balloonsCoordinate.GetLength(1)];
            int rowLength = balloonsCoordinate.GetLength(1);
            Coordinate windData;
            for (int i = 0; i < rowLength; i++)
            {
                if (getDimension(solution.solutionMatrix, i, 0).Take(index).Sum() == 0)
                {
                    if (solution.solutionMatrix[index, i] == 1)
                    {
                        windData = instance.WindData[0, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        coordinate[i] = ChangeBaloonCoordinate(windData, instance.StartCell);
                    }
                    else
                    {
                        coordinate[i] = new Coordinate();
                    }
                }
                else
                {
                    coordinate[i] = balloonsCoordinate[index - 1, i];
                }
            }
            return coordinate;
        }
        //Fitnes function calculation
        public int SolutionScore(int[,] solution, Coordinate startingCell, string caller)
        {
            int[] balloonsAltitude = new int[solution.GetLength(1)];
            balloonsAltitude.Populate(0);
            Coordinate[,] balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
            List<Coordinate> targetCellsCovered;
            int score = 0;
            for (int i = 0; i < solution.GetLength(0); i++)
            {
                targetCellsCovered = new List<Coordinate>();
                for (int j = 0; j < solution.GetLength(1); j++)
                {
                    Coordinate lastCoordinate = new Coordinate();
                    if (getDimension(solution, j, 0).Take(i).Sum() == 0)
                    {
                        if (getDimension(solution, j, 0).Take(i + 1).Sum() == 1)
                        {
                            balloonsCoordinate[i, j] = startingCell;
                            lastCoordinate = balloonsCoordinate[i, j];
                            balloonsAltitude[j] = solution[i, j];
                        }

                    }
                    else if (i != 0 && balloonsAltitude[j] != 0)
                    {
                        lastCoordinate = balloonsCoordinate[i - 1, j];
                        balloonsAltitude[j] = balloonsAltitude[j] + solution[i, j];
                    }
                    if (balloonsAltitude[j] > 0)
                    {
                        if (lastCoordinate.XCoordinate < 0 || lastCoordinate.XCoordinate >= instance.NrRows)
                        {
                            balloonsCoordinate[i, j] = lastCoordinate;
                        }
                        else
                        {
                            Coordinate windData = instance.WindData[balloonsAltitude[j] - 1, lastCoordinate.XCoordinate, lastCoordinate.YCoordinate];
                            balloonsCoordinate[i, j] = ChangeBaloonCoordinate(windData, lastCoordinate);
                            if (balloonsCoordinate[i, j].XCoordinate < 0 || balloonsCoordinate[i, j].XCoordinate >= instance.NrRows)
                            {
                                //do nothing
                            }
                            else
                            {
                                targetCellsCovered.AddRange(CalculateCoveredTargetCells(balloonsCoordinate[i, j]));


                            }
                        }

                    }

                }
                List<Coordinate> uncoveredTargetCells = UncoveredTargetCells(targetCellsCovered);
                List<Coordinate> distinctTargetcelsss = CalculateCoveredDistinctTargetCells2(targetCellsCovered);
                for (int q = 0; q < instance.TargetCells.Count; q++)
                {
                    if (distinctTargetcelsss.Exists(a => a.XCoordinate == instance.TargetCells[q].XCoordinate && a.YCoordinate == instance.TargetCells[q].YCoordinate))
                    {
                        instance.TargetCells[q].CoveredTimes += 1;
                    }
                }
                int scoreturn = 0;
                for (int qk = 0; qk < instance.TargetCells.Count; qk++)
                {
                    scoreturn += instance.TargetCells[qk].CoveredTimes;
                }
                score += CalculateCoveredDistinctTargetCells(targetCellsCovered);

            }
            int scoreCOvered = 0;
            for (int km = 0; km < instance.TargetCells.Count; km++)
            {
                scoreCOvered += instance.TargetCells[km].CoveredTimes;
                instance.TargetCells[km].CoveredTimes = 0;
            }
            List<Coordinate> coveredTargetCells = instance.TargetCells.Where(a => a.CoveredTimes < (instance.NrTurns / 2) && a.CoveredTimes != 0).ToList();
            List<Coordinate> coveredTargetCells2 = instance.TargetCells.Where(a => a.CoveredTimes == 0).ToList();
            List<Coordinate> coveredTargetCells3 = instance.TargetCells.Where(a => a.CoveredTimes > (instance.NrTurns / 2)).ToList();
            List<Coordinate> coveredTargetCells4 = instance.TargetCells.Where(a => a.CoveredTimes == 0).ToList();




            return score;


        }
        public int SolutionScoreLoon(int[,] solution, Coordinate startingCell, string caller, int loonIndex, int turnTo = 0)
        {
            if (turnTo == 0)
            {
                turnTo = solution.GetLength(0);
            }
            int[] balloonsAltitude = new int[solution.GetLength(1)];
            balloonsAltitude.Populate(0);
            Coordinate[,] balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
            List<Coordinate> targetCellsCovered;
            List<Coordinate> targetCellsCoveredLoon;

            int score = 0;
            for (int i = 0; i < turnTo; i++)
            {
                targetCellsCovered = new List<Coordinate>();
                targetCellsCoveredLoon = new List<Coordinate>();
                for (int j = 0; j < solution.GetLength(1); j++)
                {
                    Coordinate lastCoordinate = new Coordinate();
                    if (getDimension(solution, j, 0).Take(i).Sum() == 0)
                    {
                        if (getDimension(solution, j, 0).Take(i + 1).Sum() == 1)
                        {
                            balloonsCoordinate[i, j] = startingCell;
                            lastCoordinate = balloonsCoordinate[i, j];
                            balloonsAltitude[j] = solution[i, j];
                        }

                    }
                    else if (i != 0 && balloonsAltitude[j] != 0)
                    {
                        lastCoordinate = balloonsCoordinate[i - 1, j];
                        balloonsAltitude[j] = balloonsAltitude[j] + solution[i, j];
                    }
                    if (balloonsAltitude[j] > 0)
                    {
                        if (lastCoordinate.XCoordinate < 0 || lastCoordinate.XCoordinate >= instance.NrRows)
                        {
                            balloonsCoordinate[i, j] = lastCoordinate;
                        }
                        else
                        {
                            Coordinate windData = instance.WindData[balloonsAltitude[j] - 1, lastCoordinate.XCoordinate, lastCoordinate.YCoordinate];
                            balloonsCoordinate[i, j] = ChangeBaloonCoordinate(windData, lastCoordinate);
                            if (balloonsCoordinate[i, j].XCoordinate < 0 || balloonsCoordinate[i, j].XCoordinate >= instance.NrRows)
                            {
                                //do nothing
                            }
                            else
                            {
                                if (j == loonIndex)
                                {
                                    targetCellsCoveredLoon.AddRange(CalculateCoveredTargetCells(balloonsCoordinate[i, j]));
                                }
                                else
                                {
                                    targetCellsCovered.AddRange(CalculateCoveredTargetCells(balloonsCoordinate[i, j]));

                                }


                            }
                        }

                    }

                }
                List<Coordinate> distincTargetCellsLoon = new List<Coordinate>();
                foreach (var item in targetCellsCoveredLoon)
                {
                    if (targetCellsCovered.Where(a => a.XCoordinate == item.XCoordinate && a.YCoordinate == item.YCoordinate).FirstOrDefault() == null)
                    {
                        distincTargetCellsLoon.Add(item);
                    }
                }
                if (caller == "a")
                {
                    if (i == turnTo - 1)
                    {
                        score += distincTargetCellsLoon.Count;

                    }

                }
                else
                {
                    score += distincTargetCellsLoon.Count;

                }
            }

            return score;


        }
        public int[] SolutionScoreLoon2(int[,] solution, Coordinate startingCell, string caller, int turnTo = 0)
        {
            if (turnTo == 0)
            {
                turnTo = solution.GetLength(0);
            }
            int[] balloonsAltitude = new int[solution.GetLength(1)];
            balloonsAltitude.Populate(0);
            Coordinate[,] balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
            List<Coordinate> targetCellsCovered;
            int[] loonScores = new int[instance.NrBaloons];
            int score = 0;
            for (int i = 0; i < turnTo; i++)
            {
                foreach (var item in instance.TargetCells)
                {
                    item.isCovered = false;
                }
                targetCellsCovered = new List<Coordinate>();
                for (int j = 0; j < solution.GetLength(1); j++)
                {
                    targetCellsCovered = new List<Coordinate>();
                    Coordinate lastCoordinate = new Coordinate();
                    if (getDimension(solution, j, 0).Take(i).Sum() == 0)
                    {
                        if (getDimension(solution, j, 0).Take(i + 1).Sum() == 1)
                        {
                            balloonsCoordinate[i, j] = startingCell;
                            lastCoordinate = balloonsCoordinate[i, j];
                            balloonsAltitude[j] = solution[i, j];
                        }

                    }
                    else if (i != 0 && balloonsAltitude[j] != 0)
                    {
                        lastCoordinate = balloonsCoordinate[i - 1, j];
                        balloonsAltitude[j] = balloonsAltitude[j] + solution[i, j];
                    }
                    if (balloonsAltitude[j] > 0)
                    {
                        if (lastCoordinate.XCoordinate < 0 || lastCoordinate.XCoordinate >= instance.NrRows)
                        {
                            balloonsCoordinate[i, j] = lastCoordinate;
                        }
                        else
                        {
                            Coordinate windData = instance.WindData[balloonsAltitude[j] - 1, lastCoordinate.XCoordinate, lastCoordinate.YCoordinate];
                            balloonsCoordinate[i, j] = ChangeBaloonCoordinate(windData, lastCoordinate);
                            if (balloonsCoordinate[i, j].XCoordinate < 0 || balloonsCoordinate[i, j].XCoordinate >= instance.NrRows)
                            {
                                //do nothing
                            }
                            else
                            {

                                targetCellsCovered.AddRange(CalculateCoveredTargetCells(balloonsCoordinate[i, j]));
                            }
                        }

                    }
                    loonScores[j] += targetCellsCovered.Where(a => a.isCovered == false).Count();
                    foreach (var item in targetCellsCovered)
                    {
                        instance.TargetCells.Where(a => a.XCoordinate == item.XCoordinate && a.YCoordinate == item.YCoordinate).FirstOrDefault().isCovered = true;
                    }
                }



            }
            foreach (var item in instance.TargetCells)
            {
                item.isCovered = false;
            }
            return loonScores;


        }


        public void mutate(Solution solution, InputInstance instance, double explorationRate, double percentageOfBalloonsToChange)
        {
            double percentageOfCellsToChange;
            int numberOfRowsSolution;
            int numberOfColumnsSolution;
            int numberOfCellsToBeChanged;
            int randomTurn;
            int[] randomTurnArray = new int[solution.solutionMatrix.GetLength(1)];
            int[] baloonsAltiduesOnTurn = new int[solution.solutionMatrix.GetLength(1)];
            int turnArrayScore;
            int numberOfBaloonsToChangeAltitude;
            Coordinate[] turnBaloonCoordinate = new Coordinate[solution.solutionMatrix.GetLength(1)];
            int[] possibilities = { 1, 0, -1 };
            int randomNumber = randomNumberGenerator.Next(1, 101);
            int baloonAltitude;
            int[] balloonSolution;
            int XRandomCoordinate;
            int YRandomCoordinate;
            int oldSolutionValue;
            int newSolutionValue;
            int oldBaloonValue;
            int turnArrayScoreAfterChange;
            bool isTurnScoreChecked;
            int numberOfCells;
            int altitude;
            if (randomNumber <= (explorationRate * 100))
            {

                percentageOfCellsToChange = (((double)randomNumberGenerator.Next(4, 5)) / 100);
                numberOfRowsSolution = solution.solutionMatrix.GetLength(0);
                numberOfColumnsSolution = solution.solutionMatrix.GetLength(1);
                numberOfCells = (numberOfRowsSolution) * (numberOfColumnsSolution);
                double result = ((double)((numberOfRowsSolution) * (numberOfColumnsSolution) * percentageOfCellsToChange));
                //numberOfCellsToBeChanged = randomNumberGenerator.Next(10, 15);
                numberOfCellsToBeChanged = Convert.ToInt32(result);

                int i = 0;
                while (i < numberOfCellsToBeChanged)
                {
                    XRandomCoordinate = randomNumberGenerator.Next(1, solution.solutionMatrix.GetLength(0));
                    YRandomCoordinate = randomNumberGenerator.Next(0, solution.solutionMatrix.GetLength(1));
                    oldSolutionValue = solution.solutionMatrix[XRandomCoordinate, YRandomCoordinate];
                    newSolutionValue = possibilities[randomNumberGenerator.Next(0, possibilities.Length)];
                    while ((oldSolutionValue == newSolutionValue) && ((i == 0) && (newSolutionValue == -1)))
                    {
                        newSolutionValue = possibilities[randomNumberGenerator.Next(0, possibilities.Length)];
                    }

                    solution.solutionMatrix[XRandomCoordinate, YRandomCoordinate] = newSolutionValue;
                    balloonSolution = getDimension(solution.solutionMatrix, YRandomCoordinate, 0);
                    baloonAltitude = calculateAltitude(balloonSolution, instance.Altitudes);

                    //if (i == 0 && solution.solutionMatrix[XRandomCoordinate, YRandomCoordinate] == -1)
                    //{
                    //    solution.solutionMatrix[XRandomCoordinate, YRandomCoordinate] = oldSolutionValue;
                    //}
                    //else
                    //{
                    if ((baloonAltitude > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
                    {
                        //can enter infinit loop
                        solution.solutionMatrix[XRandomCoordinate, YRandomCoordinate] = oldSolutionValue;
                    }
                    else
                    {
                        i++;
                        UpdateBaloonCoordinate(solution.balloonsCoordinate, XRandomCoordinate, solution.solutionMatrix, "mutate");
                    }
                    //}
                }

            }
            else
            {
                randomTurn = randomNumberGenerator.Next(0, solution.solutionMatrix.GetLength(0));

                randomTurnArray = getDimension(solution.solutionMatrix, randomTurn, 1);
                numberOfBaloonsToChangeAltitude = Convert.ToInt32(Math.Round((double)(percentageOfBalloonsToChange * solution.solutionMatrix.GetLength(1))));
                bool[] baloonsChanged = new bool[instance.NrBaloons];
                int randomBaloonIndex;
                bool cellChanged = false;

                int iterator = 0;
                int i = 0;
                while (i < numberOfBaloonsToChangeAltitude)
                {
                    cellChanged = false;

                    if (getDimension(solution.solutionMatrix, iterator, 0).Take(i).Sum() == 0 && solution.solutionMatrix[i, iterator] == 0)
                    {
                        checkIfBaloonNeedsToLunch(solution, iterator, i);
                    }
                    else
                    {
                        //isTurnScoreChecked = Convert.ToBoolean(randomNumberGenerator.Next(0, 2));
                        isTurnScoreChecked = true;
                        randomBaloonIndex = randomNumberGenerator.Next(0, instance.NrBaloons);
                        while (baloonsChanged[randomBaloonIndex] != false)
                        {
                            randomBaloonIndex = randomNumberGenerator.Next(0, instance.NrBaloons);
                        }
                        int numro = 0;
                        foreach (var possibility in possibilities)
                        {
                            oldBaloonValue = solution.solutionMatrix[randomTurn, randomBaloonIndex];
                            if (possibility != oldBaloonValue)
                            {
                                numro++;
                                solution.solutionMatrix[randomTurn, randomBaloonIndex] = possibility;
                                balloonSolution = getDimension(solution.solutionMatrix, randomBaloonIndex, 0);
                                altitude = calculateAltitude(balloonSolution, instance.Altitudes);
                                //check if it breaks constraints
                                if ((altitude > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
                                {
                                    solution.solutionMatrix[randomTurn, randomBaloonIndex] = oldBaloonValue;
                                }
                                else
                                {
                                    solution.solutionMatrix[randomTurn, randomBaloonIndex] = oldBaloonValue;
                                    //TODO: ndryshoje pjesen me poshte sikur te metoda mutate_loon_route
                                    if (isTurnScoreChecked)
                                    {
                                        baloonsAltiduesOnTurn = getBaloonAltitudes(solution.solutionMatrix, randomTurn);
                                        turnBaloonCoordinate = GetPreviousTurnCoordinate(solution.balloonsCoordinate, randomTurn);
                                        turnArrayScore = getTurnScore(randomTurnArray, turnBaloonCoordinate, baloonsAltiduesOnTurn, solution, randomTurn).Score;
                                        randomTurnArray[randomBaloonIndex] = possibility;
                                        solution.solutionMatrix[randomTurn, randomBaloonIndex] = possibility;
                                        //baloonsAltiduesOnTurn = getBaloonAltitudes(solution.solutionMatrix, randomTurn);
                                        //UpdateBaloonCoordinate(solution.balloonsCoordinate, randomTurn, solution.solutionMatrix, "mutate");
                                        turnArrayScoreAfterChange = getTurnScore(randomTurnArray, turnBaloonCoordinate, baloonsAltiduesOnTurn, solution, randomTurn).Score;
                                        randomTurnArray[randomBaloonIndex] = oldBaloonValue;
                                        solution.solutionMatrix[randomTurn, randomBaloonIndex] = oldBaloonValue;
                                        //UpdateBaloonCoordinate(solution.balloonsCoordinate, randomTurn, solution.solutionMatrix, "mutate");
                                    }
                                    else
                                    {
                                        turnArrayScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "mutate");
                                        solution.solutionMatrix[randomTurn, randomBaloonIndex] = possibility;
                                        turnArrayScoreAfterChange = SolutionScore(solution.solutionMatrix, instance.StartCell, "mutate");
                                        solution.solutionMatrix[randomTurn, randomBaloonIndex] = oldBaloonValue;
                                    }
                                    if (turnArrayScore < turnArrayScoreAfterChange || ((turnArrayScore == turnArrayScoreAfterChange) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))))
                                    {
                                        solution.solutionMatrix[randomTurn, randomBaloonIndex] = possibility;
                                        randomTurnArray[randomBaloonIndex] = possibility;
                                        cellChanged = true;
                                        baloonsChanged[randomBaloonIndex] = true;
                                        UpdateBaloonCoordinate(solution.balloonsCoordinate, randomTurn, solution.solutionMatrix, "mutate");
                                        i++;
                                        //break;
                                    }
                                    else
                                    {
                                        solution.solutionMatrix[randomTurn, randomBaloonIndex] = oldBaloonValue;
                                        randomTurnArray[randomBaloonIndex] = oldBaloonValue;
                                    }
                                    //if last element have reached last possibility and the solution does not change, change it
                                    if (cellChanged == false && numro == 2)
                                    {
                                        solution.solutionMatrix[randomTurn, randomBaloonIndex] = possibility;
                                        randomTurnArray[randomBaloonIndex] = possibility;
                                        cellChanged = true;
                                        baloonsChanged[randomBaloonIndex] = true;
                                        UpdateBaloonCoordinate(solution.balloonsCoordinate, randomTurn, solution.solutionMatrix, "mutate");
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    iterator++;
                    if (iterator == solution.solutionMatrix.GetLength(1))
                    {
                        break;
                    }
                }
            }
        }
        /// <summary>
        ///This method makes bigger changes to the solution by choosing  
        ///the number of turns to be changed on a solution
        /// </summary>
        public void mutate_turn(Solution solution, InputInstance instance, double explorationRate, double turnPercentage, double percentageOfBaloons)
        {
            scoreDictionary = new Dictionary<Point, List<Coordinate>>();
            int[] possibilities = { 1, 0, -1 };
            int[] baloonAltitudesOnTurn;
            //percentage = (((double)randomNumberGenerator.Next(10, 15)) / 100);
            int numberOfRowsSolution = solution.solutionMatrix.GetLength(0);
            int numberOfBaloonsSolution = solution.solutionMatrix.GetLength(1);
            int numberOfTurnsToBeChanged = Convert.ToInt32(Math.Round((double)((numberOfRowsSolution) * turnPercentage)));
            int numberOfTurnsToBeChangedLoop = numberOfTurnsToBeChanged;
            //numberOfTurnsToBeChangedLoop = randomNumberGenerator.Next(2, 4);
            int numberOfBaloonsToChangeOnTurn = Convert.ToInt32(Math.Round((double)((numberOfBaloonsSolution) * percentageOfBaloons)));
            //numberOfBaloonsToChangeOnTurn = randomNumberGenerator.Next(4, 6);
            int baloonAltitude;
            int[] turnForModification;
            Coordinate[] turnBaloonCoordinate;
            int turnArrayScore;
            int[] balloonSolution;
            int oldValue;
            int newValue;
            int turnArrayScoreAfterChange;
            bool turnOrOverall;
            bool turnChanged = false;
            bool cellChanged = false;
            //pjesa random
            int randomNumber = randomNumberGenerator.Next(1, 101);
            int altitude = 0;
            if (randomNumber <= (explorationRate * 100))
            {
                for (int i = 0; i <= numberOfTurnsToBeChangedLoop; i++)
                {
                    int j = 0;
                    int iterator = 0;
                    while (j < numberOfBaloonsToChangeOnTurn)
                    {

                        oldValue = solution.solutionMatrix[i, iterator];
                        balloonSolution = getDimension(solution.solutionMatrix, iterator, 0);
                        newValue = possibilities[randomNumberGenerator.Next(0, possibilities.Length)];
                        while (oldValue == newValue)
                        {
                            newValue = possibilities[randomNumberGenerator.Next(0, possibilities.Length)];
                        }
                        solution.solutionMatrix[i, iterator] = newValue;
                        balloonSolution = getDimension(solution.solutionMatrix, iterator, 0);
                        baloonAltitude = calculateAltitude(balloonSolution, instance.Altitudes);
                        if (iterator == 0 && solution.solutionMatrix[i, iterator] == -1)
                        {
                            solution.solutionMatrix[i, iterator] = oldValue;
                        }
                        else
                        {
                            if ((baloonAltitude > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
                            {
                                //can enter into infinit loop 
                                solution.solutionMatrix[i, iterator] = oldValue;
                            }
                            else
                            {
                                j++;
                                UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "mutate_turn");
                            }
                        }
                        iterator++;
                        if (iterator == solution.solutionMatrix.GetLength(1))
                        {
                            break;
                        }
                    }

                }
            }
            //pjesa e caktimit te ndryshimeve te anetareve te array (turns) ne menyre exploative
            else
            {
                for (int i = solution.solutionMatrix.GetLength(0); i >= numberOfTurnsToBeChangedLoop; i--)
                {
                    turnChanged = false;
                    turnForModification = getDimension(solution.solutionMatrix, i, 1);
                    int iterator = 0;
                    int j = 0;
                    while (j < numberOfBaloonsToChangeOnTurn)
                    {
                        cellChanged = false;
                        if (getDimension(solution.solutionMatrix, iterator, 0).Take(i).Sum() == 0 && solution.solutionMatrix[i, iterator] == 0)
                        {
                            checkIfBaloonNeedsToLunch(solution, iterator, i);
                        }
                        else
                        {
                            //turnOrOverall = Convert.ToBoolean(randomNumberGenerator.Next(0, 2));
                            int numro = 0;
                            foreach (var possibility in possibilities)
                            {
                                oldValue = solution.solutionMatrix[i, iterator];
                                solution.solutionMatrix[i, iterator] = possibility;
                                balloonSolution = getDimension(solution.solutionMatrix, iterator, 0);
                                if ((calculateAltitude(balloonSolution, instance.Altitudes) > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
                                {
                                    solution.solutionMatrix[i, iterator] = oldValue;
                                }
                                else
                                {
                                    solution.solutionMatrix[i, iterator] = oldValue;
                                    if (possibility != oldValue)
                                    {
                                        numro++;
                                        //TODO: ndryshoje pjesen me poshte sikur te metoda mutate_loon_route

                                        turnBaloonCoordinate = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i);
                                        baloonAltitudesOnTurn = getBaloonAltitudes(solution.solutionMatrix, i);
                                        turnArrayScore = getTurnScoreNew(turnForModification, turnBaloonCoordinate, baloonAltitudesOnTurn, solution, i).Score;
                                        turnForModification[iterator] = possibility;
                                        solution.solutionMatrix[i, iterator] = possibility;
                                        //baloonAltitudesOnTurn = getBaloonAltitudes(solution.solutionMatrix, i);
                                        //UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "mutate_turn");
                                        turnArrayScoreAfterChange = getTurnScoreNew(turnForModification, turnBaloonCoordinate, baloonAltitudesOnTurn, solution, i).Score;
                                        turnForModification[iterator] = oldValue;
                                        solution.solutionMatrix[i, iterator] = oldValue;
                                        //UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "mutate_turn");
                                        if (/*turnArrayScore < turnArrayScoreAfterChange ||*/ ((turnArrayScore == turnArrayScoreAfterChange) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))))
                                        {
                                            solution.solutionMatrix[i, iterator] = possibility;
                                            turnForModification[iterator] = possibility;
                                            cellChanged = true;
                                            turnChanged = true;
                                            UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "mutate_turn");
                                            j++;
                                            //break;
                                        }
                                        else
                                        {
                                            turnForModification[iterator] = oldValue;
                                            solution.solutionMatrix[i, iterator] = oldValue;
                                        }
                                        //if last element have reached last possibility and the solution does not change, change it
                                        if (cellChanged == false && numro == 2)
                                        {
                                            solution.solutionMatrix[i, iterator] = possibility;
                                            turnForModification[iterator] = possibility;
                                            cellChanged = true;
                                            turnChanged = true;
                                            UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "mutate_turn");
                                            j++;
                                        }
                                    }
                                }
                            }
                        }
                        iterator++;
                        if (iterator == solution.solutionMatrix.GetLength(1))
                        {
                            break;
                        }
                    }

                }
            }
        }
        public void changeTurnOnSolution(int[,] solutionMatrix, int[] turnArray, int turn)
        {
            for (int i = 0; i < solutionMatrix.GetLength(0); i++)
            {
                solutionMatrix[turn, i] = turnArray[i];
            }
        }

        public void UpdateBaloonCoordinate(Coordinate[,] balloonsCoordinate, int turnNumber, int[,] solutionMatrix, string caller)
        {
            int altitudeOnPreviousTurn;
            int altitude;
            Coordinate newlyGeneratedCoordinate;

            for (int i = (turnNumber); i < instance.NrTurns; i++)
            {
                for (int j = 0; j < instance.NrBaloons; j++)
                {
                    Coordinate windDirection;
                    if ((getDimension(solutionMatrix, j, 0).Take(i).Sum() == 0) && (solutionMatrix[i, j] == 1))
                    {
                        windDirection = instance.WindData[0, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        newlyGeneratedCoordinate = ChangeBaloonCoordinate(windDirection, instance.StartCell);
                        balloonsCoordinate[i, j] = newlyGeneratedCoordinate;

                    }
                    else
                    {
                        if (i != 0)
                        {
                            if (balloonsCoordinate[i - 1, j].XCoordinate >= 0 && balloonsCoordinate[i - 1, j].XCoordinate < instance.NrRows)
                            {
                                newlyGeneratedCoordinate = new Coordinate();
                                altitudeOnPreviousTurn = calculateAltitudeOnGivenTurn(getDimension(solutionMatrix, j, 0), i);
                                altitude = altitudeOnPreviousTurn + solutionMatrix[i, j];
                                if (altitude > 0)
                                {
                                    windDirection = instance.WindData[altitude - 1, balloonsCoordinate[i - 1, j].XCoordinate, balloonsCoordinate[i - 1, j].YCoordinate];
                                    newlyGeneratedCoordinate = ChangeBaloonCoordinate(windDirection, balloonsCoordinate[i - 1, j]);
                                    balloonsCoordinate[i, j] = newlyGeneratedCoordinate;
                                }

                            }
                            else
                            {
                                balloonsCoordinate[i, j] = balloonsCoordinate[i - 1, j];
                            }
                        }
                        else
                        {
                            balloonsCoordinate[i, j] = new Coordinate();
                        }

                    }

                }
            }
        }

        private Coordinate[,] SetBaloonCoordinate(int[,] solutionMatrix)
        {
            int[] baloonSolution = new int[solutionMatrix.GetLength(0)];
            Coordinate[,] baloonCoordinates = new Coordinate[solutionMatrix.GetLength(0), solutionMatrix.GetLength(1)];
            int altitudeOnPreviousTurn;
            int altitude;
            Coordinate newlyGeneratedCoordinate;
            for (int i = 0; i < instance.NrTurns; i++)
            {
                for (int j = 0; j < instance.NrBaloons; j++)
                {
                    baloonSolution = getDimension(solutionMatrix, j, 0);
                    if (calculateAltitudeOnGivenTurn(baloonSolution, i + 1) == 0)
                    {
                        baloonCoordinates[i, j] = new Coordinate(-1, -1);
                    }
                    else
                    {
                        if ((calculateAltitudeOnGivenTurn(baloonSolution, i + 1) == 1) && (calculateAltitudeOnGivenTurn(baloonSolution, i) == 0))
                        {
                            Coordinate windDirection = instance.WindData[0, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                            newlyGeneratedCoordinate = ChangeBaloonCoordinate(windDirection, instance.StartCell);
                            baloonCoordinates[i, j] = newlyGeneratedCoordinate;
                        }
                        else
                        {
                            newlyGeneratedCoordinate = new Coordinate();
                            altitudeOnPreviousTurn = calculateAltitudeOnGivenTurn(getDimension(solutionMatrix, j, 0), i);
                            altitude = altitudeOnPreviousTurn + solutionMatrix[i, j];
                            if (altitude > 0)
                            {
                                if (baloonCoordinates[i - 1, j].XCoordinate >= 0 && baloonCoordinates[i - 1, j].XCoordinate < instance.NrRows)
                                {

                                    Coordinate windDirection = instance.WindData[altitude - 1, baloonCoordinates[i - 1, j].XCoordinate, baloonCoordinates[i - 1, j].YCoordinate];
                                    newlyGeneratedCoordinate = ChangeBaloonCoordinate(windDirection, baloonCoordinates[i - 1, j]);
                                    baloonCoordinates[i, j] = newlyGeneratedCoordinate;
                                }
                                else
                                {

                                    baloonCoordinates[i, j] = baloonCoordinates[i - 1, j];

                                }
                            }
                        }
                    }
                }
            }
            return baloonCoordinates;
        }
        public void mutate_column(int[,] solution1, int column1, int column2)
        {
            int[] column1Array = getDimension(solution1, column1, 0);
            int[] column2Array = getDimension(solution1, column2, 0);
            for (int i = 0; i < solution1.GetLength(0); i++)
            {
                solution1[i, column1] = column2Array[i];
                solution1[i, column2] = column1Array[i];
            }
        }
        public int calculateAltitude(int[] baloonSolution, int maxAltitude)
        {
            int altitude = 0;
            for (int i = 0; i < baloonSolution.Length; i++)
            {
                altitude += baloonSolution[i];
                if (altitude > maxAltitude)
                {
                    return altitude;
                }
            }
            return altitude;
        }
        public int calculateAltitudeOnGivenTurn(int[] baloonSolution, int turnNumber)
        {
            int altitude = 0;
            for (int i = 0; i < turnNumber; i++)
            {
                altitude += baloonSolution[i];
            }
            return altitude;
        }
        //Fitnes function calculation
        public List<Coordinate> UnCoveredTargetCells(int[] solution, Coordinate baloonsCurrentCoordinate)
        {
            int[] balloonsAltitude = new int[solution.GetLength(1)];
            balloonsAltitude.Populate(0);
            Coordinate[] balloonsCoordinate = new Coordinate[instance.NrBaloons];
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            for (int i = 0; i < solution.GetLength(0); i++)
            {

                Coordinate lastCoordinate = new Coordinate();
                if (i == 0 && solution[i] == 1)
                {
                    balloonsCoordinate[i] = baloonsCurrentCoordinate;
                    lastCoordinate = balloonsCoordinate[i];
                    balloonsAltitude[i] = solution[i];
                }
                else if (i != 0 && balloonsAltitude[i] != 0)
                {
                    lastCoordinate = balloonsCoordinate[i - 1];
                    balloonsAltitude[i] = balloonsAltitude[i] + solution[i];
                }
                if (lastCoordinate.XCoordinate < 0 || lastCoordinate.XCoordinate >= instance.NrRows)
                {
                    balloonsCoordinate[i] = lastCoordinate;
                    continue;
                }
                Coordinate windData = instance.WindData[balloonsAltitude[i] - 1, lastCoordinate.XCoordinate, lastCoordinate.YCoordinate];
                balloonsCoordinate[i] = ChangeBaloonCoordinate(windData, lastCoordinate);
                if (balloonsCoordinate[i].XCoordinate < 0 || balloonsCoordinate[i].XCoordinate >= instance.NrRows)
                {
                    continue;
                }
                targetCellsCovered.AddRange(CalculateUnCoveredTargetCells(balloonsCoordinate[i]));
            }
            return targetCellsCovered;
        }
        public ScoreObject getTurnScore(int[] turn, Coordinate[] previousbaloonCoordinates, int[] ballonAltitudes, Solution solution, int turnNumber)
        {
            ScoreObject scoreObject = new ScoreObject();
            int score = 0;
            int altitude;
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            Coordinate windData;
            for (int i = 0; i < turn.Length; i++)
            {
                Coordinate newBaloonCoordinate = new Coordinate();
                altitude = ballonAltitudes[i] + turn[i];
                if (altitude > 0)
                {
                    if ((getDimension(solution.solutionMatrix, i, 0).Take(turnNumber).Sum() == 0) && (altitude == 1))
                    {
                        windData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        newBaloonCoordinate = ChangeBaloonCoordinate(windData, instance.StartCell);
                    }
                    else
                    {
                        if (previousbaloonCoordinates[i].XCoordinate < 0 || previousbaloonCoordinates[i].XCoordinate >= instance.NrRows)
                        {
                            newBaloonCoordinate = previousbaloonCoordinates[i];
                        }
                        else
                        {
                            windData = instance.WindData[altitude - 1, previousbaloonCoordinates[i].XCoordinate, previousbaloonCoordinates[i].YCoordinate];
                            newBaloonCoordinate = ChangeBaloonCoordinate(windData, previousbaloonCoordinates[i]);

                        }
                    }
                    if (newBaloonCoordinate.XCoordinate < 0 || newBaloonCoordinate.XCoordinate >= instance.NrRows)
                    {
                        //donothing
                    }
                    else
                    {
                        targetCellsCovered.AddRange(CalculateCoveredTargetCells(newBaloonCoordinate));
                    }
                }
            }
            score = CalculateCoveredDistinctTargetCells(targetCellsCovered);
            scoreObject.Score = score;
            scoreObject.targetCellsCovered = CalculateCoveredDistinctTargetCells2(targetCellsCovered);
            //object targetcellsCovered = CalculateCoveredDistinctTargetCells2(targetCellsCovered);

            return scoreObject;
        }
        public ScoreObject getTurnScore(int[] turn, Coordinate[] baloonCoordinates, int[] ballonAltitudes, Solution solution, int excludedBaloon, int turnNumber)
        {
            ScoreObject scoreObject = new ScoreObject();
            int score = 0;
            int altitude;
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            for (int i = 0; i < turn.Length; i++)
            {
                if (i != excludedBaloon)
                {
                    Coordinate newBaloonCoordinate = new Coordinate();

                    altitude = ballonAltitudes[i] + turn[i];




                    if (altitude > 0)
                    {
                        if (baloonCoordinates[i].XCoordinate < 0 || baloonCoordinates[i].XCoordinate >= instance.NrRows)
                        {
                            //donothing
                        }
                        else
                        {
                            Coordinate windData = instance.WindData[altitude - 1, baloonCoordinates[i].XCoordinate, baloonCoordinates[i].YCoordinate];
                            newBaloonCoordinate = ChangeBaloonCoordinate(windData, baloonCoordinates[i]);

                            if (newBaloonCoordinate.XCoordinate < 0 || newBaloonCoordinate.XCoordinate >= instance.NrRows)
                            {
                                //donothing
                            }
                            else
                            {
                                targetCellsCovered.AddRange(CalculateCoveredTargetCells(newBaloonCoordinate));
                            }
                        }

                    }
                }
            }
            score = CalculateCoveredDistinctTargetCells(targetCellsCovered);
            scoreObject.Score = score;
            object targetCellsCovered1 = CalculateCoveredDistinctTargetCells2(targetCellsCovered);

            return scoreObject;
        }
        //to be used for method mutate_loon_route and improved_mutate_loon_route
        public ScoreObject getTurnScore(int[] turn, Coordinate[] previousbaloonCoordinates, int[] ballonAltitudes, int[] baloonArray, int turnNumber)
        {
            ScoreObject scoreObject = new ScoreObject();
            int score = 0;
            int altitude;
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            Coordinate windData;
            for (int i = 0; i < turn.Length; i++)
            {
                Coordinate newBaloonCoordinate = new Coordinate();
                altitude = ballonAltitudes[i] + turn[i];
                if (altitude > 0)
                {
                    if ((ballonAltitudes[i] == 0) && (altitude == 1))
                    {
                        windData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        newBaloonCoordinate = ChangeBaloonCoordinate(windData, instance.StartCell);
                    }
                    else
                    {
                        if (previousbaloonCoordinates[i].XCoordinate < 0 || previousbaloonCoordinates[i].XCoordinate >= instance.NrRows)
                        {
                            newBaloonCoordinate = previousbaloonCoordinates[i];
                        }
                        else
                        {
                            windData = instance.WindData[altitude - 1, previousbaloonCoordinates[i].XCoordinate, previousbaloonCoordinates[i].YCoordinate];
                            newBaloonCoordinate = ChangeBaloonCoordinate(windData, previousbaloonCoordinates[i]);

                        }
                    }
                    if (newBaloonCoordinate.XCoordinate < 0 || newBaloonCoordinate.XCoordinate >= instance.NrRows)
                    {
                        //donothing
                    }
                    else
                    {
                        targetCellsCovered.AddRange(CalculateCoveredTargetCells(newBaloonCoordinate));
                    }
                }
            }
            score = CalculateCoveredDistinctTargetCells(targetCellsCovered);
            scoreObject.Score = score;
            scoreObject.targetCellsCovered = CalculateCoveredDistinctTargetCells2(targetCellsCovered);
            //object targetcellsCovered = CalculateCoveredDistinctTargetCells2(targetCellsCovered);

            return scoreObject;
        }
        public int[] getBaloonAltitudes(int[,] solution, int turn)
        {
            int[] baloonAltitudes = new int[solution.GetLength(1)];
            baloonAltitudes.Populate(0);
            for (int i = 0; i < turn; i++)
            {
                for (int j = 0; j < solution.GetLength(1); j++)
                {
                    baloonAltitudes[j] = baloonAltitudes[j] + solution[i, j];
                }
            }
            return baloonAltitudes;
        }
        public double DistanceBetweenBaloonCoordinateAndTargetCells(Coordinate baloonCoordinate, List<Coordinate> uncoveredTargetCells)
        {
            double distance = 0;
            foreach (var item in uncoveredTargetCells)
            {
                distance += Math.Sqrt(Math.Pow(baloonCoordinate.XCoordinate - item.XCoordinate, 2) + Math.Pow(baloonCoordinate.YCoordinate - item.YCoordinate, 2));
            }
            return distance;
        }
        public Coordinate[] calculateBaloonCoordinate(int[,] solution, Coordinate startingCell, int turn)
        {
            Coordinate baloonCoordinateStartCell = startingCell;
            Coordinate[,] baloonCoordinates = new Coordinate[solution.GetLength(0), solution.GetLength(1)];
            int altitude = 0;
            Coordinate[] baloonCordiantesOnTurn = new Coordinate[solution.GetLength(1)];
            Coordinate windData = new Coordinate();
            for (int i = 0; i < turn; i++)
            {
                for (int j = 0; j < solution.GetLength(1); j++)
                {
                    altitude = calculateAltitudeOnGivenTurn(getDimension(solution, j, 0), i);


                    if (i == 0 && altitude == 0)
                    {
                        if (solution[i, j] == 1)
                        {
                            windData = instance.WindData[altitude, baloonCoordinateStartCell.XCoordinate, baloonCoordinateStartCell.YCoordinate];
                            Coordinate baloonCoordinateAfterWind = ChangeBaloonCoordinate(windData, baloonCoordinateStartCell);
                            baloonCoordinates[i, j] = baloonCoordinateAfterWind;
                        }
                        else
                        {
                            baloonCoordinates[i, j] = new Coordinate(0, 0);
                        }

                    }
                    else
                    {
                        altitude = altitude + solution[i, j];
                        windData = instance.WindData[altitude - 1, baloonCoordinates[i - 1, j].XCoordinate, baloonCoordinates[i - 1, j].YCoordinate];
                        Coordinate baloonCoordinateAfterWind = ChangeBaloonCoordinate(windData, baloonCoordinates[i - 1, j]);
                        baloonCoordinates[i, j] = baloonCoordinateAfterWind;
                    }



                }
            }
            baloonCordiantesOnTurn = GetCoordinateRow(baloonCoordinates, turn - 1);
            return baloonCordiantesOnTurn;
        }
        /// <summary>
        /// Swaps the rows of the solution
        /// </summary>
        /// <param name="solution"></param>

        public void swap_turns(Solution solution, int turn1, int turn2)
        {
            int[] turn1Array = getDimension(solution.solutionMatrix, turn1, 1);
            int[] turn2Array = getDimension(solution.solutionMatrix, turn2, 1);
            int turn1OldValue;
            int turn2OldValue;
            int[] balloonSolution;
            int altitude;
            for (int i = 0; i < solution.solutionMatrix.GetLength(1); i++)
            {
                turn1OldValue = solution.solutionMatrix[turn1, i];
                turn2OldValue = solution.solutionMatrix[turn2, i];
                balloonSolution = getDimension(solution.solutionMatrix, i, 0);
                solution.solutionMatrix[turn1, i] = turn2Array[i];
                solution.solutionMatrix[turn2, i] = turn1Array[i];
                balloonSolution = getDimension(solution.solutionMatrix, i, 0);
                altitude = calculateAltitude(balloonSolution, instance.Altitudes);
                if (altitude > instance.Altitudes)
                {
                    Console.WriteLine();
                }
                if (turn1 < turn2)
                {
                    if ((altitude > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
                    {
                        if (altitude > 6)
                        {

                        }
                        solution.solutionMatrix[turn1, i] = turn1OldValue;
                        solution.solutionMatrix[turn2, i] = turn2OldValue;
                        continue;
                    }
                }
                else
                {
                    if ((altitude > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
                    {
                        if (altitude > instance.Altitudes)
                        {
                            Console.WriteLine();

                        }
                        solution.solutionMatrix[turn1, i] = turn1OldValue;
                        solution.solutionMatrix[turn2, i] = turn2OldValue;
                        continue;
                    }
                }
            }
            if (turn1 <= turn2)
            {

                UpdateBaloonCoordinate(solution.balloonsCoordinate, turn1, solution.solutionMatrix, "swap_turn");
            }
            else
            {

                UpdateBaloonCoordinate(solution.balloonsCoordinate, turn2, solution.solutionMatrix, "swapTurn");
            }
        }
        public bool checkIfBreaksLoonConstraints(int[] balloonSolution)
        {
            bool result = false;
            int altitude = 0;
            for (int i = 0; i < balloonSolution.Length; i++)
            {
                altitude += balloonSolution[i];
                if ((altitude == 0 && balloonSolution.Take(i).Sum() > 0) || (altitude < 0))
                {
                    return true;
                }
            }
            return result;
        }
        public List<Solution> tournamentSelection(int tournamentSize, List<Solution> solutions)
        {
            List<Solution> tournamnetList = new List<Solution>();
            int randomIndex;
            Solution tournamentSolution;
            bool[] indexesOccured = new bool[solutions.Count];
            for (int i = 0; i < tournamentSize; i++)
            {
                tournamentSolution = new Solution(instance.NrRows, instance.NrColumns);
                randomIndex = randomNumberGenerator.Next(0, solutions.Count);
                while (indexesOccured[randomIndex])
                {
                    randomIndex = randomNumberGenerator.Next(0, solutions.Count);
                }
                indexesOccured[randomIndex] = true;
                tournamentSolution = solutions[randomIndex];
                tournamnetList.Add(tournamentSolution);
            }
            return tournamnetList;
        }
        public List<Solution> crossoverOperator(Solution firstParent, Solution secondParent)
        {
            int ballonAltitude;
            int[] balloonSolution = new int[firstParent.solutionMatrix.GetLength(0)];
            List<Solution> solution = new List<Solution>();
            Solution firstChild = new Solution(firstParent.solutionMatrix.GetLength(0), firstParent.solutionMatrix.GetLength(1));
            Solution secondChild = new Solution(secondParent.solutionMatrix.GetLength(0), secondParent.solutionMatrix.GetLength(1));
            //int splitPoint = firstParent.solutionMatrix.GetLength(1) / 2;
            //random split point, not always splitting in half
            int splitPoint = randomNumberGenerator.Next(1, firstParent.solutionMatrix.GetLength(1));

            int[] fristSolutionRow = new int[firstParent.solutionMatrix.GetLength(1)];
            int[] secondSolutionRow = new int[secondParent.solutionMatrix.GetLength(1)];

            for (int i = 0; i < firstParent.solutionMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < firstParent.solutionMatrix.GetLength(1); j++)
                {
                    if (j >= 0 && j < splitPoint)
                    {
                        firstChild.solutionMatrix[i, j] = firstParent.solutionMatrix[i, j];
                        secondChild.solutionMatrix[i, j] = secondParent.solutionMatrix[i, j];
                    }
                    else if (j >= splitPoint && j < firstParent.solutionMatrix.GetLength(1))
                    {
                        firstChild.solutionMatrix[i, j] = secondParent.solutionMatrix[i, j];
                        secondChild.solutionMatrix[i, j] = firstParent.solutionMatrix[i, j];
                    }
                }
            }
            firstChild.balloonsCoordinate = SetBaloonCoordinate(firstChild.solutionMatrix);
            secondChild.balloonsCoordinate = SetBaloonCoordinate(firstChild.solutionMatrix);
            //shume vonesa
            firstChild.solutionScore = CalculateScoreWithTurnNew(firstChild);
            secondChild.solutionScore = CalculateScoreWithTurnNew(secondChild);

            solution.Add(firstChild);
            solution.Add(secondChild);
            //pjesa me poshet mund te fshihet per shkak se nuk nevoje kontrollat me poshte!!

            //bool breaksConstraintsFirstChild = false;
            //for (int i = 0; i < firstChild.solutionMatrix.GetLength(1); i++)
            //{
            //    balloonSolution = getDimension(firstChild.solutionMatrix, i, 0);
            //    ballonAltitude = calculateAltitude(balloonSolution, instance.Altitudes);
            //    if ((ballonAltitude > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
            //    {
            //        breaksConstraintsFirstChild = true;
            //    }

            //}
            ////fristSolutionRow = getDimension(firstChild.solutionMatrix, 0, 1);
            ////if (checkFirstSolutionRow(fristSolutionRow))
            ////{
            ////    breaksConstraintsFirstChild = true;

            ////}

            //if (!breaksConstraintsFirstChild)
            //{
            //    firstChild.balloonsCoordinate = SetBaloonCoordinate(firstChild.solutionMatrix);
            //    firstChild.solutionScore = SolutionScore(firstChild.solutionMatrix, instance.StartCell, "crossover");
            //}
            //bool breaksConstraintsSecondChild = false;
            //for (int i = 0; i < secondChild.solutionMatrix.GetLength(1); i++)
            //{
            //    balloonSolution = getDimension(secondChild.solutionMatrix, i, 0);
            //    ballonAltitude = calculateAltitude(balloonSolution, instance.Altitudes);

            //    if ((ballonAltitude > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
            //    {
            //        breaksConstraintsSecondChild = true;
            //    }

            //}
            ////secondSolutionRow = getDimension(secondChild.solutionMatrix, 0, 1);
            ////if (checkFirstSolutionRow(secondSolutionRow))
            ////{
            ////    breaksConstraintsSecondChild = true;

            ////}
            //if (!breaksConstraintsSecondChild)
            //{
            //    secondChild.balloonsCoordinate = SetBaloonCoordinate(secondChild.solutionMatrix);
            //    secondChild.solutionScore = SolutionScore(secondChild.solutionMatrix, instance.StartCell, "crossover");
            //}
            //if (breaksConstraintsFirstChild)
            //{
            //    solution.Add(firstParent);

            //}
            //else
            //{
            //    solution.Add(firstChild);

            //}
            //if (breaksConstraintsSecondChild)
            //{
            //    solution.Add(secondParent);

            //}
            //else
            //{
            //    solution.Add(secondChild);

            //}
            return solution;
        }
        public Solution rankBasedSelection(List<Solution> solutions)
        {
            List<Solution> sortedSolutions = solutions.OrderByDescending(a => a.solutionScore).ToList();
            double[] ranks = new double[sortedSolutions.Count];
            int sumOfAllRanks = Enumerable.Range(1, sortedSolutions.Count).Sum();
            Random randomNumberGen = new Random();
            double randomNumber;
            double rank;
            Solution parent = new Solution(solutions[0].solutionMatrix.GetLength(0), solutions[0].solutionMatrix.GetLength(1));
            randomNumber = randomNumberGen.NextDouble() * (101 - 0) + 0;
            bool hasSolution = false;
            while (randomNumber > (double)100)
            {
                randomNumber = randomNumberGen.NextDouble() * (101 - 0) + 0;
            }
            for (int i = 0; i < sortedSolutions.Count; i++)
            {
                rank = ((((double)sortedSolutions.Count - (double)i) / ((double)sumOfAllRanks)) * ((double)100));
                ranks[i] = rank;
                if (i == 0)
                {
                    if (randomNumber >= 0 && randomNumber <= ranks[i])
                    {
                        parent = sortedSolutions.ElementAt(i);

                        hasSolution = true;
                    }
                }
                else
                {
                    if ((randomNumber >= ranks.Take(i).Sum() && (randomNumber <= (ranks.Take(i + 1).Sum()))))
                    {
                        parent = sortedSolutions.ElementAt(i);
                        hasSolution = true;
                    }
                }
            }
            //if (!hasSolution)
            //{
            //    int q = 2222;
            //}
            return parent;

        }
        public bool checkFirstSolutionRow(int[] solutionFirstRow)
        {
            for (int i = 0; i < solutionFirstRow.Length; i++)
            {
                if (solutionFirstRow[i] == -1)
                {
                    return true;
                }
            }
            return false;
        }
        public List<Solution> generateInitialPopulation(InputInstance instance, int popSize)
        {
            int[] ballonAltitudes = new int[instance.NrBaloons];
            ballonAltitudes.Populate<int>(0);
            List<Solution> solutionsList = new List<Solution>();
            int[] possibilities = new int[] { 0, -1, 1 };
            Coordinate[,] balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
            int[,] solution;
            int randomNumberOfBalloons = instance.NrBaloons;
            int randomBaloonIndex;
            Coordinate startWindData;
            Coordinate startNewCoordiante;
            Coordinate newCoordinate;
            Coordinate coordinateToSet;
            Coordinate windData;
            Coordinate previousTurnCoordiante;
            HashSet<int[,]> solutions = new HashSet<int[,]>(new MatrixComparer());
            int altitudeAfterAdjustment;
            int posibilityScoreMax;
            int altitudeAdjustment;
            bool takeOptimalSolution = false;
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            Coordinate[] balloonCoordinate;
            List<Coordinate> targetCellsCoveredFromOtherBalloons;
            List<int> bestSolutions = new List<int>();
            int distinctTargetCellsCovered;
            int randomAltitude;
            bool lunchBaloon = false;
            List<int> possibilityValues;
            for (int pop = 0; pop < popSize; pop++)
            {
                balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
                balloonsCoordinate.Populate(new Coordinate());
                solution = new int[instance.NrTurns, instance.NrBaloons];
                int randomNumnerOfBaloonsToLunch = randomNumberGenerator.Next(instance.NrBaloons / 4, instance.NrBaloons);
                int m = 0;
                while (m < randomNumnerOfBaloonsToLunch)
                {
                    randomBaloonIndex = randomNumberGenerator.Next(0, instance.NrBaloons);
                    if (ballonAltitudes[randomBaloonIndex] == 0)
                    {
                        solution[0, randomBaloonIndex] = 1;
                        ballonAltitudes[randomBaloonIndex] = 1;
                        startWindData = instance.WindData[0, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        startNewCoordiante = ChangeBaloonCoordinate(startWindData, instance.StartCell);
                        balloonsCoordinate[0, randomBaloonIndex] = startNewCoordiante;
                        m++;
                    }
                }
                //}
                //generate turn 1 to end, random initial solution
                for (int j = 1; j < solution.GetLength(0); j++)
                {
                    for (int i = 0; i < randomNumberOfBalloons; i++)
                    {

                        newCoordinate = new Coordinate();
                        posibilityScoreMax = 0;
                        altitudeAdjustment = 0;
                        coordinateToSet = new Coordinate();
                        possibilityValues = new List<int>();
                        //ne rast se nuk plotesohen asnjera nga kushtet me poshte altitude adjustment mbetet 0
                        if (ballonAltitudes[i] != 0)
                        {
                            if (balloonsCoordinate[j - 1, i].XCoordinate >= 0 && balloonsCoordinate[j - 1, i].XCoordinate < instance.NrRows)
                            {
                                windData = instance.WindData[ballonAltitudes[i] - 1, balloonsCoordinate[j - 1, i].XCoordinate, balloonsCoordinate[j - 1, i].YCoordinate];
                                coordinateToSet = ChangeBaloonCoordinate(windData, balloonsCoordinate[j - 1, i]);

                            }

                        }
                        //add initial coordinate value if coordinate 
                        //is not set below for specific constraints
                        int solutionCount = 0;
                        previousTurnCoordiante = balloonsCoordinate[j - 1, i];
                        if (((previousTurnCoordiante.XCoordinate < 0) || (previousTurnCoordiante.XCoordinate >= instance.NrRows)) && (ballonAltitudes[i] != 0))
                        {
                            newCoordinate = previousTurnCoordiante;
                            coordinateToSet = newCoordinate;
                        }
                        else
                        {
                            while (solutionCount < possibilities.Length)
                            {
                                randomAltitude = possibilities[randomNumberGenerator.Next(0, 3)];
                                //randomAltitude = possibilities[solutionCount];
                                while (possibilityValues.Exists(a => a.Equals(randomAltitude)))
                                {
                                    randomAltitude = possibilities[randomNumberGenerator.Next(0, 3)];
                                }
                                altitudeAfterAdjustment = ballonAltitudes[i] + randomAltitude;
                                possibilityValues.Add(randomAltitude);
                                if ((altitudeAfterAdjustment > instance.Altitudes) || (ballonAltitudes[i] != 0 && altitudeAfterAdjustment < 1) || (altitudeAfterAdjustment < 0))
                                {
                                    //continue;
                                    //do nothing

                                }
                                else if (altitudeAfterAdjustment > 0)
                                {
                                    windData = new Coordinate();
                                    if (altitudeAfterAdjustment == 1 && ballonAltitudes[i] == 0)
                                    {
                                        lunchBaloon = Convert.ToBoolean(randomNumberGenerator.Next(0, 2));
                                        lunchBaloon = Convert.ToBoolean(randomNumberGenerator.Next(0, 2));
                                        if (lunchBaloon)
                                        {
                                            balloonsCoordinate[j, i] = instance.StartCell;
                                            windData = instance.WindData[altitudeAfterAdjustment - 1, balloonsCoordinate[j, i].XCoordinate, balloonsCoordinate[j, i].YCoordinate];
                                            newCoordinate = ChangeBaloonCoordinate(windData, balloonsCoordinate[j, i]);
                                            //this two variables are set to soluion after break
                                            coordinateToSet = newCoordinate;
                                            altitudeAdjustment = altitudeAfterAdjustment;
                                        }
                                        else
                                        {
                                            coordinateToSet = new Coordinate();
                                            altitudeAdjustment = 0;
                                        }

                                        break;
                                    }
                                    else
                                    {
                                        windData = instance.WindData[altitudeAfterAdjustment - 1, previousTurnCoordiante.XCoordinate, previousTurnCoordiante.YCoordinate];
                                        newCoordinate = ChangeBaloonCoordinate(windData, previousTurnCoordiante);
                                        if (newCoordinate.XCoordinate < 0 || newCoordinate.XCoordinate >= instance.NrRows)
                                        {
                                            if (solutionCount == 2)
                                            {
                                                altitudeAdjustment = randomAltitude;
                                                coordinateToSet = newCoordinate;
                                            }
                                        }
                                        else
                                        {
                                            altitudeAdjustment = randomAltitude;
                                            coordinateToSet = newCoordinate;
                                            break;

                                        }
                                    }

                                }
                                solutionCount++;
                            }
                            ballonAltitudes[i] = ballonAltitudes[i] + altitudeAdjustment;
                            balloonsCoordinate[j, i] = coordinateToSet;
                            solution[j, i] = altitudeAdjustment;
                        }
                    }

                }
                if (!(solutions.Add(solution)))
                {
                    popSize = popSize + 1;
                    ballonAltitudes.Populate(0);
                    continue;
                }
                else
                {
                    Solution solutionObject = new Solution(instance.NrRows, instance.NrColumns);
                    solutionObject.solutionMatrix = solution;
                    solutionObject.balloonsCoordinate = balloonsCoordinate;
                    solutionObject.solutionScore = CalculateScoreWithTurnNew(solutionObject);
                    //foreach (var item in solutionsList)
                    //{
                    //    Console.WriteLine("Solution distance is " + solutionDistance(solutionObject.solutionMatrix, item.solutionMatrix));
                    //}
                    //Console.WriteLine("end of line");

                    solutionsList.Add(solutionObject);
                }
                ballonAltitudes.Populate(0);
            }

            //for (int i = 0; i < solutionsList.Count; i++)
            //{
            //    int randomNumberOfLoonsToChangerRouteInitial = randomNumberGenerator.Next(1, instance.NrBaloons);
            //    //int randomNumberOfLoonsToChangerRoute = 1;
            //    int loonIndexIntial = 0;
            //    while (loonIndexIntial < randomNumberOfLoonsToChangerRouteInitial)
            //    {
            //        solutionsList[i] = mutate_loon_route_combine(solutionsList[i], instance, loonIndexIntial, true);
            //        loonIndexIntial++;
            //    }
            //}
            //for (int i = 0; i < solutionsList.Count; i++)
            //{
            //    var solutionItem = solutionsList[i];
            //    //second turn firs loon coordinate ]
            //    for (int k = 0; k < solutionsList[i].solutionMatrix.GetLength(0) - 1; k++)
            //    {
            //        for (int j = 0; j < solutionsList[i].solutionMatrix.GetLength(1) - 1; j++)
            //        {
            //            var firstCoordinate = solutionItem.balloonsCoordinate[k + 1, 0];
            //            var nextLoonCoordinate = solutionItem.balloonsCoordinate[k + 1, j + 1];
            //            var distance = DistanceBetweenTwoPoints(firstCoordinate, nextLoonCoordinate);
            //            Console.WriteLine();
            //        }
            //    }

            //}
            return solutionsList;

        }
        public List<Solution> generateInitialPopulationExploative(InputInstance instance, int popSize)
        {
            int[] ballonAltitudes = new int[instance.NrBaloons];
            ballonAltitudes.Populate<int>(0);
            List<Solution> solutionsList = new List<Solution>();
            int[] possibilities = new int[] { 0, -1, 1 };
            Coordinate[,] balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
            int[,] solution;
            int randomNumberOfBalloons = instance.NrBaloons;
            int randomBaloonIndex;
            Coordinate startWindData;
            Coordinate startNewCoordiante;
            Coordinate newCoordinate;
            Coordinate coordinateToSet;
            Coordinate windData;
            Coordinate previousTurnCoordiante;
            HashSet<int[,]> solutions = new HashSet<int[,]>(new MatrixComparer());
            int altitudeAfterAdjustment;
            int posibilityScoreMax;
            int altitudeAdjustment;
            bool takeOptimalSolution = false;
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            Coordinate[] balloonCoordinate;
            List<Coordinate> targetCellsCoveredFromOtherBalloons;
            List<int> bestSolutions = new List<int>();
            int distinctTargetCellsCovered;
            int randomAltitude;
            bool lunchBaloon = false;
            List<int> possibilityValues;
            //random indexes of loons to be routed to a group of target cells, 
            //other part to another group of target cells

            for (int pop = 0; pop < popSize; pop++)
            {
                balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
                balloonsCoordinate.Populate(new Coordinate());
                solution = new int[instance.NrTurns, instance.NrBaloons];
                int randomNumnerOfBaloonsToLunch = randomNumberGenerator.Next(instance.NrBaloons / 4, instance.NrBaloons);
                int m = 0;
                while (m < randomNumnerOfBaloonsToLunch)
                {
                    randomBaloonIndex = randomNumberGenerator.Next(0, instance.NrBaloons);
                    if (ballonAltitudes[randomBaloonIndex] == 0)
                    {
                        solution[0, randomBaloonIndex] = 1;
                        ballonAltitudes[randomBaloonIndex] = 1;
                        startWindData = instance.WindData[0, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        startNewCoordiante = ChangeBaloonCoordinate(startWindData, instance.StartCell);
                        balloonsCoordinate[0, randomBaloonIndex] = startNewCoordiante;
                        m++;
                    }
                }
                //}
                //generate turn 1 to end, random initial solution
                for (int j = 1; j < solution.GetLength(0); j++)
                {
                    for (int i = 0; i < randomNumberOfBalloons; i++)
                    {

                        newCoordinate = new Coordinate();
                        posibilityScoreMax = 0;
                        altitudeAdjustment = 0;
                        coordinateToSet = new Coordinate();
                        possibilityValues = new List<int>();
                        //ne rast se nuk plotesohen asnjera nga kushtet me poshte altitude adjustment mbetet 0
                        if (ballonAltitudes[i] != 0)
                        {
                            if (balloonsCoordinate[j - 1, i].XCoordinate >= 0 && balloonsCoordinate[j - 1, i].XCoordinate < instance.NrRows)
                            {
                                windData = instance.WindData[ballonAltitudes[i] - 1, balloonsCoordinate[j - 1, i].XCoordinate, balloonsCoordinate[j - 1, i].YCoordinate];
                                coordinateToSet = ChangeBaloonCoordinate(windData, balloonsCoordinate[j - 1, i]);

                            }

                        }
                        //add initial coordinate value if coordinate 
                        //is not set below for specific constraints
                        int solutionCount = 0;
                        previousTurnCoordiante = balloonsCoordinate[j - 1, i];
                        if (((previousTurnCoordiante.XCoordinate < 0) || (previousTurnCoordiante.XCoordinate >= instance.NrRows)) && (ballonAltitudes[i] != 0))
                        {
                            newCoordinate = previousTurnCoordiante;
                            coordinateToSet = newCoordinate;
                        }
                        else
                        {
                            while (solutionCount < possibilities.Length)
                            {
                                randomAltitude = possibilities[randomNumberGenerator.Next(0, 3)];
                                //randomAltitude = possibilities[solutionCount];
                                while (possibilityValues.Exists(a => a.Equals(randomAltitude)))
                                {
                                    randomAltitude = possibilities[randomNumberGenerator.Next(0, 3)];
                                }
                                altitudeAfterAdjustment = ballonAltitudes[i] + randomAltitude;
                                possibilityValues.Add(randomAltitude);
                                if ((altitudeAfterAdjustment > instance.Altitudes) || (ballonAltitudes[i] != 0 && altitudeAfterAdjustment < 1) || (altitudeAfterAdjustment < 0))
                                {
                                    //continue;
                                    //do nothing

                                }
                                else if (altitudeAfterAdjustment > 0)
                                {
                                    windData = new Coordinate();
                                    if (altitudeAfterAdjustment == 1 && ballonAltitudes[i] == 0)
                                    {
                                        //lunchBaloon = Convert.ToBoolean(randomNumberGenerator.Next(0,2));
                                        lunchBaloon = Convert.ToBoolean(randomNumberGenerator.Next(0, 2));
                                        if (lunchBaloon)
                                        {
                                            balloonsCoordinate[j, i] = instance.StartCell;
                                            windData = instance.WindData[altitudeAfterAdjustment - 1, balloonsCoordinate[j, i].XCoordinate, balloonsCoordinate[j, i].YCoordinate];
                                            newCoordinate = ChangeBaloonCoordinate(windData, balloonsCoordinate[j, i]);
                                            //this two variables are set to soluion after break
                                            coordinateToSet = newCoordinate;
                                            altitudeAdjustment = altitudeAfterAdjustment;
                                        }
                                        else
                                        {
                                            coordinateToSet = new Coordinate();
                                            altitudeAdjustment = 0;
                                        }

                                        break;
                                    }
                                    else
                                    {
                                        windData = instance.WindData[altitudeAfterAdjustment - 1, previousTurnCoordiante.XCoordinate, previousTurnCoordiante.YCoordinate];
                                        newCoordinate = ChangeBaloonCoordinate(windData, previousTurnCoordiante);
                                        if (newCoordinate.XCoordinate < 0 || newCoordinate.XCoordinate >= instance.NrRows)
                                        {
                                            if (solutionCount == 2)
                                            {
                                                altitudeAdjustment = randomAltitude;
                                                coordinateToSet = newCoordinate;
                                            }
                                        }
                                        else
                                        {
                                            altitudeAdjustment = randomAltitude;
                                            coordinateToSet = newCoordinate;
                                            break;

                                        }
                                    }

                                }
                                solutionCount++;
                            }
                            ballonAltitudes[i] = ballonAltitudes[i] + altitudeAdjustment;
                            balloonsCoordinate[j, i] = coordinateToSet;
                            solution[j, i] = altitudeAdjustment;
                        }
                    }

                }
                if (!(solutions.Add(solution)))
                {
                    popSize = popSize + 1;
                    ballonAltitudes.Populate(0);
                    continue;
                }
                else
                {
                    Solution solutionObject = new Solution(instance.NrRows, instance.NrColumns);
                    solutionObject.solutionMatrix = solution;
                    solutionObject.balloonsCoordinate = balloonsCoordinate;
                    //foreach (var item in solutionsList)
                    //{
                    //    Console.WriteLine("Solution distance is " + solutionDistance(solutionObject.solutionMatrix, item.solutionMatrix));
                    //}
                    //Console.WriteLine("end of line");

                    solutionsList.Add(solutionObject);
                }
                ballonAltitudes.Populate(0);
            }
            for (int i = 0; i < solutionsList.Count; i++)
            {
                var solutionItem = solutionsList[i];
                //second turn firs loon coordinate ]
                for (int k = 0; k < solutionsList[i].solutionMatrix.GetLength(0) - 1; k++)
                {
                    for (int j = 0; j < solutionsList[i].solutionMatrix.GetLength(1) - 1; j++)
                    {
                        var firstCoordinate = solutionItem.balloonsCoordinate[k + 1, 0];
                        var nextLoonCoordinate = solutionItem.balloonsCoordinate[k + 1, j + 1];
                        var distance = DistanceBetweenTwoPoints(firstCoordinate, nextLoonCoordinate);
                        Console.WriteLine();
                    }
                }

            }
            return solutionsList;

        }
        public int solutionDistance(int[,] firstSolution, int[,] secondSolution)
        {
            int distance = 0;
            for (int i = 0; i < firstSolution.GetLength(0); i++)
            {
                for (int j = 0; j < secondSolution.GetLength(1); j++)
                {
                    if (firstSolution[i, j] != secondSolution[i, j])
                    {
                        distance++;
                    }
                }
            }
            return distance;
        }
        public List<Coordinate> UncoveredTargetCells(List<Coordinate> targetcellsCovered)
        {
            List<Coordinate> targetCellsUncovered = new List<Coordinate>();
            foreach (var item in instance.TargetCells)
            {
                if (targetcellsCovered.Where(a => a.XCoordinate == item.XCoordinate && a.YCoordinate == item.YCoordinate).FirstOrDefault() == null)
                {
                    targetCellsUncovered.Add(item);
                }
            }
            return targetCellsUncovered;
        }
        public void mutate_loon_route(Solution solution, InputInstance instance, int baloonIndex, int turnToStart = 0)
        {
            //int turnToStart = randomNumberGenerator.Next(solution.solutionMatrix.GetLength(1) / 2, solution.solutionMatrix.GetLength(1));
            //int turnToStart = 0;
            int[] turnArray = new int[solution.solutionMatrix.GetLength(1)];
            int[] baloonArray = new int[solution.solutionMatrix.GetLength(0)];
            int bestScoreValue, bestAltitude;
            int baloonAltitudeAfterChange;
            int[] baloonTurnArray;
            //ScoreObject turnScoreWithoutBaloon;
            ScoreObject turnScoreWithBaloon;
            Coordinate[] baloonCoordinatesOnTurn;
            int[] baloonAltitudes;
            int altitude = 0;
            Coordinate winData;
            int[] baloonArrayBeforeChange = getDimension(solution.solutionMatrix, baloonIndex, 0);
            //var watch = System.Diagnostics.Stopwatch.StartNew();

            //ne fund te kesaj loop ne bestAltitude vendose vleren e altitude e cila maksimizon score 
            //duke marre parasyshe target cells te cilat jane te mbuluara nga balonat tjera
            //for (int j = turnToStart; j < solution.solutionMatrix.GetLength(0); j++)
            //{
            //    for (int i = 0; i < solution.solutionMatrix.GetLength(1); i++)
            //    {
            //        solution.solutionMatrix[j, i] = 0;
            //        solution.balloonsCoordinate[j, i] = new Coordinate();
            //    }

            //}
            for (int j = turnToStart; j < solution.solutionMatrix.GetLength(0); j++)
            {

                solution.solutionMatrix[j, baloonIndex] = 0;
                solution.balloonsCoordinate[j, baloonIndex] = new Coordinate();

            }
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");
            solution.solutionScore = 0;
            //te merret parasyshe edhe i+1, te
            for (int i = turnToStart; i < solution.solutionMatrix.GetLength(0); i++)
            {

                baloonTurnArray = getDimension(solution.solutionMatrix, i, 1);
                //kjo metode overloaded GetPreviousTurnCoordinate duhet te perdoret edhe ne metodat tjera qe thirret
                baloonCoordinatesOnTurn = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i);
                baloonAltitudes = getBaloonAltitudes(solution.solutionMatrix, i);
                //turnScoreWithoutBaloon = getTurnScore(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, solution, baloonIndex);
                bestAltitude = 0;
                bestScoreValue = 0;
                //covered target cells by other baloons
                foreach (var possibility in possibilites)
                {
                    baloonArray[i] = possibility;
                    baloonTurnArray[baloonIndex] = possibility;
                    baloonAltitudeAfterChange = calculateAltitude(baloonArray, instance.Altitudes);
                    //check bounds 1 -> A
                    //perdore logjiken e njejte edhe ne metodat tjera
                    if (!(baloonAltitudeAfterChange > instance.Altitudes) && (!(checkIfBreaksLoonConstraints(baloonArray))))
                    {

                        turnScoreWithBaloon = getTurnScoreNew(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, baloonArray, i);

                        if ((turnScoreWithBaloon.Score > bestScoreValue) || ((i != 0) && (turnScoreWithBaloon.Score == bestScoreValue) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))))
                        {
                            bestScoreValue = turnScoreWithBaloon.Score;
                            bestAltitude = possibility;
                        }
                        //kalkulo score ne turn-in momental 
                    }
                }
                //if (baloonArray.Take(i).Sum() == 0 && bestScoreValue == 0)
                //{
                //    bestAltitude = 1;
                //}
                solution.solutionMatrix[i, baloonIndex] = bestAltitude;
                baloonArray[i] = bestAltitude;
                altitude = baloonAltitudes[baloonIndex] + bestAltitude;
                solution.solutionScore += bestScoreValue;
                if ((getDimension(solution.solutionMatrix, baloonIndex, 0).Take(i).Sum() == 0) && (altitude == 1))
                {
                    winData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                    solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, instance.StartCell);
                }
                else
                {
                    //loon ka shku jashte gridit

                    if (i != 0)
                    {
                        if (baloonCoordinatesOnTurn[baloonIndex].XCoordinate < 0 || baloonCoordinatesOnTurn[baloonIndex].XCoordinate >= instance.NrRows)
                        {
                            solution.balloonsCoordinate[i, baloonIndex] = solution.balloonsCoordinate[i - 1, baloonIndex];
                        }
                        else
                        {
                            if (altitude != 0)
                            {
                                winData = instance.WindData[altitude - 1, baloonCoordinatesOnTurn[baloonIndex].XCoordinate, baloonCoordinatesOnTurn[baloonIndex].YCoordinate];
                                solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, baloonCoordinatesOnTurn[baloonIndex]);
                            }
                        }
                    }
                }
                //UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "muatate_loon_route");
                //caktoje altitude 

            }
            //watch.Stop();
            //var elapsedMs = watch.ElapsedMilliseconds;
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");

        }
        public void mutate_loon_route_random(Solution solution, InputInstance instance, int baloonIndex, int turnToStart = 0)
        {
            //int turnToStart = randomNumberGenerator.Next(solution.solutionMatrix.GetLength(1) / 2, solution.solutionMatrix.GetLength(1));
            //int turnToStart = 0;
            int[] turnArray = new int[solution.solutionMatrix.GetLength(1)];
            int[] baloonArray = new int[solution.solutionMatrix.GetLength(0)];
            //int bestScoreValue, bestAltitude;
            int baloonAltitudeAfterChange;
            int[] baloonTurnArray;
            //ScoreObject turnScoreWithoutBaloon;
            ScoreObject turnScoreWithBaloon;
            Coordinate[] baloonCoordinatesOnTurn;
            int[] baloonAltitudes;
            int altitude = 0;
            Coordinate winData;
            int[] baloonArrayBeforeChange = getDimension(solution.solutionMatrix, baloonIndex, 0);
            for (int j = turnToStart; j < solution.solutionMatrix.GetLength(0); j++)
            {

                solution.solutionMatrix[j, baloonIndex] = 0;
                solution.balloonsCoordinate[j, baloonIndex] = new Coordinate();

            }
            int possibilitySet = 0;
            for (int i = turnToStart; i < solution.solutionMatrix.GetLength(0); i++)
            {
                baloonCoordinatesOnTurn = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i);
                baloonAltitudes = getBaloonAltitudes(solution.solutionMatrix, i);
                int possibility = 0;
                while (true)
                {
                    possibility = possibilites[randomNumberGenerator.Next(0, 3)];
                    baloonArray[i] = possibility;
                    baloonAltitudeAfterChange = calculateAltitude(baloonArray, instance.Altitudes);
                    if (!(baloonAltitudeAfterChange > instance.Altitudes) && (!(checkIfBreaksLoonConstraints(baloonArray))))
                    {

                        possibilitySet = possibility;
                        solution.solutionMatrix[i, baloonIndex] = possibility;
                        baloonArray[i] = possibility;
                        break;
                    }
                }
                if (baloonArray.Take(i).Sum() == 0 && Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))
                {
                    possibilitySet = 1;
                    solution.solutionMatrix[i, baloonIndex] = possibilitySet;
                    baloonArray[i] = possibilitySet;
                }

                altitude = baloonAltitudes[baloonIndex] + possibilitySet;
                if ((getDimension(solution.solutionMatrix, baloonIndex, 0).Take(i).Sum() == 0) && (altitude == 1))
                {
                    winData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                    solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, instance.StartCell);
                }
                else
                {
                    //loon ka shku jashte gridit

                    if (i != 0)
                    {
                        if (baloonCoordinatesOnTurn[baloonIndex].XCoordinate < 0 || baloonCoordinatesOnTurn[baloonIndex].XCoordinate >= instance.NrRows)
                        {
                            solution.balloonsCoordinate[i, baloonIndex] = solution.balloonsCoordinate[i - 1, baloonIndex];
                        }
                        else
                        {
                            if (altitude != 0)
                            {
                                winData = instance.WindData[altitude - 1, baloonCoordinatesOnTurn[baloonIndex].XCoordinate, baloonCoordinatesOnTurn[baloonIndex].YCoordinate];
                                solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, baloonCoordinatesOnTurn[baloonIndex]);
                            }
                        }
                    }
                }
                //UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "muatate_loon_route");
                //caktoje altitude 

            }
            //watch.Stop();
            //var elapsedMs = watch.ElapsedMilliseconds;
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");

        }
        //TODO: Shperndaji loons me shume 
        public void improved_mutate_loon_route(Solution solution, InputInstance instance, int baloonIndex, int turnToStart = 0)
        {
            //int turnToStart = randomNumberGenerator.Next(solution.solutionMatrix.GetLength(1) / 2, solution.solutionMatrix.GetLength(1));
            //int turnToStart = 0;
            int[] turnArray = new int[solution.solutionMatrix.GetLength(1)];
            int[] baloonArray = new int[solution.solutionMatrix.GetLength(0)];
            int bestScoreValue, bestAltitude;
            int baloonAltitudeAfterChange;
            int[] baloonTurnArray;
            //ScoreObject turnScoreWithoutBaloon;
            ScoreObject turnScoreWithBaloon;
            Coordinate[] baloonCoordinatesOnTurn;
            int[] baloonAltitudes;
            int altitude = 0;
            int altitude2 = 0;

            Coordinate winData;
            int[] baloonArrayBeforeChange = getDimension(solution.solutionMatrix, baloonIndex, 0);
            int altitudeAdjustment = 0;
            Coordinate[] baloonCoordinatesOnTurn2;
            int[] baloonAltitudes2;
            int[] baloonTurnArray2;
            ScoreObject turnScoreWithBaloon2;
            int bestAltitude2;
            Coordinate oldCoordinateValue;
            int oldAltitudeValue;
            int totalScore;
            long elapsedMs = 0;
            //ne fund te kesaj loop ne bestAltitude vendose vleren e altitude e cila maksimizon score 
            //duke marre parasyshe target cells te cilat jane te mbuluara nga balonat tjera
            //for (int j = turnToStart; j < solution.solutionMatrix.GetLength(0); j++)
            //{
            //    for (int i = 0; i < solution.solutionMatrix.GetLength(1); i++)
            //    {
            //        solution.solutionMatrix[j, i] = 0;
            //        solution.balloonsCoordinate[j, i] = new Coordinate();
            //    }

            //}
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for (int j = turnToStart; j < solution.solutionMatrix.GetLength(0); j++)
            {

                solution.solutionMatrix[j, baloonIndex] = 0;
                solution.balloonsCoordinate[j, baloonIndex] = new Coordinate();

            }
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");

            //te merret parasyshe edhe i+1, te caktohet altitude per dy rreshtat e rradhes, pra vlerat optimale duke e marre
            //parasyshe edhe dy rreshtat qe vijne pas
            bool isDividedByTwo = false;
            if (solution.solutionMatrix.GetLength(0) % 2 == 0)
            {
                isDividedByTwo = true;
            }
            solution.solutionScore = 0;
            int count = solution.solutionMatrix.GetLength(0);
            for (int i = turnToStart; i < count; i++)
            {

                if ((isDividedByTwo) || (!(isDividedByTwo) && (i != count - 1)))
                {
                    baloonTurnArray = getDimension(solution.solutionMatrix, i, 1);
                    baloonTurnArray2 = getDimension(solution.solutionMatrix, i + 1, 1);

                    //kjo metode overloaded GetPreviousTurnCoordinate duhet te perdoret edhe ne metodat tjera qe thirret
                    baloonCoordinatesOnTurn = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i);
                    baloonAltitudes = getBaloonAltitudes(solution.solutionMatrix, i);

                    baloonCoordinatesOnTurn2 = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i + 1);
                    baloonAltitudes2 = getBaloonAltitudes(solution.solutionMatrix, i + 1);
                    oldAltitudeValue = baloonAltitudes2[baloonIndex];
                    oldCoordinateValue = new Coordinate();
                    oldCoordinateValue = baloonCoordinatesOnTurn2[baloonIndex];
                    //turnScoreWithoutBaloon = getTurnScore(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, solution, baloonIndex);

                    bestAltitude = 0;
                    bestAltitude2 = 0;
                    bestScoreValue = 0;
                    //covered target cells by other baloons
                    foreach (var possibility in possibilites)
                    {
                        baloonArray[i] = possibility;
                        baloonArray[i + 1] = 0;
                        baloonTurnArray[baloonIndex] = possibility;
                        baloonAltitudeAfterChange = calculateAltitude(baloonArray, instance.Altitudes);

                        if (!(baloonAltitudeAfterChange > instance.Altitudes) && (!(checkIfBreaksLoonConstraints(baloonArray))))
                        {
                            //ndryshoje coordinate dhe altitude paraprake te balones 
                            altitudeAdjustment = baloonAltitudes[baloonIndex] + possibility;
                            if (altitudeAdjustment >= 0)
                            {
                                baloonAltitudes2[baloonIndex] = altitudeAdjustment;
                                if ((baloonArray.Take(i).Sum() == 0) && (altitudeAdjustment == 1))
                                {
                                    winData = instance.WindData[altitudeAdjustment - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                                    baloonCoordinatesOnTurn2[baloonIndex] = ChangeBaloonCoordinate(winData, instance.StartCell);
                                }
                                else if ((baloonArray.Take(i).Sum() == 0) && (altitudeAdjustment == 0))
                                {
                                    baloonCoordinatesOnTurn2[baloonIndex] = new Coordinate();
                                }
                                else
                                {
                                    if (i != 0)
                                    {
                                        //loon ka shku jashte gridit
                                        if (solution.balloonsCoordinate[i - 1, baloonIndex].XCoordinate < 0 || solution.balloonsCoordinate[i - 1, baloonIndex].XCoordinate >= instance.NrRows)
                                        {
                                            baloonCoordinatesOnTurn2[baloonIndex] = solution.balloonsCoordinate[i - 1, baloonIndex];
                                        }
                                        else
                                        {
                                            if (altitudeAdjustment != 0)
                                            {
                                                winData = instance.WindData[altitudeAdjustment - 1, solution.balloonsCoordinate[i - 1, baloonIndex].XCoordinate, solution.balloonsCoordinate[i - 1, baloonIndex].YCoordinate];
                                                baloonCoordinatesOnTurn2[baloonIndex] = ChangeBaloonCoordinate(winData, solution.balloonsCoordinate[i - 1, baloonIndex]);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        baloonCoordinatesOnTurn2[baloonIndex] = new Coordinate();
                                    }

                                }
                                foreach (var possibility2 in possibilites)
                                {
                                    baloonArray[i + 1] = possibility2;
                                    baloonTurnArray2[baloonIndex] = possibility2;
                                    baloonAltitudeAfterChange = calculateAltitude(baloonArray, instance.Altitudes);

                                    //check bounds 1 -> A
                                    //perdore logjiken e njejte edhe ne metodat tjera
                                    if (!(baloonAltitudeAfterChange > instance.Altitudes) && (!(checkIfBreaksLoonConstraints(baloonArray))))
                                    {

                                        turnScoreWithBaloon = getTurnScoreNew(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, baloonArray, i);
                                        turnScoreWithBaloon2 = getTurnScoreNew(baloonTurnArray2, baloonCoordinatesOnTurn2, baloonAltitudes2, baloonArray, i + 1);
                                        totalScore = turnScoreWithBaloon.Score + turnScoreWithBaloon2.Score;
                                        if ((totalScore > bestScoreValue) || ((i != 0) && (totalScore == bestScoreValue) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))))
                                        {
                                            bestScoreValue = totalScore;
                                            bestAltitude = possibility;
                                            bestAltitude2 = possibility2;
                                        }
                                        //kalkulo score ne turn-in momental 
                                    }
                                }
                            }
                        }
                    }
                    baloonAltitudes2[baloonIndex] = oldAltitudeValue;
                    baloonCoordinatesOnTurn2[baloonIndex] = oldCoordinateValue;

                    solution.solutionMatrix[i, baloonIndex] = bestAltitude;
                    solution.solutionMatrix[i + 1, baloonIndex] = bestAltitude2;

                    baloonArray[i] = bestAltitude;
                    baloonArray[i + 1] = bestAltitude2;

                    altitude = baloonAltitudes[baloonIndex] + bestAltitude;
                    altitude2 = altitude + bestAltitude2;
                    solution.solutionScore += bestScoreValue;

                    //frist turn coordinate
                    if ((getDimension(solution.solutionMatrix, baloonIndex, 0).Take(i).Sum() == 0) && (altitude == 1))
                    {
                        winData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, instance.StartCell);
                        baloonCoordinatesOnTurn2[baloonIndex] = solution.balloonsCoordinate[i, baloonIndex];
                    }
                    else
                    {
                        if (i != 0)
                        {
                            //loon ka shku jashte gridit
                            if (baloonCoordinatesOnTurn[baloonIndex].XCoordinate < 0 || baloonCoordinatesOnTurn[baloonIndex].XCoordinate >= instance.NrRows)
                            {
                                solution.balloonsCoordinate[i, baloonIndex] = solution.balloonsCoordinate[i - 1, baloonIndex];
                                baloonCoordinatesOnTurn2[baloonIndex] = solution.balloonsCoordinate[i, baloonIndex];
                            }
                            else
                            {
                                if (altitude != 0)
                                {
                                    winData = instance.WindData[altitude - 1, baloonCoordinatesOnTurn[baloonIndex].XCoordinate, baloonCoordinatesOnTurn[baloonIndex].YCoordinate];
                                    solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, baloonCoordinatesOnTurn[baloonIndex]);
                                    baloonCoordinatesOnTurn2[baloonIndex] = solution.balloonsCoordinate[i, baloonIndex];
                                }
                            }
                        }
                        else
                        {
                            baloonCoordinatesOnTurn2[baloonIndex] = new Coordinate();
                        }
                    }
                    //second turn coordinate
                    if ((getDimension(solution.solutionMatrix, baloonIndex, 0).Take(i + 1).Sum() == 0) && (altitude2 == 1))
                    {
                        winData = instance.WindData[altitude2 - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        solution.balloonsCoordinate[i + 1, baloonIndex] = ChangeBaloonCoordinate(winData, instance.StartCell);
                    }
                    else
                    {
                        if (i + 1 != 0)
                        {
                            if (baloonCoordinatesOnTurn2[baloonIndex].XCoordinate < 0 || baloonCoordinatesOnTurn2[baloonIndex].XCoordinate >= instance.NrRows)
                            {
                                solution.balloonsCoordinate[i + 1, baloonIndex] = solution.balloonsCoordinate[i, baloonIndex];
                            }
                            else
                            {
                                if (altitude2 != 0)
                                {
                                    winData = instance.WindData[altitude2 - 1, baloonCoordinatesOnTurn2[baloonIndex].XCoordinate, baloonCoordinatesOnTurn2[baloonIndex].YCoordinate];
                                    solution.balloonsCoordinate[i + 1, baloonIndex] = ChangeBaloonCoordinate(winData, baloonCoordinatesOnTurn2[baloonIndex]);
                                }
                            }
                        }
                        else
                        {
                            solution.balloonsCoordinate[i + 1, baloonIndex] = new Coordinate();
                        }
                        //loon ka shku jashte gridit
                    }



                    i++;

                }
                else if (i == count - 1)
                {
                    baloonTurnArray = getDimension(solution.solutionMatrix, i, 1);
                    //kjo metode overloaded GetPreviousTurnCoordinate duhet te perdoret edhe ne metodat tjera qe thirret
                    baloonCoordinatesOnTurn = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i);
                    baloonAltitudes = getBaloonAltitudes(solution.solutionMatrix, i);
                    //turnScoreWithoutBaloon = getTurnScore(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, solution, baloonIndex);
                    bestAltitude = 0;
                    bestScoreValue = 0;
                    //covered target cells by other baloons
                    foreach (var possibility in possibilites)
                    {
                        baloonArray[i] = possibility;
                        baloonTurnArray[baloonIndex] = possibility;
                        baloonAltitudeAfterChange = calculateAltitude(baloonArray, instance.Altitudes);
                        //check bounds 1 -> A
                        //perdore logjiken e njejte edhe ne metodat tjera
                        if (!(baloonAltitudeAfterChange > instance.Altitudes) && (!(checkIfBreaksLoonConstraints(baloonArray))))
                        {
                            turnScoreWithBaloon = getTurnScore(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, baloonArray, i);
                            if ((turnScoreWithBaloon.Score > bestScoreValue) || ((i != 0) && (turnScoreWithBaloon.Score == bestScoreValue) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))))
                            {
                                bestScoreValue = turnScoreWithBaloon.Score;
                                bestAltitude = possibility;
                            }
                            //kalkulo score ne turn-in momental 
                        }
                    }
                    solution.solutionMatrix[i, baloonIndex] = bestAltitude;
                    baloonArray[i] = bestAltitude;
                    altitude = baloonAltitudes[baloonIndex] + bestAltitude;
                    if ((getDimension(solution.solutionMatrix, baloonIndex, 0).Take(i).Sum() == 0) && (altitude == 1))
                    {
                        winData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, instance.StartCell);
                    }
                    else
                    {
                        if (i != 0)
                        {
                            if (baloonCoordinatesOnTurn[baloonIndex].XCoordinate < 0 || baloonCoordinatesOnTurn[baloonIndex].XCoordinate >= instance.NrRows)
                            {
                                solution.balloonsCoordinate[i, baloonIndex] = solution.balloonsCoordinate[i - 1, baloonIndex];
                            }
                            else
                            {
                                if (altitude != 0)
                                {
                                    winData = instance.WindData[altitude - 1, baloonCoordinatesOnTurn[baloonIndex].XCoordinate, baloonCoordinatesOnTurn[baloonIndex].YCoordinate];
                                    solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, baloonCoordinatesOnTurn[baloonIndex]);
                                }
                            }
                        }
                        else
                        {
                            solution.balloonsCoordinate[i, baloonIndex] = new Coordinate();
                        }
                        //loon ka shku jashte gridit

                    }
                }

                //UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "muatate_loon_route");
                //caktoje altitude 
                //watch.Stop();
                //elapsedMs = watch.ElapsedMilliseconds;
            }
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
        }
        public bool checkIfNoImprovement(int nrIterationsNoImprovement, List<int> bestSolutions)
        {
            bool result = true;
            int k = 0;
            bestSolutions = bestSolutions.OrderByDescending(a => a).ToList();
            int firstElement = bestSolutions.First();
            for (int i = 0; i < nrIterationsNoImprovement; i++)
            {
                if (firstElement == bestSolutions[i])
                {
                    k++;
                }
            }
            if ((k - 1) == nrIterationsNoImprovement - 1)
            {
                result = false;
            }
            return result;
        }
        //TODO:FINISH THE METHOD
        public Solution mutate_loon_route_combine(Solution solution, InputInstance instance, int baloonIndex, bool exploativeMethod)
        {
            //var watch = System.Diagnostics.Stopwatch.StartNew();
            scoreDictionary = new Dictionary<Point, List<Coordinate>>();
            int firstMutationScore = 0;
            //int secondMutationScore = 0;
            int oldLoonScore = 0;
            int bestMuatationScore = 0;
            oldLoonScore = solution.solutionScore;
            Solution firstMutationSolution = new Solution(instance.NrRows, instance.NrColumns);
            //Solution secondMutationSolution = new Solution(instance.NrRows, instance.NrColumns);
            firstMutationSolution = ExtensionMethods.DeepCopy(solution);
            //secondMutationSolution = ExtensionMethods.DeepCopy(solution);
            if (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))
            {
                //ka shume vonesa
                // the code that you want to measure comes here
                mutate_loon_route(firstMutationSolution, instance, baloonIndex);
                //watch.Stop();
                firstMutationScore = firstMutationSolution.solutionScore;
                //optionaly me lone edhe zgjidhjet e kqija me hy ketu
                if (/*(firstMutationScore >= secondMutationScore) &&*/ (firstMutationScore > oldLoonScore) || ((firstMutationScore == oldLoonScore) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))) || ((firstMutationScore < oldLoonScore) && (exploativeMethod == false)))
                {
                    firstMutationSolution.solutionScore = firstMutationScore;
                    bestMuatationScore = firstMutationScore;
                    solution = (firstMutationSolution);
                }
                else
                {
                    return solution;
                }
            }
            else
            {
                //ka shume vonesa
                // the code that you want to measure comes here
                //mutate_loon_route(firstMutationSolution, instance, baloonIndex);
                improved_mutate_loon_route(firstMutationSolution, instance, baloonIndex);
                //improved_mutate_loon_route(secondMutationSolution, instance, baloonIndex);
                firstMutationScore = firstMutationSolution.solutionScore;
                if (/*(secondMutationScore > firstMutationScore) &&*/ (firstMutationScore > oldLoonScore) || ((firstMutationScore == oldLoonScore) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))) || ((firstMutationScore < oldLoonScore) && (exploativeMethod == false)))
                {
                    firstMutationSolution.solutionScore = firstMutationScore;
                    bestMuatationScore = firstMutationScore;
                    solution = (firstMutationSolution);
                }
                else
                {
                    return solution;
                }

            }
            //me poshte me probabilitet 50 - 50 lehohet qe te merret zgjidhje e re edhe nese eshte e barabarte ose me e keqe se zgjidhja paraprake
            //watch.Stop();
            //var elapsedMs = watch.ElapsedMilliseconds;
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");
            return solution;
        }
        public void mutate_loon_route_loon_score(Solution solution, InputInstance instance, int baloonIndex, int turnToStart = 0)
        {
            //int turnToStart = randomNumberGenerator.Next(solution.solutionMatrix.GetLength(1) / 2, solution.solutionMatrix.GetLength(1));
            //int turnToStart = 0;
            int[] turnArray = new int[solution.solutionMatrix.GetLength(1)];
            int[] baloonArray = new int[solution.solutionMatrix.GetLength(0)];
            int bestScoreValue, bestAltitude;
            int baloonAltitudeAfterChange;
            int[] baloonTurnArray;
            //ScoreObject turnScoreWithoutBaloon;
            ScoreObject turnScoreWithBaloon = new ScoreObject();
            Coordinate[] baloonCoordinatesOnTurn;
            int[] baloonAltitudes;
            int altitude = 0;
            Coordinate winData;
            int[] baloonArrayBeforeChange = getDimension(solution.solutionMatrix, baloonIndex, 0);
            //int turnScore = 0;
            //ne fund te kesaj loop ne bestAltitude vendose vleren e altitude e cila maksimizon score 
            //duke marre parasyshe target cells te cilat jane te mbuluara nga balonat tjera
            //for (int j = turnToStart; j < solution.solutionMatrix.GetLength(0); j++)
            //{
            //    for (int i = 0; i < solution.solutionMatrix.GetLength(1); i++)
            //    {
            //        solution.solutionMatrix[j, i] = 0;
            //        solution.balloonsCoordinate[j, i] = new Coordinate();
            //    }

            //}
            for (int j = turnToStart; j < solution.solutionMatrix.GetLength(0); j++)
            {

                solution.solutionMatrix[j, baloonIndex] = 0;
                solution.balloonsCoordinate[j, baloonIndex] = new Coordinate();

            }
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");

            //te merret parasyshe edhe i+1, te
            for (int i = turnToStart; i < solution.solutionMatrix.GetLength(0); i++)
            {
                baloonTurnArray = getDimension(solution.solutionMatrix, i, 1);
                //kjo metode overloaded GetPreviousTurnCoordinate duhet te perdoret edhe ne metodat tjera qe thirret
                baloonCoordinatesOnTurn = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i);
                baloonAltitudes = getBaloonAltitudes(solution.solutionMatrix, i);
                //turnScoreWithoutBaloon = getTurnScore(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, solution, baloonIndex);
                bestAltitude = 0;
                bestScoreValue = 0;
                //covered target cells by other baloons
                foreach (var possibility in possibilites)
                {
                    baloonArray[i] = possibility;
                    baloonTurnArray[baloonIndex] = possibility;
                    solution.solutionMatrix[i, baloonIndex] = possibility;
                    baloonAltitudeAfterChange = calculateAltitude(baloonArray, instance.Altitudes);
                    //check bounds 1 -> A
                    //perdore logjiken e njejte edhe ne metodat tjera
                    if (!(baloonAltitudeAfterChange > instance.Altitudes) && (!(checkIfBreaksLoonConstraints(baloonArray))))
                    {
                        //turnScore = getTurnScore(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, baloonArray, i).Score;
                        turnScoreWithBaloon.Score = SolutionScoreLoon2(solution.solutionMatrix, instance.StartCell, "a", i + 1)[baloonIndex];
                        if (possibility == 1 && turnScoreWithBaloon.Score == bestScoreValue)
                        {
                            bestScoreValue = turnScoreWithBaloon.Score;
                            bestAltitude = possibility;
                        }
                        else if ((turnScoreWithBaloon.Score > bestScoreValue) || ((i != 0) && (turnScoreWithBaloon.Score == bestScoreValue) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))))
                        {
                            bestScoreValue = turnScoreWithBaloon.Score;
                            bestAltitude = possibility;
                        }
                        //kalkulo score ne turn-in momental 
                    }
                }
                if (baloonArray.Take(i).Sum() == 0 && bestScoreValue == 0)
                {
                    bestAltitude = 1;
                }
                solution.solutionMatrix[i, baloonIndex] = bestAltitude;
                baloonArray[i] = bestAltitude;
                altitude = baloonAltitudes[baloonIndex] + bestAltitude;
                if ((getDimension(solution.solutionMatrix, baloonIndex, 0).Take(i).Sum() == 0) && (altitude == 1))
                {
                    winData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                    solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, instance.StartCell);
                }
                else
                {
                    //loon ka shku jashte gridit

                    if (i != 0)
                    {
                        if (baloonCoordinatesOnTurn[baloonIndex].XCoordinate < 0 || baloonCoordinatesOnTurn[baloonIndex].XCoordinate >= instance.NrRows)
                        {
                            solution.balloonsCoordinate[i, baloonIndex] = solution.balloonsCoordinate[i - 1, baloonIndex];
                        }
                        else
                        {
                            if (altitude != 0)
                            {
                                winData = instance.WindData[altitude - 1, baloonCoordinatesOnTurn[baloonIndex].XCoordinate, baloonCoordinatesOnTurn[baloonIndex].YCoordinate];
                                solution.balloonsCoordinate[i, baloonIndex] = ChangeBaloonCoordinate(winData, baloonCoordinatesOnTurn[baloonIndex]);
                            }
                        }
                    }
                }
                //UpdateBaloonCoordinate(solution.balloonsCoordinate, i, solution.solutionMatrix, "muatate_loon_route");
                //caktoje altitude 
            }
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");

        }
        public void checkIfBaloonNeedsToLunch(Solution solution, int iterator, int turnNumber)
        {
            int[] balloonSolution = new int[instance.NrTurns];
            int altitude = 0;
            if (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))
            {
                solution.solutionMatrix[turnNumber, iterator] = 1;
                Coordinate startWindData = instance.WindData[0, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                Coordinate startNewCoordiante = ChangeBaloonCoordinate(startWindData, instance.StartCell);
                solution.balloonsCoordinate[turnNumber, iterator] = startNewCoordiante;
                balloonSolution = getDimension(solution.solutionMatrix, iterator, 0);
                altitude = calculateAltitude(balloonSolution, instance.Altitudes);
                if ((altitude > instance.Altitudes) || ((checkIfBreaksLoonConstraints(balloonSolution))))
                {
                    solution.solutionMatrix[turnNumber, iterator] = 0;
                    solution.balloonsCoordinate[turnNumber, iterator] = new Coordinate();
                }
                else
                {
                    UpdateBaloonCoordinate(solution.balloonsCoordinate, turnNumber, solution.solutionMatrix, "test");
                }
            }
        }
        public double DistanceBetweenTwoPoints(Coordinate a, Coordinate b)
        {
            if (a.XCoordinate < 0 || b.XCoordinate < 0)
            {
                return 10000;
            }
            return Math.Sqrt(((b.XCoordinate - a.XCoordinate) * (b.XCoordinate - a.XCoordinate)) + (b.YCoordinate - a.YCoordinate) * (b.YCoordinate - a.YCoordinate));
        }
        public int CalculateScoreWithTurnNew(Solution solution)
        {
            scoreDictionary = new Dictionary<Point, List<Coordinate>>();
            int score = 0;
            int[] turnArray;
            Coordinate[] baloonCoordinate;
            int[] baloonAltitudes;
            ScoreObject score12 = new ScoreObject();
            for (int i = 0; i < solution.solutionMatrix.GetLength(0); i++)
            {
                turnArray = getDimension(solution.solutionMatrix, i, 1);
                baloonCoordinate = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i);
                baloonAltitudes = getBaloonAltitudes(solution.solutionMatrix, i);
                score12 = getTurnScoreNew(turnArray, baloonCoordinate, baloonAltitudes, solution, i);
                score += score12.Score;
            }
            return score;
        }
        public int CalculateScoreWithTurn(Solution solution)
        {
            int score = 0;
            for (int i = 0; i < solution.solutionMatrix.GetLength(0); i++)
            {
                int[] turnArray = getDimension(solution.solutionMatrix, i, 1);
                Coordinate[] baloonCoordinate = GetPreviousTurnCoordinate(solution.balloonsCoordinate, i);
                int[] baloonAltitudes = getBaloonAltitudes(solution.solutionMatrix, i);
                score += getTurnScore(turnArray, baloonCoordinate, baloonAltitudes, solution, i).Score;
            }
            return score;
        }

        public void WriteMeanSolutionToFile(List<double> MeanSolutions)
        {
            string filename = "meanSolutions";
            using (TextWriter tw = new StreamWriter(@"C:\Users\vegim\Desktop\MeanSolutuion" + filename + ".txt"))

            {
                for (int j = 0; j < MeanSolutions.Count; j++)
                {

                    tw.Write(MeanSolutions[j]);

                    tw.WriteLine();
                }
            }
        }
        public void WriteSolutionToFile(int Solution)
        {
            string filename = "FinalExecutionElitistFinalInstance2_1";
            using (StreamWriter tw = File.AppendText(@"C:\Users\vegim\Desktop\" + filename + ".txt"))
            {
                tw.Write(Solution);
                tw.WriteLine();
            }
        }
        public ScoreObject getTurnScoreNew(int[] turn, Coordinate[] previousbaloonCoordinates, int[] ballonAltitudes, Solution solution, int turnNumber)
        {
            ScoreObject scoreObject = new ScoreObject();
            int score = 0;
            int altitude;
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            List<Coordinate> baloonsCoordinates = new List<Coordinate>();
            Coordinate windData;
            //Coordinate windData;
            foreach (var item in instance.TargetCells)
            {
                item.isCovered = false;
            }
            for (int i = 0; i < turn.Length; i++)
            {
                Coordinate newBaloonCoordinate = new Coordinate();
                altitude = ballonAltitudes[i] + turn[i];
                if (altitude > 0)
                {
                    if ((getDimension(solution.solutionMatrix, i, 0).Take(turnNumber).Sum() == 0) && (altitude == 1))
                    {
                        windData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        newBaloonCoordinate.XCoordinate = ChangeBaloonCoordinate(windData, instance.StartCell).XCoordinate;
                        newBaloonCoordinate.YCoordinate = ChangeBaloonCoordinate(windData, instance.StartCell).YCoordinate;
                    }
                    else
                    {
                        if (previousbaloonCoordinates[i].XCoordinate < 0 || previousbaloonCoordinates[i].XCoordinate >= instance.NrRows)
                        {
                            newBaloonCoordinate = previousbaloonCoordinates[i];
                        }
                        else
                        {
                            windData = instance.WindData[altitude - 1, previousbaloonCoordinates[i].XCoordinate, previousbaloonCoordinates[i].YCoordinate];
                            newBaloonCoordinate.XCoordinate = ChangeBaloonCoordinate(windData, previousbaloonCoordinates[i]).XCoordinate;
                            newBaloonCoordinate.YCoordinate = ChangeBaloonCoordinate(windData, previousbaloonCoordinates[i]).YCoordinate;
                        }
                    }
                    if (newBaloonCoordinate.XCoordinate < 0 || newBaloonCoordinate.XCoordinate >= instance.NrRows)
                    {
                        //donothing
                    }
                    else
                    {
                        baloonsCoordinates.Add(newBaloonCoordinate);
                        if (coverage.ContainsKey(ConvertCoordinateToPoint(newBaloonCoordinate)))
                        {

                            foreach (var item in coverage[ConvertCoordinateToPoint(newBaloonCoordinate)])
                            {
                                score += (instance.TargetCells[item].isCovered ? 0 : 1);
                                if (!instance.TargetCells[item].isCovered)
                                {
                                    targetCellsCovered.Add(instance.TargetCells[item]);
                                }
                                instance.TargetCells[item].isCovered = true;

                            }

                        }
                        else
                        {
                            List<int> coveredCoordinates = (CalculateCoveredTargetCellsNew(newBaloonCoordinate));
                            foreach (var item in coveredCoordinates)
                            {
                                score += (instance.TargetCells[item].isCovered ? 0 : 1);
                                if (!instance.TargetCells[item].isCovered)
                                {
                                    targetCellsCovered.Add(instance.TargetCells[item]);
                                }
                                instance.TargetCells[item].isCovered = true;
                            }
                            coverage.Add(ConvertCoordinateToPoint(newBaloonCoordinate), coveredCoordinates);


                        }
                    }
                }
            }
            scoreObject.Score = score;
            //object targetcellsCovered = CalculateCoveredDistinctTargetCells2(targetCellsCovered);
            scoreObject.targetCellsCovered = targetCellsCovered;
            scoreObject.baloonCoordinates = baloonsCoordinates;
            return scoreObject;
        }
        public ScoreObject getTurnScoreNew(int[] turn, Coordinate[] previousbaloonCoordinates, int[] ballonAltitudes, int[] baloonArray, int turnNumber)
        {


            ScoreObject scoreObject = new ScoreObject();
            int score = 0;
            int altitude;
            List<Coordinate> targetCellsCovered = new List<Coordinate>();
            List<Coordinate> baloonsCoordinates = new List<Coordinate>();
            Coordinate windData;
            foreach (var item in instance.TargetCells)
            {
                item.isCovered = false;
            }
            for (int i = 0; i < turn.Length; i++)
            {
                Coordinate newBaloonCoordinate = new Coordinate();
                altitude = ballonAltitudes[i] + turn[i];
                if (altitude > 0)
                {
                    if ((ballonAltitudes[i] == 0) && (altitude == 1))
                    {
                        windData = instance.WindData[altitude - 1, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        newBaloonCoordinate = ChangeBaloonCoordinate(windData, instance.StartCell);
                    }
                    else
                    {
                        if (previousbaloonCoordinates[i].XCoordinate < 0 || previousbaloonCoordinates[i].XCoordinate >= instance.NrRows)
                        {
                            newBaloonCoordinate = previousbaloonCoordinates[i];
                        }
                        else
                        {
                            windData = instance.WindData[altitude - 1, previousbaloonCoordinates[i].XCoordinate, previousbaloonCoordinates[i].YCoordinate];
                            newBaloonCoordinate = ChangeBaloonCoordinate(windData, previousbaloonCoordinates[i]);

                        }
                    }
                    if (newBaloonCoordinate.XCoordinate < 0 || newBaloonCoordinate.XCoordinate >= instance.NrRows)
                    {
                        //donothing
                    }
                    else
                    {
                        baloonsCoordinates.Add(newBaloonCoordinate);
                        if (coverage.ContainsKey(ConvertCoordinateToPoint(newBaloonCoordinate)))
                        {

                            foreach (var item in coverage[ConvertCoordinateToPoint(newBaloonCoordinate)])
                            {
                                score += (instance.TargetCells[item].isCovered ? 0 : 1);
                                if (!instance.TargetCells[item].isCovered)
                                {
                                    targetCellsCovered.Add(instance.TargetCells[item]);
                                }
                                instance.TargetCells[item].isCovered = true;
                            }

                        }
                        else
                        {
                            List<int> coveredCoordinates = (CalculateCoveredTargetCellsNew(newBaloonCoordinate));
                            foreach (var item in coveredCoordinates)
                            {
                                score += (instance.TargetCells[item].isCovered ? 0 : 1);
                                if (!instance.TargetCells[item].isCovered)
                                {
                                    targetCellsCovered.Add(instance.TargetCells[item]);
                                }
                                instance.TargetCells[item].isCovered = true;
                            }
                            coverage.Add(ConvertCoordinateToPoint(newBaloonCoordinate), coveredCoordinates);
                        }
                    }
                }
            }
            scoreObject.Score = score;
            scoreObject.targetCellsCovered = targetCellsCovered;
            scoreObject.baloonCoordinates = baloonsCoordinates;
            //object targetcellsCovered = CalculateCoveredDistinctTargetCells2(targetCellsCovered);

            return scoreObject;
        }

        public List<int> CalculateCoveredTargetCellsNew(Coordinate baloonPosition)
        {
            List<int> coveredCoordinates = new List<int>();
            for (int i = 0; i < this.instance.TargetCells.Count; i++)
            {
                if (((((baloonPosition.XCoordinate - this.instance.TargetCells[i].XCoordinate) * (baloonPosition.XCoordinate - this.instance.TargetCells[i].XCoordinate)) + ((ColumnDist(baloonPosition.YCoordinate, this.instance.TargetCells[i].YCoordinate)) * (ColumnDist(baloonPosition.YCoordinate, this.instance.TargetCells[i].YCoordinate)))) <= ((this.instance.Radius) * (this.instance.Radius))))
                {
                    coveredCoordinates.Add(i);
                    this.instance.TargetCells[i].isCovered = true;
                }
            }
            return coveredCoordinates;
        }
        public List<Point> CalculateCoveredTargetCellsNewPoint(Coordinate baloonPosition)
        {
            //for a baloon positioned at r,c  u,v is covered if (r − u)2 + (columndist(c,v))2 ≤ V 2
            //Ruje me dynamic programming ne poziten x,y nese je me ni matrix edhe nese e ki te gatshme ktheje prej asaj
            List<Point> coveredCoordinates = new List<Point>();
            foreach (var targetCell in this.instance.TargetCells)
            {
                if ((targetCell.isCovered == false) && ((((baloonPosition.XCoordinate - targetCell.XCoordinate) * (baloonPosition.XCoordinate - targetCell.XCoordinate)) + ((ColumnDist(baloonPosition.YCoordinate, targetCell.YCoordinate)) * (ColumnDist(baloonPosition.YCoordinate, targetCell.YCoordinate)))) <= ((this.instance.Radius) * (this.instance.Radius))))
                {
                    coveredCoordinates.Add(ConvertCoordinateToPoint(targetCell));
                    targetCell.isCovered = true;
                }
            }
            return coveredCoordinates;
        }

        public Point ConvertCoordinateToPoint(Coordinate coordinate)
        {
            Point point = new Point();
            point.X = coordinate.XCoordinate;
            point.Y = coordinate.YCoordinate;
            return point;
        }
        //TODO:Thirrja e metodes mutate_loon_route_combine ne menyre iterative duke i mbajtur loon-s tjere si fixed
        public List<Solution> generateInitialSolutionImproved(int popSize)
        {

            List<Solution> solutionList = new List<Solution>();
            solutionList = generateInitialPopulation(instance, popSize);
            Solution solution;
            int number_Of_Times_Iterate = randomNumberGenerator.Next(1, 2);
            for (int p = 0; p < 4; p++)
            {
                solution = new Solution(instance.NrTurns, instance.NrBaloons);

                for (int i = 0; i < number_Of_Times_Iterate; i++)
                {
                    for (int j = 0; j < instance.NrBaloons; j++)
                    {
                        solutionList[p] = mutate_loon_route_combine(solutionList[p], instance, j, true);
                    }
                    //solutionList[p].solutionScore = getSolutionScore(solutionList[p]);
                }
                //solutionList.Add(solution);
            }
            return solutionList;
        }

        public List<Solution> generateInitialSolutionRandomly(int popSize)
        {
            List<Solution> solutionList = new List<Solution>();
            Solution solution;
            for (int p = 0; p < popSize; p++)
            {
                solution = new Solution(instance.NrTurns, instance.NrBaloons);
                for (int j = 0; j < instance.NrBaloons; j++)
                {
                    mutate_loon_route_random(solution, instance, j);
                }
                solution.solutionScore = CalculateScoreWithTurn(solution);

                solutionList.Add(solution);
            }
            return solutionList;
        }


        public Solution mutate_with_biggerChange(Solution solution, bool exploativeMethod)
        {
            if (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))
            {
                for (int j = 0; j < instance.NrBaloons / 2; j++)
                {
                    solution = mutate_loon_route_combine(solution, instance, j, exploativeMethod);
                }
            }
            else
            {
                for (int j = instance.NrBaloons / 2; j < instance.NrBaloons; j++)
                {
                    solution = mutate_loon_route_combine(solution, instance, j, exploativeMethod);
                }
            }
            //int number_Of_Times_Iterate = randomNumberGenerator.Next(1, 3);
            //for (int i = 0; i < number_Of_Times_Iterate; i++)
            //{
            //    for (int j = 0; j < instance.NrBaloons; j++)
            //    {
            //        solution = mutate_loon_route_combine(solution, instance, j, true);
            //    }
            //}
            return solution;
        }
        public void mutate_loon_turn(Solution solution, int turnindex)
        {
            int[] turnArray = new int[solution.solutionMatrix.GetLength(1)];
            int[] baloonArray = new int[solution.solutionMatrix.GetLength(0)];
            int bestScoreValue, bestAltitude;
            int baloonAltitudeAfterChange;
            int[] baloonTurnArray;
            //ScoreObject turnScoreWithoutBaloon;
            ScoreObject turnScoreWithBaloon;
            Coordinate[] baloonCoordinatesOnTurn;
            int[] baloonAltitudes;
            int altitude = 0;
            Coordinate winData;
            int oldValue;
            int[] turnBeforeChange = getDimension(solution.solutionMatrix, turnindex, 1);
            //var watch = System.Diagnostics.Stopwatch.StartNew();

            //ne fund te kesaj loop ne bestAltitude vendose vleren e altitude e cila maksimizon score 
            //duke marre parasyshe target cells te cilat jane te mbuluara nga balonat tjera
            //for (int j = turnToStart; j < solution.solutionMatrix.GetLength(0); j++)
            //{
            //    for (int i = 0; i < solution.solutionMatrix.GetLength(1); i++)
            //    {
            //        solution.solutionMatrix[j, i] = 0;
            //        solution.balloonsCoordinate[j, i] = new Coordinate();
            //    }

            //}
            for (int j = 0; j < solution.solutionMatrix.GetLength(1); j++)
            {

                solution.solutionMatrix[turnindex, j] = 0;
                solution.balloonsCoordinate[turnindex, j] = new Coordinate();

            }
            //solution.solutionScore = SolutionScore(solution.solutionMatrix, instance.StartCell, "test");
            solution.solutionScore = 0;
            //te merret parasyshe edhe i+1, te
            for (int i = 0; i < solution.solutionMatrix.GetLength(1); i++)
            {
                baloonArray = getDimension(solution.solutionMatrix, i, 0);
                baloonTurnArray = getDimension(solution.solutionMatrix, i, 1);
                //kjo metode overloaded GetPreviousTurnCoordinate duhet te perdoret edhe ne metodat tjera qe thirret
                baloonCoordinatesOnTurn = GetPreviousTurnCoordinate(solution.balloonsCoordinate, turnindex);
                baloonAltitudes = getBaloonAltitudes(solution.solutionMatrix, turnindex);
                //turnScoreWithoutBaloon = getTurnScore(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, solution, baloonIndex);
                bestAltitude = solution.solutionMatrix[turnindex, i];
                bestScoreValue = 0;
                oldValue = solution.solutionMatrix[turnindex, i];
                //covered target cells by other baloons
                foreach (var possibility in possibilites)
                {
                    baloonArray[turnindex] = possibility;
                    baloonTurnArray[i] = possibility;
                    baloonAltitudeAfterChange = calculateAltitude(baloonArray, instance.Altitudes);
                    //check bounds 1 -> A
                    //perdore logjiken e njejte edhe ne metodat tjera
                    if (!(baloonAltitudeAfterChange > instance.Altitudes) && (!(checkIfBreaksLoonConstraints(baloonArray))))
                    {
                        turnScoreWithBaloon = getTurnScoreNew(baloonTurnArray, baloonCoordinatesOnTurn, baloonAltitudes, baloonArray, i);
                        if ((turnScoreWithBaloon.Score > bestScoreValue) || ((i != 0) && (turnScoreWithBaloon.Score == bestScoreValue) && (Convert.ToBoolean(randomNumberGenerator.Next(0, 2)))))
                        {
                            bestScoreValue = turnScoreWithBaloon.Score;
                            bestAltitude = possibility;
                        }
                        //kalkulo score ne turn-in momental 
                    }
                }
                solution.solutionMatrix[turnindex, i] = bestAltitude;
                baloonAltitudeAfterChange = calculateAltitude(getDimension(solution.solutionMatrix, i, 0), instance.Altitudes);
                if (baloonAltitudeAfterChange >= 9)
                {
                    Console.WriteLine();
                }
                //baloonArray[i] = bestAltitude;
            }
            UpdateBaloonCoordinate(solution.balloonsCoordinate, turnindex, solution.solutionMatrix, "test");
        }
    }
}
