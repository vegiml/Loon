﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loon.Models
{
    public class ScoreObject
    {
        public int Score { get; set; }
        public List<Coordinate> targetCellsCovered { get; set; }
        public List<Coordinate> baloonCoordinates { get; set; }
    }
}
