﻿namespace Loon.Models
{
    public class MatrixSolution
    {
        public int Score { get; set; }
        public int LoonIndex { get; set; }
        public int[] LoonArray { get; set; }
        public MatrixSolution(int Score_, int LoonIndex_,int nrTruns)
        {
            Score = Score;
            LoonIndex = LoonIndex_;
            LoonArray = new int[nrTruns];
        }
    }
}
