﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loon.Models
{
    [Serializable]

    public class Solution
    {
        public int[,] solutionMatrix { get; set; }
        public int solutionScore { get; set; }
        public Coordinate[,] balloonsCoordinate { get; set; }
        public Solution(int rows, int columns)
        {
            balloonsCoordinate = new Coordinate[rows, columns];
            solutionMatrix = new int[rows, columns];
        }
    }
}
