﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loon.Models
{
    [Serializable]
    public class Coordinate
    {
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }
        public int CoveredTimes { get; set; }
        public bool isCovered { get; set; }
        public Coordinate(int x = -1, int y = -1)
        {
            this.XCoordinate = x;
            this.YCoordinate = y;
            this.CoveredTimes = 0;
            this.isCovered = false;
        }

    }
}
