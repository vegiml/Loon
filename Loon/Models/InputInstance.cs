﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loon.Models
{
    public class InputInstance
    {
        public int NrRows { get; set; }
        public int NrColumns { get; set; }
        public int Altitudes { get; set; }
        public int Radius { get; set; }
        public int NrTargetCells { get; set; }
        public int NrBaloons { get; set; }
        public int NrTurns { get; set; }
        public Coordinate StartCell { get; set; } 
        public List<Coordinate> TargetCells { get; set; }
        public Coordinate [,,] WindData { get; set; }
        public InputInstance(int _altitudes,int _columns, int _rows)
        {
            this.NrRows = _rows;
            this.Altitudes = _altitudes;
            this.NrColumns = _columns;
            WindData = new Coordinate[this.Altitudes,this.NrRows,  this.NrColumns];
            StartCell = new Coordinate(0,0);
            TargetCells = new List<Coordinate>();
        }


    }
}
