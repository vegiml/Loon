﻿using Loon.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loon
{
    class Program
    {
        static void Main(string[] args)
        {


            //Declarations of variables
            string follderPath = @"C:\Users\vegim\OneDrive\Diploma\Documents-Vegim\Visual Studio 2017\Projects\Loon\Loon\Instances\";
            string file = "loon_r7_c10_a5_radius3_saturation.in";
            string fullPath = follderPath + file;
            string[] instanceLines = File.ReadAllLines(fullPath);
            Random randomNumberGenerator = new Random();
            int[] possibilities = new int[] { 0, -1, 1 };
            //Input file processing
            List<Solution> solutionsList = new List<Solution>();
            int rows = Convert.ToInt32(instanceLines[0].Split(' ')[0]);
            int columns = Convert.ToInt32(instanceLines[0].Split(' ')[1]);
            int altitudes = Convert.ToInt32(instanceLines[0].Split(' ')[2]);
            InputInstance instance = new InputInstance(altitudes, columns, rows);
            instance.NrTargetCells = Convert.ToInt32(instanceLines[1].Split(' ')[0]);
            instance.Radius = Convert.ToInt32(instanceLines[1].Split(' ')[1]);
            instance.NrBaloons = Convert.ToInt32(instanceLines[1].Split(' ')[2]);
            instance.NrTurns = Convert.ToInt32(instanceLines[1].Split(' ')[3]);
            instance.StartCell = new Coordinate(Convert.ToInt32(instanceLines[2].Split(' ')[0]), Convert.ToInt32(instanceLines[2].Split(' ')[1]));
            LoonFunctions loonfunction = new LoonFunctions(instance);

            int[] ballonAltitudes = new int[instance.NrBaloons];
            ballonAltitudes.Populate<int>(0);
            int popSize = 10;
            Console.WriteLine("Select population size ");

            popSize = int.Parse(Console.ReadLine());
            HashSet<int[,]> solutions = new HashSet<int[,]>(new MatrixComparer());
            int posibilityScoreMax;
            //dy variablat me poshte vendosin altitude dhe koordinaten
            int altitudeAdjustment;
            Coordinate coordinateToSet = new Coordinate();
            Coordinate newCoordinate = new Coordinate();
            Coordinate previousTurnCoordiante;
            Coordinate windData;
            int numberOfTimesNoSolution;
            int altitudeAfterAdjustment;
            int distinctTargetCellsCovered;
            int randomAltitude;
            bool takeOptimalSolution;
            List<Coordinate> targetCellsCovered;
            Coordinate[] balloonCoordinate;
            List<Coordinate> targetCellsCoveredFromOtherBalloons;
            List<int> bestSolutions = new List<int>();
            int takOptimal = 0;
            for (int i = 0; i < instance.NrTargetCells; i++)
            {
                instance.TargetCells.Add(new Coordinate(Convert.ToInt32(instanceLines[i + 3].Split(' ')[0]), Convert.ToInt32(instanceLines[i + 3].Split(' ')[1])));
            }
            for (int i = 0; i < instance.Altitudes; i++)
            {
                for (int j = 0; j < instance.NrRows; j++)
                {
                    for (int k = 0; k < instance.NrColumns; k++)
                    {
                        int x = Convert.ToInt32(instanceLines[j + (i * instance.NrRows) + (3 + instance.NrTargetCells)].Split(' ')[2 * k]);
                        int y = Convert.ToInt32(instanceLines[(j + (i * instance.NrRows) + (3 + instance.NrTargetCells))].Split(' ')[2 * k + 1]);
                        instance.WindData[i, j, k] = new Coordinate(x, y);
                    }
                }
            }

            for (int pop = 0; pop < popSize; pop++)
            {
                Coordinate[,] balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
                balloonsCoordinate.Populate(new Coordinate());
                int[,] solution = new int[instance.NrTurns, instance.NrBaloons];
                int randomNumberOfBalloons;
                if (instance.NrBaloons < 10)
                {
                    randomNumberOfBalloons = instance.NrBaloons;
                }
                else
                {
                    randomNumberOfBalloons = randomNumberGenerator.Next(instance.NrBaloons / 3, instance.NrBaloons / 2);
                }
                for (int m = 0; m < instance.NrBaloons; m++)
                {
                    if (m != 0 && ballonAltitudes[m] == 0)
                    {
                        solution[0, m] = 1;
                        ballonAltitudes[m] = 1;
                        Coordinate startWindData = instance.WindData[0, instance.StartCell.XCoordinate, instance.StartCell.YCoordinate];
                        Coordinate startNewCoordiante = loonfunction.ChangeBaloonCoordinate(startWindData, instance.StartCell);
                        balloonsCoordinate[0, m] = startNewCoordiante;
                    }
                }
                for (int i = 0; i < randomNumberOfBalloons; i++)
                {
                    int randomIndex = i;
                    int[] column = loonfunction.getDimension(solution, randomIndex, 0);
                    for (int j = 0; j < column.Length; j++)
                    {
                        //Logjika eshte gjetja e pathit optimal per loon ne fjale
                        //duke e marre parasyshe qe te gjithe balonat tjere ndodhen ne gjendje qetesie

                        if (!(j == 0 && ballonAltitudes[randomIndex] != 0))
                        {
                            newCoordinate = new Coordinate();
                            posibilityScoreMax = 0;
                            altitudeAdjustment = 0;
                            coordinateToSet = new Coordinate();

                            if (ballonAltitudes[randomIndex] != 0)
                            {
                                if (balloonsCoordinate[j - 1, i].XCoordinate >= 0)
                                {
                                    windData = instance.WindData[ballonAltitudes[randomIndex] - 1, balloonsCoordinate[j - 1, i].XCoordinate, balloonsCoordinate[j - 1, i].YCoordinate];
                                    coordinateToSet = loonfunction.ChangeBaloonCoordinate(windData, balloonsCoordinate[j - 1, i]);

                                }

                            }
                            //add initial coordinate value if coordinate 
                            //is not set below for specific constraints

                            numberOfTimesNoSolution = 0;
                            takeOptimalSolution = randomNumberGenerator.Next(1, 101) <= takOptimal;
                            for (int k = 0; k < possibilities.Length; k++)
                            {
                                altitudeAfterAdjustment = ballonAltitudes[randomIndex] + possibilities[k];
                                // kushti per mos humbjen e loon, nuk duhet te kaloj numri maksimal i altitudes
                                // nuk e marrim kete lartesi constraint 
                                //constraint i problemit, nuk guxon ta tejkaloj 

                                //hard constraint
                                if ((altitudeAfterAdjustment > instance.Altitudes) || (ballonAltitudes[randomIndex] != 0 && altitudeAfterAdjustment < 1))
                                {
                                    continue;
                                }
                                else if (altitudeAfterAdjustment > 0)
                                {
                                    previousTurnCoordiante = new Coordinate();
                                    windData = new Coordinate();
                                    //altitude e balones eshte zero
                                    if (altitudeAfterAdjustment == 1 && ballonAltitudes[randomIndex] == 0)
                                    {
                                        balloonsCoordinate[j, i] = instance.StartCell;
                                        windData = instance.WindData[altitudeAfterAdjustment - 1, balloonsCoordinate[j, i].XCoordinate, balloonsCoordinate[j, i].YCoordinate];
                                        newCoordinate = loonfunction.ChangeBaloonCoordinate(windData, balloonsCoordinate[j, i]);
                                        //this two variables are set to soluion after break
                                        coordinateToSet = newCoordinate;
                                        altitudeAdjustment = altitudeAfterAdjustment;
                                        break;
                                    }
                                    else
                                    {
                                        //pervious baloon coordinate (save that on a variable)
                                        previousTurnCoordiante = balloonsCoordinate[j - 1, i];
                                        //soft constraint
                                        if (previousTurnCoordiante.XCoordinate < 0 || previousTurnCoordiante.XCoordinate >= instance.NrRows)
                                        {
                                            newCoordinate = previousTurnCoordiante;
                                            coordinateToSet = newCoordinate;
                                            break;
                                        }
                                        windData = instance.WindData[altitudeAfterAdjustment - 1, previousTurnCoordiante.XCoordinate, previousTurnCoordiante.YCoordinate];
                                        newCoordinate = loonfunction.ChangeBaloonCoordinate(windData, previousTurnCoordiante);

                                    }
                                    //After the coordiante change baloon goes outside of the row grid
                                    if (newCoordinate.XCoordinate < 0 || newCoordinate.XCoordinate >= instance.NrRows)
                                    {
                                        numberOfTimesNoSolution++;
                                        if (numberOfTimesNoSolution == possibilities.Length)
                                        {
                                            //caktoje nje lartesi te caktume random 
                                            altitudeAdjustment = possibilities[k];
                                            coordinateToSet = newCoordinate;
                                            break;
                                        }
                                        continue;
                                    }

                                    if (ballonAltitudes[randomIndex] == 0)
                                    {
                                        altitudeAdjustment = possibilities[k];
                                        coordinateToSet = newCoordinate;
                                    }
                                    //gjeje optimalen 
                                    else if (takeOptimalSolution)
                                    {
                                        targetCellsCovered = loonfunction.CalculateCoveredTargetCells(newCoordinate);
                                        //just one baloons coordinate have been changed, get balloon coordinate 
                                        balloonCoordinate = loonfunction.GetCoordinateRow(balloonsCoordinate, j);
                                        targetCellsCoveredFromOtherBalloons = loonfunction.TargetCellsCoveredForRow(ballonAltitudes, balloonCoordinate, instance.NrBaloons, randomIndex);
                                        targetCellsCovered.AddRange(targetCellsCoveredFromOtherBalloons);
                                        distinctTargetCellsCovered = loonfunction.CalculateCoveredDistinctTargetCells(targetCellsCovered);

                                        //Nese plotesohet kushti mundohemi me gjete ende zgjidhje me te mira
                                        if (posibilityScoreMax <= distinctTargetCellsCovered)
                                        {
                                            posibilityScoreMax = distinctTargetCellsCovered;
                                            altitudeAdjustment = possibilities[k];
                                            coordinateToSet = newCoordinate;
                                        }
                                    }
                                    //gjeje random
                                    else
                                    {
                                        randomAltitude = possibilities[randomNumberGenerator.Next(0, 3)];
                                        altitudeAfterAdjustment = ballonAltitudes[randomIndex] + randomAltitude;
                                        if ((altitudeAfterAdjustment > instance.Altitudes) || (ballonAltitudes[randomIndex] != 0 && altitudeAfterAdjustment < 1))
                                        {
                                            continue;
                                        }
                                        previousTurnCoordiante = balloonsCoordinate[j - 1, i];
                                        if (previousTurnCoordiante.XCoordinate < 0 || previousTurnCoordiante.XCoordinate >= instance.NrRows)
                                        {
                                            newCoordinate = previousTurnCoordiante;
                                            coordinateToSet = newCoordinate;
                                            break;
                                        }
                                        windData = instance.WindData[altitudeAfterAdjustment - 1, previousTurnCoordiante.XCoordinate, previousTurnCoordiante.YCoordinate];
                                        newCoordinate = loonfunction.ChangeBaloonCoordinate(windData, previousTurnCoordiante); coordinateToSet = newCoordinate;
                                        altitudeAdjustment = randomAltitude;
                                        break;
                                    }
                                }
                            }
                            ballonAltitudes[randomIndex] = ballonAltitudes[randomIndex] + altitudeAdjustment;
                            balloonsCoordinate[j, i] = coordinateToSet;
                            solution[j, randomIndex] = altitudeAdjustment;
                        }
                    }
                }
                if (!(solutions.Add(solution)))
                {
                    popSize = popSize + 1;
                    ballonAltitudes.Populate(0);
                    continue;
                }
                else
                {
                    Solution solutionObject = new Solution(instance.NrRows, instance.NrColumns);
                    solutionObject.solutionMatrix = solution;
                    solutionObject.balloonsCoordinate = balloonsCoordinate;
                    solutionsList.Add(solutionObject);
                }
                ballonAltitudes.Populate(0);

            }

            for (int i = 0; i < solutions.Count; i++)
            {
                Console.WriteLine("Solution for {0} population is {1} ", i, loonfunction.SolutionScore(solutions.ElementAt(i), instance.StartCell));
                solutionsList[i].solutionScore = loonfunction.SolutionScore(solutions.ElementAt(i), instance.StartCell);
            }


            //pjesa e GA - evolutionary algorithm
            //Steady state or generational EA. Selection method to be choosen

            //Declare parameters that we should consider
            //selection aggresivity is based on the tournamenSize variable value
            HashSet<int[,]> solutionsEvolution = new HashSet<int[,]>(new MatrixComparer());
            int generationSize = 10000;
            int tournamentSize = 10;
            double explorationRate = 0.6;
            double mutate_Rate = 0.6;
            double swap_turns_Rate = 0.1;
            double mutate_turn_Rate = 0.3;
            double mute_rate = 1;
            double percentageOfRowsToBeChanged = (((double)randomNumberGenerator.Next(10, 20)) / 100);
            double percentageOfBaloonsToBeChanged = (((double)randomNumberGenerator.Next(10, 20)) / 100);
            List<Solution> tournametList;
            Solution firstParent;
            Solution secondParent;
            Solution firstWorstSolutionTournamnet;
            Solution secondWorstSolutionTournament;
            bool executeMutate;
            bool executeMutateTurn;
            bool executeSwapTurn;
            int randomNumber;
            int firsTurn;
            int SecondTurn;
            double crossoverRate = 0.7;
            Solution firstChild = new Solution(instance.NrRows, instance.NrColumns);
            Solution secondChild = new Solution(instance.NrRows, instance.NrColumns);
            int minScore;
            List<Solution> childs = new List<Solution>();
            //Console.WriteLine("Select Generation size ");
            //Console.WriteLine("Select tournament size ");
            //tournamentSize =int.Parse( Console.ReadLine());
            //generationSize = int.Parse(Console.ReadLine());
            int bestSolutionOnInitialPopulation = solutionsList.Max(a => a.solutionScore);
            int worstSolutionOnInitialPopulationst = solutionsList.Min(a => a.solutionScore);
            for (int generation = 1; generation <= generationSize; generation++)
            {
                //tourne
                tournametList = loonfunction.tournamentSelection(tournamentSize, solutionsList);
                tournametList = tournametList.OrderByDescending(b => b.solutionScore).ToList();
                firstParent = tournametList.ElementAt(0);
                secondParent = tournametList.ElementAt(1);
                firstWorstSolutionTournamnet = tournametList.ElementAt(tournametList.Count - 1);
                secondWorstSolutionTournament = tournametList.ElementAt(tournametList.Count - 2);
                minScore = solutionsList.Min(a => a.solutionScore);
                firstWorstSolutionTournamnet = solutionsList.Where(a => a.solutionScore == minScore).FirstOrDefault();
                //firstParent = loonfunction.rankBasedSelection(solutionsList);
                //secondParent = loonfunction.rankBasedSelection(solutionsList);
                randomNumber = randomNumberGenerator.Next(1, 101);
                executeMutate = (randomNumber >= 0) && (randomNumber <= (mutate_Rate * 100));
                executeMutateTurn = (randomNumber >= (mutate_Rate * 100)) && (randomNumber <= ((mutate_Rate * 100) + (mutate_turn_Rate * 100)));
                executeSwapTurn = ((randomNumber >= (mutate_Rate * 100) + (mutate_turn_Rate * 100))) && (randomNumber <= ((mutate_turn_Rate * 100) + (mutate_Rate * 100) + (swap_turns_Rate * 100)));
                //Solution parent = loonfunction.rankBasedSelection(solutionsList);

                //add crossover
                if (randomNumberGenerator.Next(1, 101) <= (crossoverRate * 100))
                {
                    childs = loonfunction.crossoverOperator(firstParent, secondParent);
                    firstChild = childs[0];
                    secondChild = childs[1];

                }
                else
                {
                    firstChild = firstParent;
                    secondChild = secondParent;
                }
                if (randomNumberGenerator.Next(1, 101) <= (mute_rate * 100))
                {
                    if (executeMutate)
                    {
                        loonfunction.mutate(firstChild, instance, explorationRate, percentageOfBaloonsToBeChanged);
                        loonfunction.mutate(secondChild, instance, explorationRate, percentageOfBaloonsToBeChanged);

                    }
                    else if (executeMutateTurn)
                    {
                        loonfunction.mutate_turn(firstChild, instance, explorationRate, percentageOfRowsToBeChanged, percentageOfBaloonsToBeChanged);
                        loonfunction.mutate_turn(secondChild, instance, explorationRate, percentageOfRowsToBeChanged, percentageOfBaloonsToBeChanged);

                    }
                    else if (executeSwapTurn)
                    {
                        firsTurn = randomNumberGenerator.Next(1, instance.NrTurns);
                        SecondTurn = randomNumberGenerator.Next(1, instance.NrTurns);
                        while (firsTurn == SecondTurn)
                        {
                            SecondTurn = randomNumberGenerator.Next(1, instance.NrTurns);
                        }
                        loonfunction.swap_turns(firstChild, firsTurn, SecondTurn);
                        loonfunction.swap_turns(secondChild, firsTurn, SecondTurn);
                    }
                    firstChild.solutionScore = loonfunction.SolutionScore(firstChild.solutionMatrix, instance.StartCell);
                    secondChild.solutionScore = loonfunction.SolutionScore(secondChild.solutionMatrix, instance.StartCell);
                }
                //testing convergence
                int randomindividualindex1 = randomNumberGenerator.Next(0, solutionsList.Count);

                if (generation > 9000)
                {
                    if (solutions.Add(firstChild.solutionMatrix))
                    {
                        solutions.Remove(solutions.ElementAt(randomindividualindex1));
                    }
                }
                //if (firstChild.solutionScore >= firstWorstSolutionTournamnet.solutionScore)
                //{
                //solutionsList.Add(firstChild);
                int randomindividualindex;
                if (firstChild.solutionScore >= firstWorstSolutionTournamnet.solutionScore)
                {
                    solutionsList.Add(firstChild);
                    randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);

                    solutionsList.RemoveAt(randomindividualindex);
                }
                if (secondChild.solutionScore >= firstWorstSolutionTournamnet.solutionScore)
                {
                    solutionsList.Add(secondChild);
                    randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);

                    solutionsList.RemoveAt(randomindividualindex);
                }

                //solutionsList.RemoveAt(randomindividualindex);
                //solutionsList.Remove(secondWorstSolutionTournament);
                //}
                //if (secondChild.solutionScore >= firstWorstSolutionTournamnet.solutionScore)
                //{
                //solutionsList.Add(secondChild);
                //if (generation > 9000)
                //{
                //    if (solutions.Add(secondChild.solutionMatrix))
                //    {
                //        solutions.Remove(solutions.ElementAt(randomindividualindex));
                //    }
                //}

                //solutionsList.RemoveAt(randomindividualindex);

                //solutionsList.Remove(secondWorstSolutionTournament);
                //}
                bestSolutions.Add(solutionsList.Max(a => a.solutionScore));
                int maximumOnBestSolution = bestSolutions.Max(a => a);
                int bestscore = bestSolutions.Max();
                if (generation == 10000)
                {
                    Console.WriteLine(bestscore);
                }
                //until the best solution is found
            }
        }
    }
}
