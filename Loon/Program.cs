﻿using Loon.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loon
{
    class Program
    {
        static void Main(string[] args)
        {


            //Declarations of variables
            string follderPath = @"C:\Users\vegim\OneDrive\Diploma\Documents-Vegim\Visual Studio 2017\Projects\Loon\Loon\Instances\";
            string file = "loon_r7_c10_a5_radius3_saturation.in";
            string fullPath = follderPath + file;
            string[] instanceLines = File.ReadAllLines(fullPath);
            Random randomNumberGenerator = new Random();
            int[] possibilities = new int[] { 0, -1, 1 };
            List<double> meanSolutionFItness = new List<double>();

            //Input file processing
            List<Solution> solutionsList = new List<Solution>();
            int rows = Convert.ToInt32(instanceLines[0].Split(' ')[0]);
            int columns = Convert.ToInt32(instanceLines[0].Split(' ')[1]);
            int altitudes = Convert.ToInt32(instanceLines[0].Split(' ')[2]);
            InputInstance instance = new InputInstance(altitudes, columns, rows);
            instance.NrTargetCells = Convert.ToInt32(instanceLines[1].Split(' ')[0]);
            instance.Radius = Convert.ToInt32(instanceLines[1].Split(' ')[1]);
            instance.NrBaloons = Convert.ToInt32(instanceLines[1].Split(' ')[2]);
            instance.NrTurns = Convert.ToInt32(instanceLines[1].Split(' ')[3]);
            instance.StartCell = new Coordinate(Convert.ToInt32(instanceLines[2].Split(' ')[0]), Convert.ToInt32(instanceLines[2].Split(' ')[1]));

            int[] ballonAltitudes = new int[instance.NrBaloons];
            ballonAltitudes.Populate<int>(0);


            HashSet<int[,]> solutions = new HashSet<int[,]>(new MatrixComparer());
            Coordinate windData;
            List<Solution> bestSolutions = new List<Solution>();
            for (int i = 0; i < instance.NrTargetCells; i++)
            {
                instance.TargetCells.Add(new Coordinate(Convert.ToInt32(instanceLines[i + 3].Split(' ')[0]), Convert.ToInt32(instanceLines[i + 3].Split(' ')[1])));
            }
            for (int i = 0; i < instance.Altitudes; i++)
            {
                for (int j = 0; j < instance.NrRows; j++)
                {
                    for (int k = 0; k < instance.NrColumns; k++)
                    {
                        int x = Convert.ToInt32(instanceLines[j + (i * instance.NrRows) + (3 + instance.NrTargetCells)].Split(' ')[2 * k]);
                        int y = Convert.ToInt32(instanceLines[(j + (i * instance.NrRows) + (3 + instance.NrTargetCells))].Split(' ')[2 * k + 1]);
                        instance.WindData[i, j, k] = new Coordinate(x, y);
                    }
                }
            }

            LoonFunctions loonfunction = new LoonFunctions(instance);


            //pjesa e GA - evolutionary algorithm
            //Steady state or generational EA. Selection method to be choosen

            //Declare parameters that we should consider
            //selection aggresivity is based on the tournamenSize variable value
            int randomindividualindex;

            HashSet<int[,]> solutionsEvolution = new HashSet<int[,]>(new MatrixComparer());

            List<int> bestSolutionsScore = new List<int>();
            List<Solution> tournametList;
            Solution firstParent;
            Solution secondParent;
            Solution firstWorstSolutionTournamnet;
            Solution secondWorstSolutionTournament;
            bool executeMutate;
            bool executeMutateTurn;
            bool executeSwapTurn;
            bool executeMutate_Loon_Route;
            int randomNumberForMutation;
            int randomNumberForCrossover;
            int randomNumberForMutationExecution;
            int firsTurn;
            int SecondTurn;
            Solution firstChild = new Solution(instance.NrRows, instance.NrColumns);
            Solution secondChild = new Solution(instance.NrRows, instance.NrColumns);
            int minScore;
            List<Solution> childs = new List<Solution>();
            int maximumOnBestSolution;
            int bestscore = 0;
            int bestScoreUntilNow = 0;
            int maxScoreOnThisPopulation = 0;
            //Console.WriteLine("Select Generation size ");
            //Console.WriteLine("Select tournament size ");
            //tournamentSize =int.Parse( Console.ReadLine());
            //generationSize = int.Psare(Console.ReadLine());
            //parameters of Genetic Algorithm
            Console.WriteLine("Select generation size: ");

            int generationSize = int.Parse(Console.ReadLine());
            bool exploitiveMethod = true;
            int nrIterationsNoImprovement = 200;
            int popSize = 10;
            Console.WriteLine("Select population size ");
            popSize = int.Parse(Console.ReadLine());
            int tournamentSize = int.Parse(Console.ReadLine());
            //exploration rate for genetic operators mutate and mutate_rate
            //if we should have mutation at all
            //double mute_rate = 0.8;
            //double crossoverRate = 0.2;

            //parametrat
            double mute_rate = 0.5;
            double crossoverRate = 0.4;
            //types of mutation operators rates that will be used
            double mutate_Rate = 0.3;
            double mutate_loon_route_rate = 0.7;
            //percentage of turns and baloons to be changed by methods mutate (percentageOfBaloonsToBeChanged)
            //and mutate_rate (percentageOfRowsToBeChanged,percentageOfBaloonsToBeChanged)
            int randomLoonIndex;
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromSeconds(10);
            var timer = new System.Threading.Timer((e) =>
            {
                loonfunction.WriteSolutionToFile(bestscore);
            }, null, startTimeSpan, periodTimeSpan);
            #region
            solutionsList = loonfunction.generateInitialSolutionImproved(popSize);
            int bestSolutionOnInitialPopulation = solutionsList.Max(a => a.solutionScore);
            bestscore = bestSolutionOnInitialPopulation;
            int worstSolutionOnInitialPopulationst = solutionsList.Min(a => a.solutionScore);
            List<Solution> eliteSolutions = new List<Solution>();
            bool[] nrRandomBaloons = new bool[instance.NrBaloons];
            #endregion
            for (int generation = 1; generation <= generationSize; generation++)
            {
                eliteSolutions = new List<Solution>();
                solutionsList = solutionsList.OrderByDescending(a => a.solutionScore).ToList();
                int n = solutionsList.Count / 3;
                for (int i = 0; i < n; i++)
                {
                    eliteSolutions.Add(ExtensionMethods.DeepCopy(solutionsList[i]));

                }
                for (int i = 0; i < (popSize - n) / 2; i++)
                {
                    tournametList = loonfunction.tournamentSelection(tournamentSize, solutionsList);
                    tournametList = tournametList.OrderByDescending(b => b.solutionScore).ToList();
                    firstParent = tournametList.ElementAt(0);
                    secondParent = tournametList.ElementAt(1);
                    int s = 1;
                    while (firstParent.solutionScore == secondParent.solutionScore)
                    {
                        s++;
                        if (s == tournamentSize)
                        {
                            break;
                        }
                        secondParent = tournametList.ElementAt(s);
                    }
                    firstWorstSolutionTournamnet = tournametList.ElementAt(tournametList.Count - 1);
                    secondWorstSolutionTournament = tournametList.ElementAt(tournametList.Count - 2);
                    // end selection
                    randomNumberForMutation = randomNumberGenerator.Next(1, 101);
                    randomNumberForCrossover = randomNumberGenerator.Next(1, 101);
                    executeMutate = (randomNumberForMutation >= 0) && (randomNumberForMutation <= (mutate_Rate * 100));
                    executeMutate_Loon_Route = (randomNumberForMutation >= (mutate_Rate * 100)) && (randomNumberForMutation <= ((mutate_Rate * 100) + (mutate_loon_route_rate * 100)));
                    //Solution parent = loonfunction.rankBasedSelection(solutionsList);
                    if (randomNumberForCrossover <= (crossoverRate * 100))
                    {
                        childs = loonfunction.crossoverOperator(firstParent, secondParent);
                        //childs = loonfunction.crossoverOperatorImproved(firstParent, secondParent);
                        firstChild = childs[0];
                        secondChild = childs[1];
                    }
                    else
                    {
                        firstChild = firstParent;
                        secondChild = secondParent;
                    }
                    //end crossover
                    // mutation
                    //firstChild = firstParent;
                    randomNumberForMutationExecution = randomNumberGenerator.Next(1, 101);
                    if (randomNumberForMutationExecution <= (mute_rate * 100))
                    {
                        if (executeMutate)
                        {
                            int randomNumberOfLoonsToChangerRoute = randomNumberGenerator.Next(1, instance.NrBaloons);
                            //int randomNumberOfLoonsToChangerRoute = 1;
                            int loonIndex = 0;
                            while (loonIndex < randomNumberOfLoonsToChangerRoute)
                            {
                                loonfunction.mutate_loon_route_random(firstChild, instance, loonIndex);
                                loonfunction.mutate_loon_route_random(secondChild, instance, loonIndex);
                                loonIndex++;
                            }
                            firstChild.solutionScore = loonfunction.CalculateScoreWithTurnNew(firstChild);
                            secondChild.solutionScore = loonfunction.CalculateScoreWithTurnNew(secondChild);
                        }
                        else if (executeMutate_Loon_Route)
                        {
                            //mundesia qe ne disa raste keto metdo te thirren per disa balona
                            int randomNumberOfLoonsToChangerRoute = randomNumberGenerator.Next(1, instance.NrBaloons);
                            int loonIndex = 0;
                            while (loonIndex < randomNumberOfLoonsToChangerRoute)
                            {
                                randomLoonIndex = randomNumberGenerator.Next(0, instance.NrBaloons);
                                while (!nrRandomBaloons[randomLoonIndex])
                                {
                                    randomLoonIndex = randomNumberGenerator.Next(0, instance.NrBaloons);
                                    nrRandomBaloons[randomLoonIndex] = true;
                                }
                                firstChild = loonfunction.mutate_loon_route_combine(firstChild, instance, randomLoonIndex, exploitiveMethod);
                                secondChild = loonfunction.mutate_loon_route_combine(secondChild, instance, randomLoonIndex, exploitiveMethod);
                                loonIndex++;
                            }
                        }
                    }
                    eliteSolutions.Add(ExtensionMethods.DeepCopy(firstChild));
                    eliteSolutions.Add(ExtensionMethods.DeepCopy(secondChild));
                }
                solutionsList = new List<Solution>();
                solutionsList = eliteSolutions;
                //if (firstChild.solutionScore >= firstWorstSolutionTournamnet.solutionScore)
                //{
                //    if (exploitiveMethod)
                //    {
                //        solutionsList.Add(firstChild);
                //        solutionsList.Remove(firstWorstSolutionTournamnet);
                //    }
                //    else
                //    {
                //        solutionsList.Add(firstChild);
                //        randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);
                //        while (firstChild.Equals(solutionsList[randomindividualindex]))
                //        {
                //            randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);
                //        }
                //        solutionsList.RemoveAt(randomindividualindex);
                //    }
                //}
                //if (secondChild.solutionScore >= secondWorstSolutionTournament.solutionScore)
                //{
                //    if (exploitiveMethod)
                //    {
                //        solutionsList.Add(secondChild);
                //        solutionsList.Remove(secondWorstSolutionTournament);
                //    }
                //    else
                //    {
                //        solutionsList.Add(secondChild);
                //        randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);
                //        while (secondChild.Equals(solutionsList[randomindividualindex]))
                //        {
                //            randomindividualindex = randomNumberGenerator.Next(0, solutionsList.Count);

                //        }
                //        solutionsList.RemoveAt(randomindividualindex);
                //    }
                //}
                maxScoreOnThisPopulation = solutionsList.Max(a => a.solutionScore);
                if (maxScoreOnThisPopulation > bestscore)
                {
                    bestscore = maxScoreOnThisPopulation;
                    bestSolutionsScore.Add(bestscore);
                    Solution solution = solutionsList.Where(a => a.solutionScore == bestscore).FirstOrDefault();
                    bestSolutions.Add(ExtensionMethods.DeepCopy(solution));
                }
                else
                {
                    bestSolutionsScore.Add(maxScoreOnThisPopulation);
                }
                if (((generation) % nrIterationsNoImprovement) == 0)
                {
                    //nese ne nrIterationsNoImprovement nuk ka improvement shko ne meneyre explorative
                    // perndryshe shko ne meneyre exploitive
                    if (!(loonfunction.checkIfNoImprovement(nrIterationsNoImprovement, bestSolutionsScore)))
                    {
                        //explorative
                        exploitiveMethod = false;
                        tournamentSize = Convert.ToInt32(popSize * 0.6);
                        mutate_Rate = 0.4;
                        mutate_loon_route_rate = 0.6;
                    }
                    else
                    {
                        //exploative
                        exploitiveMethod = true;
                        mutate_Rate = 0.3;
                        mutate_loon_route_rate = 0.7;
                        tournamentSize = Convert.ToInt32(popSize * 0.8);

                    }
                }
                //Console.WriteLine(bestscore);
                //meanSolutionFItness.Add(solutionsList.Average(a => a.solutionScore));
                //loonfunction.WriteMeanSolutionToFile(meanSolutionFItness);

                if (generation == generationSize)
                {


                    //calculate score for every turn and find the position of the loons for every turn, together with the covered target cells



                    bestSolutions = bestSolutions.OrderByDescending(a=>a.solutionScore).ToList();

                    Solution bestSolution = bestSolutions.First();
                    int score = loonfunction.CalculateScoreWithTurnNew(bestSolution);
                    string filename4 = "solution" + DateTime.Now.Ticks;
                    using (TextWriter tw = new StreamWriter(@"C:\Users\vegim\Desktop\" + filename4 + ".txt"))

                    {
                        for (int j = 0; j < bestSolution.solutionMatrix.GetLength(0); j++)
                        {
                            for (int i = 0; i < bestSolution.solutionMatrix.GetLength(1); i++)
                            {
                                tw.Write(bestSolution.solutionMatrix[j, i] + " ");
                            }
                            tw.WriteLine();
                        }
                    }
                    int scoreBest = loonfunction.CalculateScoreWithTurnNew(bestSolution);
                    Console.WriteLine(bestscore);
                }

                //until the best solution is found
            }
        }

    }
}
