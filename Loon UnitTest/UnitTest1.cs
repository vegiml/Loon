﻿using Loon;
using Loon.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LoonUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMuateBaloon()
        {



            //for (int j = 0; j < solutionsList1[0].solutionMatrix.GetLength(0); j++)
            //{

            //    solutionsList1[0].solutionMatrix[j, 0] = 0;
            //    solutionsList1[0].balloonsCoordinate[j, 0] = new Coordinate();


            //}

            //string filename = "solution" + DateTime.Now.Ticks;
            //using (TextWriter tw = new StreamWriter(@"C:\Users\vegim\Desktop\" + filename + ".txt"))

            //{
            //    for (int j = 0; j < solutionsList1[0].solutionMatrix.GetLength(0); j++)
            //    {
            //        for (int i = 0; i < solutionsList1[0].solutionMatrix.GetLength(1); i++)
            //        {
            //            tw.Write(solutionsList1[0].balloonsCoordinate[j, i].XCoordinate + " " + solutionsList1[0].balloonsCoordinate[j, i].YCoordinate + " ");
            //        }
            //        tw.WriteLine();
            //    }
            //}

            //int loonScoreRandomLoon = loonfunction.SolutionScoreLoon(solutionsList1[0].solutionMatrix, instance.StartCell, "test", 0);
            //loonfunction.mutate_loon_route(solutionsList1[0], instance, iterator);

            //int score = 0;
            //for (int i = 0; i < solutionsList1[2].solutionMatrix.GetLength(0); i++)
            //{
            //    int[] turnArray = loonfunction.getDimension(solutionsList1[0].solutionMatrix, i, 1);
            //    Coordinate[] baloonCoordinate = loonfunction.GetPreviousTurnCoordinate(solutionsList1[0].balloonsCoordinate, i);
            //    int[] baloonAltitudes = loonfunction.getBaloonAltitudes(solutionsList1[0].solutionMatrix, i);
            //    score += loonfunction.getTurnScore(turnArray, baloonCoordinate, baloonAltitudes, solutionsList1[0], i).Score;
            //}


            //solutionsList1[0].solutionMatrix[1, 0] = 0;
            //int score = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");
            //loonfunction.UpdateBaloonCoordinate(solutionsList1[0].balloonsCoordinate, 1, solutionsList1[0].solutionMatrix, "test");
            //loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");
            //Coordinate point1 = new Coordinate(1, 3);
            //Coordinate point2 = new Coordinate(2, 4);
            //Coordinate point3 = new Coordinate(2, 4);
            //List<Coordinate> list = new List<Coordinate> { point1, point2, point3 };
            //var objects = loonfunction.CalculateCoveredDistinctTargetCells(list);
            //string filename = "solution" + DateTime.Now.Ticks;
            //using (TextWriter tw = new StreamWriter(@"C:\Users\vegim\Desktop\" + filename + ".txt"))

            //{
            //    for (int j = 0; j < solutionsList1[0].solutionMatrix.GetLength(0); j++)
            //    {
            //        for (int i = 0; i < solutionsList1[0].solutionMatrix.GetLength(1); i++)
            //        {
            //            tw.Write(solutionsList1[0].solutionMatrix[j, i] + " ");
            //        }
            //        tw.WriteLine();
            //    }
            //}


            //loonfunction.mutate_loon_route_combine(solutionsList1[0], instance,0);





            string follderPath = @"C:\Users\vegim\OneDrive\Diploma\Documents-Vegim\Visual Studio 2017\Projects\Loon\Loon\Instances\";
            string file = "loon_r70_c300_a8_radius7_saturation_250.in";
            string fullPath = follderPath + file;
            string[] instanceLines = File.ReadAllLines(fullPath);
            Random randomNumberGenerator = new Random();
            int[] possibilities = new int[] { 0, -1, 1 };
            //Input file processing
            List<Solution> solutionsList = new List<Solution>();
            int rows = Convert.ToInt32(instanceLines[0].Split(' ')[0]);
            int columns = Convert.ToInt32(instanceLines[0].Split(' ')[1]);
            int altitudes = Convert.ToInt32(instanceLines[0].Split(' ')[2]);
            InputInstance instance = new InputInstance(altitudes, columns, rows);
            instance.NrTargetCells = Convert.ToInt32(instanceLines[1].Split(' ')[0]);
            instance.Radius = Convert.ToInt32(instanceLines[1].Split(' ')[1]);
            instance.NrBaloons = Convert.ToInt32(instanceLines[1].Split(' ')[2]);
            instance.NrTurns = Convert.ToInt32(instanceLines[1].Split(' ')[3]);
            instance.StartCell = new Coordinate(Convert.ToInt32(instanceLines[2].Split(' ')[0]), Convert.ToInt32(instanceLines[2].Split(' ')[1]));

            for (int i = 0; i < instance.NrTargetCells; i++)
            {
                instance.TargetCells.Add(new Coordinate(Convert.ToInt32(instanceLines[i + 3].Split(' ')[0]), Convert.ToInt32(instanceLines[i + 3].Split(' ')[1])));
            }
            for (int i = 0; i < instance.Altitudes; i++)
            {
                for (int j = 0; j < instance.NrRows; j++)
                {
                    for (int k = 0; k < instance.NrColumns; k++)
                    {
                        int x = Convert.ToInt32(instanceLines[j + (i * instance.NrRows) + (3 + instance.NrTargetCells)].Split(' ')[2 * k]);
                        int y = Convert.ToInt32(instanceLines[(j + (i * instance.NrRows) + (3 + instance.NrTargetCells))].Split(' ')[2 * k + 1]);
                        instance.WindData[i, j, k] = new Coordinate(x, y);
                    }
                }
            }


            LoonFunctions loonfunction = new LoonFunctions(instance);
            List<Solution> solutionsList12 =  loonfunction.generateInitialPopulation(instance, 10);


            //loonfunction.CalculateScoreWithTurnNew(solutionsList12[0]);
            //loonfunction.CalculateScoreWithTurnNew(solutionsList12[1]);
            //for (int i = solutionsList12[0].solutionMatrix.GetLength(1); i >= 0; i--)
            //{
            //    loonfunction.mutate_loon_turn(solutionsList12[0], i);

            //}
            //List<Solution> solutionsList123 =  loonfunction.crossoverOperator(solutionsList12[0], solutionsList12[1]);

            //loonfunction.CalculateScoreWithTurnNew(solutionsList123[0]);
            //loonfunction.CalculateScoreWithTurnNew(solutionsList123[1]);


            //List<Solution> solutionsList12 = loonfunction.generateInitialSolutionSpread(20);

            //List<Solution> solutionsList12 = loonfunction.generateInitialSolutionSpread(10);

            //loonfunction.mutate_loon_turn(solutionsList12[0], 4);
            //solutionsList12[0].solutionScore = loonfunction.CalculateScoreWithTurnNew(solutionsList12[0]);




            //solutionsList12[0] = loonfunction.mutate_with_biggerChange(solutionsList12[0],true);
            //solutionsList12[0].solutionScore = loonfunction.CalculateScoreWithTurnNew(solutionsList12[0]);
            //Solution solutionsList1 = new Solution(instance.NrTurns, instance.NrBaloons);
            //solutionsList1.solutionScore = loonfunction.getSolutionScore(solutionsList1);





            //loonfunction.generateInitialSolutionImproved(100);


            //loonfunction.mutate_loon_route_random(solutionsList12[0], instance, randomNumberGenerator.Next(1, instance.NrBaloons));
            //solutionsList1.solutionScore = loonfunction.getSolutionScore(solutionsList12[0]);

            //String input = File.ReadAllText(@"C:\Users\vegim\Desktop\solution636832734011273128.txt");
            //int r = 0, c = 0;
            //foreach (var row in input.Split('\n'))
            //{
            //    c = 0;
            //    foreach (var col in row.Trim().Split(' '))
            //    {
            //        solutionsList1.solutionMatrix[r, c] = int.Parse(col.Trim());
            //        c++;
            //    }
            //    r++;
            //}
            //loonfunction.UpdateBaloonCoordinate(solutionsList1.balloonsCoordinate, 0, solutionsList1.solutionMatrix, "test");
            //int score = loonfunction.getSolutionScore(solutionsList1);
            //int score2 = 0;
            //var watch = System.Diagnostics.Stopwatch.StartNew();
            //score2 = loonfunction.getSolutionScore(solutionsList1);
            ////for (int i = 0; i < solutionsList1.solutionMatrix.GetLength(0); i++)
            ////{
            ////    int[] turnArray = loonfunction.getDimension(solutionsList1.solutionMatrix, i, 1);
            ////    Coordinate[] baloonCoordinate = loonfunction.GetPreviousTurnCoordinate(solutionsList1.balloonsCoordinate, i);
            ////    int[] baloonAltitudes = loonfunction.getBaloonAltitudes(solutionsList1.solutionMatrix, i);
            ////    score2 += loonfunction.getTurnScoreNew(turnArray, baloonCoordinate, baloonAltitudes, solutionsList1, i).Score;
            ////}
            //watch.Stop();
            //var elapsed = watch.ElapsedMilliseconds;

            Console.WriteLine();


            

            //int score = loonfunction.CalculateScoreWithTurn(solutionsList1[0]);


            //Coordinate [] baloonCoordinatesOnTurn = loonfunction.GetPreviousTurnCoordinate(solutionsList1[0].balloonsCoordinate, 0);
            //int [] baloonAltitudes = loonfunction.getBaloonAltitudes(solutionsList1[0].solutionMatrix,3);
            //var watch = System.Diagnostics.Stopwatch.StartNew();
            //for (int i = 0; i < 1200; i++)
            //{
            //    loonfunction.getTurnScore(loonfunction.getDimension(solutionsList1[0].solutionMatrix, 3, 1), baloonCoordinatesOnTurn, baloonAltitudes, solutionsList1[0],3) ;
            //}
            //loonfunction.mutate_loon_route(solutionsList1[0], instance, 0);
            //loonfunction.improved_mutate_loon_route(solutionsList1[0], instance, 0);
            //int score = 0;
            //for (int i = 0; i < solutionsList1[0].solutionMatrix.GetLength(0); i++)
            //{
            //    int[] turnArray = loonfunction.getDimension(solutionsList1[0].solutionMatrix, i, 1);
            //    Coordinate[] baloonCoordinate = loonfunction.GetPreviousTurnCoordinate(solutionsList1[0].balloonsCoordinate, i);
            //    int[] baloonAltitudes = loonfunction.getBaloonAltitudes(solutionsList1[0].solutionMatrix, i);
            //    score += loonfunction.getTurnScore(turnArray, baloonCoordinate, baloonAltitudes, solutionsList1[0], i).Score;
            //}
            //return score;
            //int oldLoonScore = 0,loon2  = 0 ;
            //int[] loonScores = loonfunction.SolutionScoreLoon2New(solutionsList1.solutionMatrix, instance.StartCell, "test");
            //oldLoonScore = loonScores.Sum();
            //oldLoonScore = loonfunction.SolutionScoreLoonNew(solutionsList1.solutionMatrix, instance.StartCell, "test", 0);
            //loon2 = loonfunction.SolutionScoreLoonNew(solutionsList1.solutionMatrix, instance.StartCell, "test", 1);
            //loonfunction.mutate_loon_route(solutionsList1, instance, 0);
            //oldLoonScore = loonfunction.SolutionScoreLoonNew(solutionsList1.solutionMatrix, instance.StartCell, "test", 0);
            //loon2 = loonfunction.SolutionScoreLoonNew(solutionsList1.solutionMatrix, instance.StartCell, "test", 1);

            //for (int i = 0; i < instance.NrBaloons; i++)
            //{
            //    oldLoonScore += loonfunction.SolutionScoreLoonNew(solutionsList1.solutionMatrix, instance.StartCell, "test", i);
            //}
            //oldLoonScore = loonfunction.SolutionScoreLoon2(solutionsList1.solutionMatrix, instance.StartCell, "test")[0];




           // var watch = System.Diagnostics.Stopwatch.StartNew();
           //// solutionsList1.solutionScore = loonfunction.CalculateScoreWithTurnNew(solutionsList1);

           // //solutionsList1.solutionScore = loonfunction.CalculateScoreWithTurnNew(solutionsList1);
           // watch.Stop();
           // var elapsedMs = watch.ElapsedMilliseconds;

           // string filename = "solution" + DateTime.Now.Ticks;
           // using (TextWriter tw = new StreamWriter(@"C:\Users\vegim\Desktop\" + filename + ".txt"))

           // {
           //     for (int j = 0; j < solutionsList1.solutionMatrix.GetLength(0); j++)
           //     {
           //         for (int i = 0; i < solutionsList1.solutionMatrix.GetLength(1); i++)
           //         {
           //             tw.Write(solutionsList1.solutionMatrix[j, i] + " ");
           //         }
           //         tw.WriteLine();
           //     }
           // }

            Console.WriteLine();













            //List<Solution> childs = new List<Solution>();
            //childs = loonfunction.crossoverOperatorImproved(solutionsList1[0], solutionsList1[1]);

            //int solutionScore = 0;

            //for (int i = 0; i < solutionsList.Count; i++)
            //{
            //    solutionScore = loonfunction.SolutionScore(solutionsList[i].solutionMatrix, instance.StartCell, "main");
            //    Console.WriteLine("Solution for {0} population is {1} ", i, solutionScore);
            //    solutionsList[i].solutionScore = solutionScore;
            //}


            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");
            //loonfunction.mutate(solutionsList1[0], instance, 0.2, 0.4);
            //loonfunction.mutate_turn(solutionsList1[0],instance,0.2, 0.3, 0.2);
            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");
            //Console.ReadLine();
            //string filename = "solution" + DateTime.Now.Ticks;
            //using (TextWriter tw = new StreamWriter(@"C:\Users\vegim\Desktop\" + filename + ".txt"))

            //{
            //    for (int j = 0; j < solutionsList1[0].solutionMatrix.GetLength(0); j++)
            //    {
            //        for (int i = 0; i < solutionsList1[0].solutionMatrix.GetLength(1); i++)
            //        {
            //            tw.Write(solutionsList1[0].solutionMatrix[j, i] + " ");
            //        }
            //        tw.WriteLine();
            //    }
            //}



            //String input = File.ReadAllText(@"C:\Users\vegim\Desktop\solution636825706176435597.txt");

            //int r = 0, c = 0;
            //foreach (var row in input.Split('\n'))
            //{
            //    c = 0;
            //    foreach (var col in row.Trim().Split(' '))
            //    {
            //        solutionsList1[0].solutionMatrix[r, c] = int.Parse(col.Trim());
            //        c++;
            //    }
            //    r++;
            //}
            //loonfunction.UpdateBaloonCoordinate(solutionsList1[0].balloonsCoordinate, 0, solutionsList1[0].solutionMatrix, "test");







            //int score = 0;
            //for (int i = 0; i < solutionsList1[2].solutionMatrix.GetLength(0); i++)
            //{
            //    int[] turnArray = loonfunction.getDimension(solutionsList1[2].solutionMatrix, i, 1);
            //    Coordinate[] baloonCoordinate = loonfunction.GetPreviousTurnCoordinate(solutionsList1[2].balloonsCoordinate, i);
            //    int[] baloonAltitudes = loonfunction.getBaloonAltitudes(solutionsList1[2].solutionMatrix, i);
            //    score += loonfunction.getTurnScore(turnArray, baloonCoordinate, baloonAltitudes, solutionsList1[2], i).Score;
            //}
            //solutionsList1[2].solutionScore = loonfunction.SolutionScore(solutionsList1[2].solutionMatrix, instance.StartCell, "test");
            //Console.Read();
            //int iterator = 0;

            //int[] oonsScores = loonfunction.SolutionScoreLoon2(solutionsList1[0].solutionMatrix, instance.StartCell, "test", 0);
            //oonsScores = loonfunction.SolutionScoreLoon2(solutionsList1[0].solutionMatrix, instance.StartCell, "test", 0);
            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");

            //loonfunction.crossoverOperatorImproved(solutionsList1[0], solutionsList1[1]);

            //int loon_score = loonfunction.SolutionScoreLoon(solutionsList1[0].solutionMatrix, instance.StartCell, "test", 0);
            ////int[] array = loonfunction.SolutionScoreLoon2(solutionsList1[0].solutionMatrix, instance.StartCell, "test", 0);
            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");
            //Console.WriteLine();
            //loonfunction.mutate_loon_route(solutionsList1[0], instance, 0);
            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");
            //loon_score = loonfunction.SolutionScoreLoon(solutionsList1[0].solutionMatrix, instance.StartCell, "test", iterator);
            ////loonfunction.improved_mutate_loon_route(solutionsList1[0], instance, iterator);
            //solutionsList1[0] = loonfunction.mutate_loon_route_combine(solutionsList1[0], instance,0);
            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");
            ////int score = 0;
            ////for (int i = 0; i < solutionsList1[2].solutionMatrix.GetLength(0); i++)
            ////{
            ////    int[] turnArray = loonfunction.getDimension(solutionsList1[0].solutionMatrix, i, 1);
            ////    Coordinate[] baloonCoordinate = loonfunction.GetPreviousTurnCoordinate(solutionsList1[0].balloonsCoordinate, i);
            ////    int[] baloonAltitudes = loonfunction.getBaloonAltitudes(solutionsList1[0].solutionMatrix, i);
            ////    score += loonfunction.getTurnScore(turnArray, baloonCoordinate, baloonAltitudes, solutionsList1[0], i).Score;
            ////}
            //solutionsList1[0] = loonfunction.mutate_loon_route_combine(solutionsList1[0], instance, 1);
            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");

            //solutionsList1[0] = loonfunction.mutate_loon_route_combine(solutionsList1[0], instance, 2);
            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");

            //loonfunction.crossoverOperator(solutionsList1[0], solutionsList1[1]);

            //solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");


            //while (iterator < 1)
            //{
            //    loonfunction.improved_mutate_loon_route(solutionsList1[0], instance, iterator);
            //    //int loon_score = loonfunction.SolutionScoreLoon(solutionsList1[0].solutionMatrix, instance.StartCell, "test", iterator);
            //    solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");

            //    for (int j = 0; j < solutionsList1[0].solutionMatrix.GetLength(0); j++)
            //    {

            //        solutionsList1[0].solutionMatrix[j, 0] = 0;
            //        solutionsList1[0].balloonsCoordinate[j, 0] = new Coordinate();


            //    }
            //    loonfunction.mutate_loon_route(solutionsList1[0], instance, iterator);

            //    //loon_score = loonfunction.SolutionScoreLoon(solutionsList1[0].solutionMatrix, instance.StartCell, "test", 0);
            //    solutionsList1[0].solutionScore = loonfunction.SolutionScore(solutionsList1[0].solutionMatrix, instance.StartCell, "test");
            //    //using (TextWriter tw = new StreamWriter(@"C:\Users\vegim\Desktop\solution2.txt"))
            //    //{
            //    //    for (int j = 0; j < solutionsList1[0].solutionMatrix.GetLength(0); j++)
            //    //    {
            //    //        for (int i = 0; i < solutionsList1[0].solutionMatrix.GetLength(1); i++)
            //    //        {
            //    //            tw.Write(solutionsList1[0].solutionMatrix[j, i] + " ");
            //    //        }
            //    //        tw.WriteLine();
            //    //    }
            //    //}
            //    iterator++;
            //}


            //int qm = 0;

            //    //int[,] solutionmatrix = { { 1 }, { 1 }, { 1 }, { 0 }, { 0 } };
            //    //int[] baloonAltitudes = { 1, 1, 1, 0, 1, 0, 1, 0, 0, 1 };
            //    //int altitude = loonfunction.calculateAltitude(baloonAltitudes, instance.Altitudes);
            //    ////loonfunction.mutate_turn()
            //    ////int[] baloonsAltitudes = { 1, 1, 2, 2 };

            //    ////Coordinate[,] ballonCoordinates = new Coordinate[instance.NrTurns, instance.NrBaloons];

            //    ////ballonCoordinates[0,0] = new Coordinate(1, 3);
            //    ////ballonCoordinates[1,0] = new Coordinate(0, 3);
            //    ////ballonCoordinates[2,0] = new Coordinate(0, 0);
            //    ////ballonCoordinates[3,0] = new Coordinate(0, 1);
            //    ////ballonCoordinates[4,0] = new Coordinate(0, 2);

            //    ////Solution solution = new Solution(instance.NrRows, instance.NrColumns);
            //    ////solution.solutionMatrix = solutionmatrix;
            //    ////solution.balloonsCoordinate = ballonCoordinates;


            //    //////loonfunction.mutate(solution, instance, 0.1);
            //    ////int score = loonfunction.SolutionScore(solutionmatrix, instance.StartCell);

            //    //////loonfunction.mutate_column(solutionmatrix, 0, 1);
            //    //////int score2 = loonfunction.SolutionScore(solutionmatrix, instance.StartCell);

            //    ////////int score = loonfunction.getTurnScore(turnArray, ballonCoordinates, baloonsAltitudes);
            //    ////////int[] baloonAltitudes = loonfunction.getBaloonAltitudes(solutionmatrix, 3);
            //    //////solution.solutionMatrix = solutionmatrix;

            //    ////loonfunction.mutate(solution, instance, 0);


            //    //int[,] parent1 = { { 1, 2, 3 }, { 3, 5, 6 }, { 2, 1, 1 } };
            //    //int[,] parent2 = { { 1, 2, 1 }, { 1, 6, 8 }, { 3, 4, 6 } };
            //    ////List<int[,]> list = loonfunction.crossoverOperator(parent1, parent2);

            //    //int[] array = { 1, 1, 1, 1, 1, 1, 1, 1, 1 ,-1};
            //    //int altitude1 = loonfunction.calculateAltitude(array, 8);


            //    //List<int> test = new List<int>() { 12, 3, 4, 5 };
            //    //for (int i = 0; i < test.Count; i++)
            //    //{
            //    //    int c = test.Take(i).Sum();

            //    //}



            //    for (int i = 0; i < solutionsList1.Count; i++)
            //    {
            //        Console.WriteLine("Solution for {0} population is {1} ", i, SolutionScore(solutionsList1[i].solutionMatrix, instance.StartCell, "main",instance));
            //        solutionsList1[i].solutionScore = SolutionScore(solutionsList1[i].solutionMatrix, instance.StartCell, "main",instance);
            //    }

            //}
            //public int SolutionScore(int[,] solution, Coordinate startingCell, string caller,InputInstance instance)
            //{
            //    int[] balloonsAltitude = new int[solution.GetLength(1)];
            //    balloonsAltitude.Populate(0);
            //    Coordinate[,] balloonsCoordinate = new Coordinate[instance.NrTurns, instance.NrBaloons];
            //    List<Coordinate> targetCellsCovered;
            //    int score = 0;
            //    for (int i = 0; i < solution.GetLength(0); i++)
            //    {
            //        targetCellsCovered = new List<Coordinate>();
            //        for (int j = 0; j < solution.GetLength(1); j++)
            //        {
            //            Coordinate lastCoordinate = new Coordinate();
            //            if (i == 0 && solution[i, j] == 1)
            //            {
            //                balloonsCoordinate[i, j] = startingCell;
            //                lastCoordinate = balloonsCoordinate[i, j];
            //                balloonsAltitude[j] = solution[i, j];
            //            }
            //            else if (i != 0 && balloonsAltitude[j] != 0)
            //            {
            //                lastCoordinate = balloonsCoordinate[i - 1, j];
            //                balloonsAltitude[j] = balloonsAltitude[j] + solution[i, j];
            //            }
            //            if (balloonsAltitude[j] > 0)
            //            {
            //                if (lastCoordinate.XCoordinate < 0 || lastCoordinate.XCoordinate >= instance.NrRows)
            //                {
            //                    balloonsCoordinate[i, j] = lastCoordinate;
            //                }
            //                else
            //                {
            //                    Coordinate windData = instance.WindData[balloonsAltitude[j] - 1, lastCoordinate.XCoordinate, lastCoordinate.YCoordinate];
            //                    balloonsCoordinate[i, j] = ChangeBaloonCoordinate(windData, lastCoordinate,instance);
            //                    if (balloonsCoordinate[i, j].XCoordinate < 0 || balloonsCoordinate[i, j].XCoordinate >= instance.NrRows)
            //                    {
            //                        //do nothing
            //                    }
            //                    else
            //                    {
            //                        targetCellsCovered.AddRange(CalculateCoveredTargetCells(balloonsCoordinate[i, j],instance));

            //                    }
            //                }

            //            }

            //        }
            //        score += CalculateCoveredDistinctTargetCells(targetCellsCovered);
            //    }
            //    return score;


            //}
            //public Coordinate ChangeBaloonCoordinate(Coordinate windDirection, Coordinate baloonLocation,InputInstance instance)
            //{
            //    Coordinate newCoordinate = new Coordinate(0, 0);
            //    newCoordinate.YCoordinate = (baloonLocation.YCoordinate + windDirection.YCoordinate) % (instance.NrColumns);
            //    while (newCoordinate.YCoordinate < 0)
            //    {
            //        newCoordinate.YCoordinate = instance.NrColumns + newCoordinate.YCoordinate;
            //    }

            //    newCoordinate.XCoordinate = (baloonLocation.XCoordinate + windDirection.XCoordinate);
            //    return newCoordinate;
            //}
            //public List<Coordinate> CalculateCoveredTargetCells(Coordinate baloonPosition, InputInstance instance)
            //{
            //    //for a baloon positioned at r,c  u,v is covered if (r − u)2 + (columndist(c,v))2 ≤ V 2
            //    List<Coordinate> coveredCoordinates = new List<Coordinate>();
            //    foreach (var targetCell in instance.TargetCells)
            //    {
            //        if ((Math.Pow(baloonPosition.XCoordinate - targetCell.XCoordinate, 2) + Math.Pow(Math.Min(Math.Abs(baloonPosition.YCoordinate - targetCell.YCoordinate), instance.NrColumns - Math.Abs(baloonPosition.YCoordinate - targetCell.YCoordinate)), 2)) <= Math.Pow(instance.Radius, 2))
            //        {
            //            coveredCoordinates.Add(targetCell);
            //        }
            //    }
            //    return coveredCoordinates;
            //}
            //public int CalculateCoveredDistinctTargetCells(List<Coordinate> coveredTargetCells)
            //{
            //    var distinctCoveredTargetCells = coveredTargetCells.GroupBy(x => new { x.XCoordinate, x.YCoordinate })
            //                                                        .Select(x => x.First())
            //                                                        .ToList();
            //    return distinctCoveredTargetCells.Count;
            //}

            //float meanDistance = distances.Sum() / distances.Count;
            //var sortedDistances = distances.OrderByDescending(a=>a).ToList();
            //var midElement = sortedDistances[sortedDistances.Count / 2];

            //    List<float> distances = new List<float>();
            //    for (int i = 0; i < instance.TargetCells.Count(); i++)
            //    {
            //        for (int j = i + 1; j < instance.TargetCells.Count();)
            //        {
            //            var pointHere = instance.TargetCells[i];
            //            var pointThere = instance.TargetCells[j];
            //            var vectorX = pointThere.XCoordinate - pointHere.XCoordinate;
            //            var vectorY = pointThere.YCoordinate - pointHere.YCoordinate;
            //            var length = Math.Sqrt(Math.Pow(vectorX, 2) + Math.Pow(vectorY, 2));
            //            distances.Add((float)length);
            //            if (length <= 1)
            //            {
            //                instance.TargetCells.RemoveAt(j);
            //            }
            //            else
            //            {
            //                j += 1;
            //            }
            //        }
            //    }

            //    distances = distances.OrderBy(a => a).ToList() ;
            //    Console.WriteLine(instance.TargetCells.Count);
            //}



        }
    }

}
